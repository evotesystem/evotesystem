const util = require("util");
const exec = util.promisify(require("child_process").exec);
const chalk = require("chalk");
const fs = require("fs");
const ncp = require("ncp").ncp;
const path = require("path");
const log = console.log;
const config = require("./config/config.json");

// Copy the Evote.js file to all the required directories

const evoteFileDestinations = [
  "src/adminapp/src/contract",
  "src/votingapp/src/contract",
  "src/evote-cli/contract",
  "server/contract",
];

function distributeEvoteConfig() {
  return new Promise(async (resolve, reject) => {
    await createEvoteJS();

    for (var i = 0; i < evoteFileDestinations.length; i++) {
      await copyEvoteJS(evoteFileDestinations[i]);
    }

    resolve();
  });
}

function copyEvoteJS(p) {
  return new Promise((resolve, reject) => {
    fs.copyFile(
      path.join(__dirname, "config/temp/Evote.js"),
      path.join(__dirname, p, "Evote.js"),
      fs.constants.COPYFILE_FICLONE,
      (err) => {
        if (err) {
          log(chalk.red("Failed to copy Evote file to: ") + err.message);
        } else {
          log(chalk.green("Successfully copied Evote.js file to: " + p));
        }

        resolve();
      }
    );
  });
}

function createEvoteJS() {
  return new Promise((resolve, reject) => {
    try {
      var data = fs.readFileSync(
        path.resolve(__dirname, "config/Evote.config.json")
      );

      if (config.contract.UPDATE_ABI === "TRUE") {
        //load the solidity compiler output
        var abi = fs.readFileSync(
          path.resolve(__dirname, "contract/build/contracts/Evote.json")
        );
        //extract the abi
        abi = JSON.parse(abi).abi;

        //convert the raw data to a js object and set its abi value to the abi
        data = JSON.parse(data);
        data.abi = abi;

        //convert the js object back to raw data
        data = JSON.stringify(data);
      }

      fs.writeFileSync(
        path.resolve(__dirname, "config/temp/Evote.js"),
        `module.exports = ${data}`
      );

      resolve();
    } catch (e) {
      throw e;
    }
  });
}

// Create a build of the voting app and the admin app and copy the build to the server

async function buildAndCopyApps() {
  try {
    log(chalk.blue("Attempting to make a build of the admin app"));

    await exec("cd src/adminapp && npm run build");

    ncp(
      path.resolve(__dirname, "src/adminapp/build"),
      path.resolve(__dirname, "server/public/admin"),
      (err) => {
        if (err) {
          log(
            chalk.red("Failed to copy build of the admin app: ") + err.message
          );
        } else {
          log(chalk.green("Successfully copied build of the admin app"));
        }
      }
    );
  } catch (e) {
    log(chalk.red("Failed to build the admin app: ") + e.message);
    console.error(e);
  }

  try {
    log(chalk.blue("Attempting to make a build of the voting app"));

    await exec("cd src/votingapp && npm run build");

    ncp(
      path.resolve(__dirname, "src/votingapp/build"),
      path.resolve(__dirname, "server/public/client"),
      (err) => {
        if (err) {
          log(
            chalk.red("Failed to copy build of the voting app: ") + err.message
          );
        } else {
          log(chalk.green("Successfully copied build of the voting app"));
        }
      }
    );
  } catch (e) {
    log(chalk.red("Failed to build the voting app: ") + e.message);
    console.error(e);
  }
}

async function main() {
  await distributeEvoteConfig();
  buildAndCopyApps();
}

main();
