const express = require("express");
const helmet = require("helmet");
const path = require("path");
const morgan = require("morgan");
const { createProxyMiddleware } = require("http-proxy-middleware");
const relayTransaction = require("./relay");

require("dotenv").config();

const PORT = process.env.PORT;

const server = express();

if (process.env.NODE_ENV === "production") {
  //code that is executed in production

  //configure logging
  server.use(morgan("tiny"));

  //configure the server to use helmet
  server.use(
    helmet({
      contentSecurityPolicy: {
        useDefaults: true,
        directives: {
          "script-src": ["'self'", "https://ropsten.infura.io"],
          "worker-src": ["'self'", "blob:"],
          "child-src": ["blob:"],
        },
      },
    })
  );
} else {
  //code that is executed in development

  //configure logging
  server.use(morgan("dev"));
}

//configure middleware that parses json
server.use(express.json());

//configure the server to proxy requests to infura
if (process.env.TARGET) {
  server.use(
    "/provider",
    createProxyMiddleware({
      target: process.env.TARGET,
      changeOrigin: true,
      pathRewrite: { "^/provider": process.env.ENDPOINT },
    })
  );
}

//configure the server to use the /public/client folder to server static files for the client app
server.use("/client", express.static(path.join(__dirname, "public", "client")));

//get all the requests coming to the client and send the frontend
server.get("/client/*", (req, res) => {
  res.sendFile(path.resolve(__dirname + "/public/client/index.html"));
});

//configure the server to use the /public/client folder to server static files for the client app
server.use("/admin", express.static(path.join(__dirname, "public", "admin")));

//get all the requests coming to the client and send the frontend
server.get("/admin/*", (req, res) => {
  res.sendFile(path.resolve(__dirname + "/public/admin/index.html"));
});

//configure the server to use the /public/docs folder to serve static files
server.use("/d", express.static(path.join(__dirname, "public", "docs")));

//get all the requests coming to the docs and send the frontend
server.get("/d/*", (req, res) => {
  res.sendFile(path.resolve(__dirname + "/public/docs/index.html"));
});

//get all the request going to the transaction relay
server.post("/relay-transaction", (req, res) => {
  relayTransaction(req.body)
    .then((tx) => {
      res.status(200).send(tx);
    })
    .catch((e) => {
      res.status(e.status).send(e.error.message);
    });
});

//get all other requests
server.get("*", (req, res) => {
  res.redirect("/client/");
});

//start the server
server.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
  console.log(`Voting app is available at: "http://localhost:${PORT}/client/"`);
  console.log(`Admin app is available at: "http://localhost:${PORT}/admin/"`);
  console.log(`Documentation is available at: "http://localhost:${PORT}/d/"`);
});
