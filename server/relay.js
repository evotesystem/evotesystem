const ethers = require('ethers')

const Evote = require("./contract/Evote")

require('dotenv').config()

const PrivateKey = process.env.PRIVATE_KEY

const provider = new ethers.providers.JsonRpcProvider(Evote.provider)
const wallet = new ethers.Wallet(PrivateKey, provider)
const contract = new ethers.Contract(Evote.address, Evote.abi, wallet)

function sendTransaction(vote) {

    return new Promise((res, rej) => {

        contract.vote(vote.pollID, vote.voteHash, vote.vote, vote.from, vote.signature)
        .then(tx => {
            return res(tx)
        })
        .catch(err => {
            return rej(err)
        })

    })

}

function verifyTransaction(vote) {

    return new Promise((res, rej) => {

        // Estimate the gas for the transaction. This tests whether the transaction will succeed
        if(Evote.address !== vote.to) rej(new Error("Contract address doesn't match meta transactions' to address"))
        
        contract.estimateGas.vote(vote.pollID, vote.voteHash, vote.vote, vote.from, vote.signature)
        .then(() => {
            return res()
        })
        .catch(err => {
            return rej(err)
        })

    })
}

function relayTransaction(vote) {

    return new Promise(async (resolve, reject) => {

        //verify that the transaction is correct
        await verifyTransaction(vote).catch(err => reject({status: 400, error: err}))

        //send the transaction
        const tx = await sendTransaction(vote).catch(err => reject({status: 500, error: err}))

        //resolve the promise
        resolve(tx)
    })

}

module.exports = relayTransaction