const fs = require("fs");
const path = require("path");
const chalk = require("chalk");
const config = require("./config/config.json");
const log = console.log;

function main() {
  try {
    //write the server .env file
    log(chalk.blue("Writing .env files"));

    let serverEnvContent = `PORT=${config.server.PORT}\nPRIVATE_KEY=${config.server.PRIVATE_KEY}\nNODE_ENV=${config.server.NODE_ENV}`;

    if (config.server.PROVIDER_PROXY.TARGET) {
      serverEnvContent =
        serverEnvContent +
        `\nTARGET=${config.server.PROVIDER_PROXY.TARGET}\nENDPOINT=${config.server.PROVIDER_PROXY.ENDPOINT}`;
    }

    fs.writeFileSync(path.resolve(__dirname, "server/.env"), serverEnvContent);

    //write the contract .env file

    const contractEnvContent = `LOAD_ADMINS=${config.contract.LOAD_ADMINS}\nPUBLICNET=${config.contract.PUBLICNET}\nMNEMONIC=${config.contract.MNEMONIC}`;

    fs.writeFileSync(
      path.resolve(__dirname, "contract/.env"),
      contractEnvContent
    );

    log(chalk.green("Successfully wrote .env files"));

    //writing admin file (tells the contract which addresses should be used as admins on deployment)

    log(chalk.blue("Writing admin file"));

    const adminConfig = require("./config/Admins.config.json");

    const adminDist = [
      ...adminConfig.registrars,
      ...adminConfig.chairpeople,
      ...adminConfig.electoralBoard,
    ];

    fs.writeFileSync(
      path.resolve(__dirname, "contract/migrations/Admin_Accounts.json"),
      JSON.stringify(adminDist)
    );

    log(chalk.green("Successfully wrote admin file"));
  } catch (error) {
    log(chalk.red("Failed to write .env files"));
    throw error;
  }
}

main();
