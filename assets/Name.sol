// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

contract NameStorage {

    // The contract's state

    address public owner = 0xCa2215c9F029f2548C4964E1F84FD0131B6E3D83;
    string public name;

    // The contract's function

    function updateName(string memory _newName) public {

        // The next line of code checks if the sender of the transaction has the same
        // address as the owner. If this is not the case, the transaction will be reverted
        // and the name won't be updated.
        require(msg.sender == owner);

        // Set the name in the contract's state equal to the _newName parameter
        name = _newName;
    }
}