import "./App.css";
import React, { Suspense, useEffect } from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import useUserManagement from "./hooks/useUserManagement";
import useDataCenter from "./hooks/useDataCenter";

import { ApplicationContext } from "./helper/Context";

import Login from "./components/Login";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import Navbar from "./components/Navbar";
import Loading from "./components/Loading";
const Polls = React.lazy(() => import("./components/poll/Polls"));
const CreatePoll = React.lazy(() => import("./components/poll/CreatePoll"));
const Poll = React.lazy(() => import("./components/poll/Poll"));
const Admin = React.lazy(() => import("./components/admin/Admin"));

function App() {
  //use the custom hooks
  const userManagement = useUserManagement();
  const dataCenter = useDataCenter(
    userManagement.contract,
    userManagement.hasRegistrarAccess,
    userManagement.hasChairpersonAccess,
    userManagement.hasElectoralBoardAccess
  );

  //executed when the login status changes
  useEffect(() => {
    //only execute code if the user is logged in
    if (!userManagement.loggedIn) return;

    //get the open request
    dataCenter.getRequest().catch((err) => console.error(err));
    //listen for changes in the contract state
    dataCenter.listenForChanges();

    // eslint-disable-next-line
  }, [userManagement.loggedIn]);

  return (
    <ApplicationContext.Provider value={{ dataCenter, userManagement }}>
      <Switch>
        {!userManagement.loggedIn ? (
          <>
            <Route path="/login">
              <Login />
            </Route>
            <Route path="*">
              <Redirect to="/login" />
            </Route>
          </>
        ) : (
          <>
            <>
              <Navbar />
              <br />
              <br />
              <br />
              <Suspense fallback={<Loading />}>
                <Route path="/polls">
                  <Polls />
                </Route>
                <Route path="/poll/:id" children={<Poll />} />
                <Route path="/admins">
                  <Admin />
                </Route>
                {userManagement.hasChairpersonAccess() && (
                  <Route path="/create-poll">
                    <CreatePoll />
                  </Route>
                )}
                <Route path="*">
                  <Redirect to="/polls" />
                </Route>
              </Suspense>
              <br />
              <br />
              <footer className="footer py-3">
                <div className="container">
                  {/*TODO: update api, it will be deprecated soon*/}
                  <span className="text-muted">
                    User: {window.ethereum.selectedAddress} (
                    {userManagement.level})
                  </span>
                </div>
              </footer>
            </>
          </>
        )}
      </Switch>
      <ToastContainer
        theme="dark"
        autoClose={5000}
        draggable={true}
        pauseOnHover={true}
        position="bottom-right"
      />
    </ApplicationContext.Provider>
  );
}

export default App;
