function getAdminLevel(contract) {
  return new Promise(async (res, rej) => {
    try {
      const address = await contract.signer.getAddress();

      //check if signer is election board member
      if (await contract.isElectionBoardMember(address)) return res("ELECTORAL_BOARD");

      //check if signer is registrar
      if (await contract.isRegistrar(address)) return res("REGISTRAR");

      //check if singer is a chairperson
      if (await contract.isChairperson(address)) return res("CHAIRPERSON");

      //signer is not an admin
      return res("NO_ADMIN");
    } catch (error) {
      rej(error);
    }
  });
}

export default getAdminLevel;
