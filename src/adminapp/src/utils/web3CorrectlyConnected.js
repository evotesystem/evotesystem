import getAdminLevel from "./getAdminLevel";
import Evote from "../contract/Evote.js";
import { ethers } from "ethers";

function web3CorrectlyConnected(contract) {
  return new Promise((res, rej) => {
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    //Check if network is correct
    provider
      .getNetwork()
      .then((r) => {
        if (r.name === Evote.network) {
          //check if admin level is correct
          getAdminLevel(contract)
            .then((level) => {
              if (level === "NO_ADMIN") {
                //user is not an admin
                rej(new Error("User is not an admin"));
              } else {
                //user is an admin
                res(level);
              }
            })
            .catch((err) => rej(err));
        } else {
          //Reject promise with an error if network is incorrect
          rej(
            new Error(
              "Wrong network connected, please connect to: " + Evote.network
            )
          );
        }
      })
      .catch((err) => console.error(err));
  });
}

export default web3CorrectlyConnected;
