import { useContext, useEffect, useState } from "react";
import { ApplicationContext } from "../../helper/Context";
import {
  Alert,
  ListGroup,
  ListGroupItem,
  Badge,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  Label,
} from "reactstrap";
import { ethers } from "ethers";
import { toast } from "react-toastify";

function Admin() {
  const { dataCenter, userManagement } = useContext(ApplicationContext);

  //code executed on component mount
  useEffect(() => {
    if (dataCenter.admins.electoralBoard.length >= 5) return;

    //get the admins
    toast.loading("Getting admins");
    dataCenter
      .getAdmins()
      .catch((err) => {
        console.error(err);
        toast.dismiss();
        toast.error("Failed to get admins", {
          // position: "bottom-right",
          autoClose: 5000,
          // hideProgressBar: false,
          // closeOnClick: true,
          // pauseOnHover: true,
          // draggable: true,
          // theme: "dark",
          // progress: undefined,
        });
      })
      .then(() => toast.dismiss());

    //cleanup function
    return () => {
      toast.dismiss();
    };

    // eslint-disable-next-line
  }, []);

  //admin actions
  const adminActions = () => ({
    0: userManagement.contract.addRegistrar,
    1: userManagement.contract.removeRegistrar,
    2: userManagement.contract.addChairperson,
    3: userManagement.contract.removeChairperson,
    4: userManagement.contract.addElectionBoardMember,
    5: userManagement.contract.removeElectionBoardMember,
  });

  //get info of a request
  const getRequestInfo = (type) => {
    switch (type) {
      case 0:
        return {
          title: "Add Registrar",
          message: "to add a registrar",
        };
      case 1:
        return {
          title: "Remove Registrar",
          message: "to remove a registrar",
        };
      case 2:
        return {
          title: "Add Chairperson",
          message: "to add a chairperson",
        };
      case 3:
        return {
          title: "Remove Chairperson",
          message: "to remove a chairperson",
        };
      case 4:
        return {
          title: "Add Electoral Board Member",
          message: "to add an electoral board member",
        };
      case 5:
        return {
          title: "Remove Electoral Board Member",
          message: "to remove an electoral board member",
        };
      default:
        return {
          title: "",
          message: "",
        };
    }
  };

  //create request modal
  const [createRequestModal, setCreateRequestModal] = useState({
    modal: false,
    requestType: 0,
    adminAddress: "",
  });

  const toggleCreateRequestModal = () => {
    setCreateRequestModal({
      modal: !createRequestModal,
      requestType: 0,
      adminAddress: "",
    });
  };

  const showRequestModal = (type, address) => {
    setCreateRequestModal({
      modal: true,
      requestType: type,
      adminAddress: address,
    });
  };

  return (
    <>
      <div className="container container-margin-bottom">
        <div className="header">
          <h1>Admins</h1>
        </div>

        <br />
        <RequestBanner
          request={dataCenter.request}
          getRequestInfo={getRequestInfo}
          adminActions={adminActions}
          setRequest={dataCenter.setRequest}
        />

        <CreateRequestModal
          modal={createRequestModal.modal}
          requestType={createRequestModal.requestType}
          adminAddress={createRequestModal.adminAddress}
          toggle={toggleCreateRequestModal}
          getRequestInfo={getRequestInfo}
          adminActions={adminActions}
        />

        <legend>
          Registrars{" "}
          <Badge color="secondary">{dataCenter.admins.registrars.length}</Badge>
        </legend>
        <ListGroup className="word-break">
          {dataCenter.admins.registrars.map((admin) => (
            <ListItem
              key={admin.address}
              address={admin.address}
              remove={() => showRequestModal(1, admin.address)}
              showRemoveButton={
                userManagement.hasElectoralBoardAccess() &&
                dataCenter.admins.registrars.length === 3 &&
                dataCenter.admins.electoralBoard.length === 6
              }
            />
          ))}
        </ListGroup>
        {userManagement.hasElectoralBoardAccess() &&
          dataCenter.admins.registrars.length === 2 && (
            <>
              <Button
                className="addAdminButton"
                color="primary"
                outline
                onClick={() =>
                  setCreateRequestModal({
                    modal: true,
                    requestType: 0,
                    adminAddress: "",
                  })
                }
              >
                Add Registrar
              </Button>
              <br />
            </>
          )}
        <br />

        <legend>
          Chairpeople{" "}
          <Badge picolor="secondary">
            {dataCenter.admins.chairpeople.length}
          </Badge>
        </legend>
        <ListGroup className="word-break">
          {dataCenter.admins.chairpeople.map((admin) => (
            <ListItem
              key={admin.address}
              address={admin.address}
              remove={() => showRequestModal(3, admin.address)}
              showRemoveButton={
                userManagement.hasElectoralBoardAccess() &&
                dataCenter.admins.chairpeople.length === 3 &&
                dataCenter.admins.electoralBoard.length === 6
              }
            />
          ))}
        </ListGroup>
        {userManagement.hasElectoralBoardAccess() &&
          dataCenter.admins.chairpeople.length === 2 && (
            <>
              <Button
                className="addAdminButton"
                color="primary"
                outline
                onClick={() =>
                  setCreateRequestModal({
                    modal: true,
                    requestType: 2,
                    adminAddress: "",
                  })
                }
              >
                Add Chairperson
              </Button>
              <br />
            </>
          )}
        <br />

        <legend>
          Electoral Board{" "}
          <Badge color="secondary">
            {dataCenter.admins.electoralBoard.length}
          </Badge>
        </legend>
        <ListGroup className="word-break">
          {dataCenter.admins.electoralBoard.map((admin) => (
            <ListItem
              key={admin.address}
              address={admin.address}
              remove={() => showRequestModal(5, admin.address)}
              showRemoveButton={
                (userManagement.hasRegistrarAccess() ||
                  userManagement.hasChairpersonAccess()) &&
                dataCenter.admins.electoralBoard.length === 6 &&
                dataCenter.admins.chairpeople.length === 3 &&
                dataCenter.admins.registrars.length === 3
              }
            />
          ))}
        </ListGroup>
        {(userManagement.hasRegistrarAccess() ||
          userManagement.hasChairpersonAccess()) &&
          dataCenter.admins.electoralBoard.length === 5 && (
            <Button
              className="addAdminButton"
              color="primary"
              outline
              onClick={() =>
                setCreateRequestModal({
                  modal: true,
                  requestType: 4,
                  adminAddress: "",
                })
              }
            >
              Add Electoral Board Member
            </Button>
          )}
      </div>
    </>
  );
}

function ListItem(props) {
  return (
    <>
      <ListGroupItem className="admin-list-item">
        <div className="li-left">{props.address}</div>
        {props.showRemoveButton && (
          <div className="li-right">
            <Button outline color="danger" size="sm" onClick={props.remove}>
              Remove
            </Button>
          </div>
        )}
      </ListGroupItem>
    </>
  );
}

function RequestBanner(props) {
  const buttonClicked = () => {
    toast.loading("Submitting request");
    props
      .adminActions()
      [props.request.requestType](props.request.adminAddress, props.request.id)
      .then(() => {
        toast.dismiss();
        toast.success("Succesfully submitted vote");
        props.setRequest((prev) => {
          return {
            ...prev,
            adminVoteSubmitted: true,
          };
        });
      })
      .catch((err) => {
        toast.dismiss();
        toast.error(err.message);
      });
  };

  if (props.request.adminCouldVote && props.request.requestType < 6) {
    return (
      <>
        <Alert color="primary">
          <h4 className="alert-heading">
            {props.getRequestInfo(props.request.requestType).title} Request
          </h4>
          <p>
            A two third request{" "}
            {props.getRequestInfo(props.request.requestType).message} has been
            started:
          </p>
          <ul>
            <li>Request ID: {props.request.id}</li>
            <li>Admin: {props.request.adminAddress}</li>
            <li>
              Votes: {props.request.voteCount}/6 (4 votes required){" "}
              {props.request.adminVoteSubmitted && (
                <Badge pill>Vote is processing</Badge>
              )}
              {props.request.adminVoted && <Badge pill>Voted</Badge>}
            </li>
          </ul>
          {!props.request.adminVoteSubmitted && !props.request.adminVoted && (
            <>
              <hr />
              <Button outline color="primary" onClick={buttonClicked}>
                {props.getRequestInfo(props.request.requestType).title}
              </Button>
            </>
          )}
        </Alert>
      </>
    );
  } else {
    return <></>;
  }
}

function CreateRequestModal(props) {
  const [adminAddress, setAdminAddress] = useState("");
  const [primaryButtonEnabled, setPrimaryButtonEnabled] = useState(false);

  useEffect(() => {
    if (ethers.utils.isHexString(adminAddress) && adminAddress.length === 42) {
      setPrimaryButtonEnabled(true);
    } else {
      setPrimaryButtonEnabled(false);
    }
  }, [adminAddress]);

  useEffect(() => {
    // eslint-disable-next-line
    if (props.adminAddress != "") {
      setPrimaryButtonEnabled(true);
      setAdminAddress(props.adminAddress);
    } else {
      setAdminAddress("");
    }
  }, [props.adminAddress]);

  const createRequestClicked = () => {
    toast.loading("Submitting request");
    props
      .adminActions()
      [props.requestType](adminAddress, 0)
      .then(() => {
        toast.dismiss();
        toast.success("Successfully submitted request");
        props.toggle();
      })
      .catch((err) => {
        toast.dismiss();
        toast.error(err.message);
      });
  };

  return (
    <Modal isOpen={props.modal} toggle={props.toggle}>
      <ModalHeader toggle={props.toggle}>Create new Request</ModalHeader>
      <ModalBody>
        {props.adminAddress === "" && (
          <>
            <Label for="newAdminAddressInput">
              Enter the address you want to add as{" "}
              {props.getRequestInfo(props.requestType).title}
            </Label>
            <Input
              id="newAdminAddressInput"
              type="text"
              value={adminAddress}
              onChange={(e) => setAdminAddress(e.target.value)}
            />
          </>
        )}
        Creating a new request will override the currently open request. Are you
        sure you want to proceed?
      </ModalBody>
      <ModalFooter>
        <Button outline color="danger" onClick={props.toggle}>
          Cancel
        </Button>
        <Button
          color="primary"
          disabled={!primaryButtonEnabled}
          onClick={createRequestClicked}
        >
          Create new request
        </Button>
      </ModalFooter>
    </Modal>
  );
}

export default Admin;
