import { useEffect, useState, useContext } from "react";
import { toast } from "react-toastify";
import { Button, Card } from "reactstrap";
import { ApplicationContext } from "../helper/Context";

function Login() {
  const { userManagement } = useContext(ApplicationContext);

  const [buttonActivated, toggleButtonActivated] = useState(false);

  const loginClicked = () => {
    toast.loading("Logging in", { autoClose: false });

    userManagement.login().catch((err) => handleError(err));
  };

  const handleError = (err) => {
    console.error(err);

    toast.dismiss();

    toast.error(err.message, { autoClose: false });
  };

  useEffect(() => {
    //check if web3 is available and if button should be enabled
    if (window.ethereum) {
      toggleButtonActivated(true);
      setTimeout(() => {
        userManagement.backgroundLogin().catch((err) => console.error(err));
      }, 200);
    } else {
      toggleButtonActivated(false);
      toast.warn(
        "Web3 is not enabled in your browser. Enable it and reload the page.",
        { autoClose: false }
      );
    }

    // eslint-disable-next-line
  }, []);

  // dismiss any toasts on component unmount
  useEffect(() => {
    return () => toast.dismiss();
  });

  return (
    <div>
      <div className="login-container">
        <Card className="login-card box-shadow">
          <h1 className="login-text">Login</h1>
          <Button
            disabled={!buttonActivated}
            color="warning"
            className="metamask-button"
            size="lg"
            onClick={loginClicked}
          >
            Connect Metamask
          </Button>
        </Card>
      </div>
    </div>
  );
}

export default Login;
