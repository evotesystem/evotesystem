import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";
import { useContext, useState } from "react";
import { useParams } from "react-router";
import { ApplicationContext } from "../../helper/Context";
import { toast } from "react-toastify";

function AddVerificationHashModal(props) {
  const {
    userManagement: { contract },
  } = useContext(ApplicationContext);
  const [verificationHashes, setVerificationHashes] = useState(false);

  //get the id of the poll that should be viewed
  const { id } = useParams();

  const fileSelected = (e) => {
    e.target.files[0]
      .text()
      .then((data) => {
        setVerificationHashes(JSON.parse(data));
        //TODO: Check that Meta file is valid
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const cancelClicked = () => {
    setVerificationHashes("");
    props.toggle();
  };

  const submitClicked = () => {
    //TODO: Make this more elegant
    var vHashes = [
      verificationHashes.A.hash,
      verificationHashes.U.hash,
      verificationHashes.O.hash,
      verificationHashes.T.hash,
      verificationHashes.AU.hash,
      verificationHashes.OT.hash,
      verificationHashes.AUOT.hash,
    ];

    contract
      .addVerificationHashes(id, vHashes, verificationHashes.AUOT.signatures)
      .then(() => {
        setVerificationHashes(false);
        props.toggle();
        toast.success("Submitted verification hashes");
      })
      .catch((err) => {
        console.error(err);
        toast.error(err.message, { autoClose: 5000 });
      });
  };

  return (
    <Modal isOpen={props.modal} toggle={props.toggle}>
      <ModalHeader toggle={props.toggle}>Add Verification Hashes</ModalHeader>
      <ModalBody>
        {!verificationHashes ? (
          <>
            <p>Select the meta file</p>
            <Input type="file" accept=".json" onChange={fileSelected} />
          </>
        ) : (
          <>
            {Object.entries(verificationHashes).map(([key, value]) => (
              <div key={key}>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>{key}</InputGroupText>
                  </InputGroupAddon>
                  <Input
                    type="text"
                    disabled
                    value={value.hash ? value.hash : value}
                  />
                </InputGroup>
                <br />
              </div>
            ))}
          </>
        )}
      </ModalBody>
      <ModalFooter>
        <Button outline color="danger" onClick={cancelClicked}>
          Cancel
        </Button>{" "}
        <Button
          color="primary"
          disabled={verificationHashes === ""}
          onClick={submitClicked}
        >
          Submit
        </Button>
      </ModalFooter>
    </Modal>
  );
}

export default AddVerificationHashModal;
