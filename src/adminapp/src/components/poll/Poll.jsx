import { useParams } from "react-router";
import { useEffect, useState, useContext } from "react";
import {
  FormGroup,
  Label,
  Button,
  Input,
  Row,
  Col,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Alert,
  Spinner,
} from "reactstrap";
import classnames from "classnames";
import { toast } from "react-toastify";
import { useHistory } from "react-router-dom";
import { ethers } from "ethers";
import { ApplicationContext } from "../../helper/Context";

import AddVerificationHashModal from "./AddVerificationHashes";
import Voters from "./Voters";
import PollStatus from "./PollStatus";
import Banner from "./Banner";
import Votes from "./Votes";

function Poll() {
  const history = useHistory();

  //get all the polls, level and contract from the application context
  const { dataCenter, userManagement } = useContext(ApplicationContext);

  //get the id of the poll that should be viewed
  const { id } = useParams();

  //store the poll that is being viewed in the state
  const [poll, setPoll] = useState(false);

  //store the vote options of the poll in the state
  const [voteOptions, setVoteOptions] = useState([]);

  //store the verification hashes of the poll in the state
  const [verificationHashes, setVerificationHashes] = useState([]);

  //format the opening, closing and tallying time
  const timeToFormat = (time) => {
    var date = new Date(time * 1000);
    return date.toLocaleString("de-CH"); //date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes()
  };

  //function that gets called when the go back button is presses
  const goBack = () => {
    history.goBack();
  };

  //set the selected poll in the component state
  const setCurrentPoll = () => {
    //TODO: CONSIDER CHANGING THIS CODE
    if (dataCenter.polls.length < id) {
      //poll doesn't exist
      setPoll(false);
      toast.dismiss();
      toast.error(`Poll doesn't exist`, { autoClose: 5000 });
      history.push("/polls");
    } else {
      setPoll(dataCenter.polls[dataCenter.polls.length - id]);
    }
  };

  //get the vote options and verification hashes every time the current poll is updated
  useEffect(() => {
    if (!poll) return;

    //Get the vote options from the smart contract if the poll is set
    setVoteOptions([]);
    for (var i = 0; i < poll.voteOptionsCount; i++) {
      let optionID = i + 1;
      userManagement.contract
        .viewVoteOption(poll.pollID, i + 1)
        .then((res) => {
          setVoteOptions((prev) => {
            let newVoteOptions = [
              ...prev,
              {
                name: res[0],
                votes: ethers.BigNumber.from(res[1]).toNumber(),
                id: optionID,
              },
            ];
            newVoteOptions.sort((a, b) => a.id - b.id);
            return newVoteOptions;
          });
        })
        .catch((err) => {
          console.error(err);
          toast.error(err.message, {
            // position: "bottom-right",
            autoClose: 5000,
            // hideProgressBar: false,
            // closeOnClick: false,
            // pauseOnHover: true,
            // draggable: true,
            // theme: "dark",
            // progress: undefined,
          });
        });
    }

    //Check if the verification hashes have been added and then get them from the blockchain
    if (poll.verificationHashesAdded) {
      userManagement.contract
        .pollIdToVerificationObject(poll.pollID)
        .then((res) => {
          setVerificationHashes(Object.entries(res).splice(7, 7));
        })
        .catch((err) => {
          console.error(err);
          toast.error(err.message, {
            // position: "bottom-right",
            autoClose: 5000,
            // hideProgressBar: false,
            // closeOnClick: false,
            // pauseOnHover: true,
            // draggable: true,
            // theme: "dark",
            // progress: undefined,
          });
        });
    }

    // eslint-disable-next-line
  }, [poll]);

  //code executed on component mount
  useEffect(() => {
    setCurrentPoll();

    // eslint-disable-next-line
  }, []);

  //update the poll when the polls are updated
  useEffect(() => {
    setCurrentPoll();

    // eslint-disable-next-line
  }, [dataCenter.polls]);

  //keeping track of the open tab for votes and voters
  const [activeTab, setActiveTab] = useState("1");

  const toggleTab = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  return (
    <>
      <div className="container container-margin-bottom">
        {poll && (
          <>
            <div className="header">
              <h1>
                #{poll.pollID}: {poll.pollName}
              </h1>
              <div className="right">
                <Button outline color="primary" onClick={goBack}>
                  Back
                </Button>
              </div>
            </div>
            <br />
            <Banner poll={poll} request={dataCenter.request} />
            <PollStatus poll={poll} />
            <legend>Vote Options ({poll.voteOptionsCount})</legend>
            {voteOptions.length > 0 ? (
              <>
                {voteOptions.map((o) => (
                  <div key={o.name}>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>ID: {o.id}</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" disabled value={o.name} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>{o.votes} Votes</InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    <br />
                  </div>
                ))}
              </>
            ) : (
              <div className="align-content-center">
                <Spinner color="primary" />
              </div>
            )}
            <legend>Important Dates</legend>
            <Row>
              <Col md={4}>
                <FormGroup>
                  <Label for="openingDateTime">Opening Time</Label>
                  <Input
                    type="datetime"
                    disabled
                    id="openingDateTime"
                    value={timeToFormat(poll.openingTime)}
                  />
                </FormGroup>
              </Col>
              <Col md={4}>
                <FormGroup>
                  <Label for="closingDateTime">Closing Time</Label>
                  <Input
                    type="datetime"
                    disabled
                    id="closingDateTime"
                    value={timeToFormat(poll.closingTime)}
                  />
                </FormGroup>
              </Col>
              <Col md={4}>
                <FormGroup>
                  <Label for="tallyingDateTime">Tallying Time</Label>
                  <Input
                    type="datetime"
                    disabled
                    id="tallyingDateTime"
                    value={timeToFormat(poll.countTime)}
                  />
                </FormGroup>
              </Col>
            </Row>
            <legend>Poll Security</legend>
            <FormGroup>
              <Label>Verification Hashes</Label>
              <VerificationHashesView
                verificationHashes={verificationHashes}
                poll={poll}
                registrar={userManagement.hasRegistrarAccess()}
              />
            </FormGroup>
            <FormGroup>
              <Label for="pollContentHash">Content Hash</Label>
              <Input
                type="text"
                disabled
                id="pollContentHash"
                value={poll.contentHash}
              />
            </FormGroup>
            <FormGroup>
              <Label for="pollPublicKey">Public Key</Label>
              <textarea
                className="form-control"
                disabled
                id="pollPublicKey"
                rows="14"
                value={poll.publicKey}
              ></textarea>
            </FormGroup>
            {poll.verificationHashesAdded && (
              <>
                {poll.approved && (
                  <>
                    <br />
                    <Nav tabs>
                      <NavItem>
                        <NavLink
                          onClick={() => toggleTab("1")}
                          className={classnames({ active: activeTab === "1" })}
                        >
                          Voters
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          onClick={() => toggleTab("2")}
                          className={classnames({ active: activeTab === "2" })}
                        >
                          Votes
                        </NavLink>
                      </NavItem>
                    </Nav>
                  </>
                )}
                <TabContent activeTab={activeTab}>
                  <TabPane tabId="1">
                    <br />
                    <legend>Voters</legend>
                    <Voters poll={poll} />
                  </TabPane>
                  {poll.approved && (
                    <TabPane tabId="2">
                      <br />
                      <Votes poll={poll} />
                    </TabPane>
                  )}
                </TabContent>
              </>
            )}
          </>
        )}
      </div>
    </>
  );
}

function VerificationHashesView(props) {
  const [modal, setModal] = useState(false);

  const toggleModal = () => setModal(!modal);

  if (props.verificationHashes.length > 0) {
    return (
      <>
        {props.verificationHashes.map(([key, value]) => (
          <div key={key}>
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>{key}</InputGroupText>
              </InputGroupAddon>
              <Input type="text" disabled value={value} />
            </InputGroup>
            <br />
          </div>
        ))}
      </>
    );
  } else if (!props.poll.verificationHashesAdded) {
    if (props.registrar) {
      return (
        <>
          <AddVerificationHashModal modal={modal} toggle={toggleModal} />
          <br />
          <Button outline color="primary" onClick={toggleModal}>
            Add Verification Hashes
          </Button>
        </>
      );
    } else {
      return (
        <Alert color="warning">
          Verification Hashes haven't been added yet
        </Alert>
      );
    }
  } else {
    return <p>Loading...</p>;
  }
}

export default Poll;
