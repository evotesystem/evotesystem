import { useForm } from "react-hook-form";
import { useState, useContext, useEffect } from "react";
import {
  Alert,
  Form,
  FormGroup,
  Label,
  Button,
  Input,
  Row,
  Col,
} from "reactstrap";
import { toast } from "react-toastify";
import { useHistory } from "react-router-dom";
import { ApplicationContext } from "../../helper/Context";

function CreatePoll() {
  let history = useHistory();

  const { userManagement } = useContext(ApplicationContext);

  const [voteOptionCount, setVoteOptionCount] = useState(2);

  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm();

  const [errorMessage, setErrorMessage] = useState("");
  const [showSuccessMessage, setShowSuccessMessage] = useState(false);

  const onSubmit = (e) => {
    //TODO: validate time input more extensive in future version

    const openingDateSplit = e.openingDate.split("-");
    const openingTimeSplit = e.openingTime.split(":");
    console.log(e.openingTime);

    const closingDateSplit = e.closingDate.split("-");
    const closingTimeSplit = e.closingTime.split(":");

    const tallyingDateSplit = e.tallyingDate.split("-");
    const tallyingTimeSplit = e.tallyingTime.split(":");

    const openingTimestamp = Math.round(
      new Date(
        openingDateSplit[0],
        Number(openingDateSplit[1]) - 1,
        openingDateSplit[2],
        openingTimeSplit[0],
        openingTimeSplit[1]
      ).getTime() / 1000
    );
    const closingTimestamp = Math.round(
      new Date(
        closingDateSplit[0],
        Number(closingDateSplit[1]) - 1,
        closingDateSplit[2],
        closingTimeSplit[0],
        closingTimeSplit[1]
      ).getTime() / 1000
    );
    const tallyingTimestamp = Math.round(
      new Date(
        tallyingDateSplit[0],
        Number(tallyingDateSplit[1]) - 1,
        tallyingDateSplit[2],
        tallyingTimeSplit[0],
        tallyingTimeSplit[1]
      ).getTime() / 1000
    );

    var voteOptions = [e.option1, e.option2];

    if (e.voteOptionCount >= 3) voteOptions.push(e.option3);
    if (e.voteOptionCount >= 4) voteOptions.push(e.option4);
    if (e.voteOptionCount >= 5) voteOptions.push(e.option5);

    toast.loading("Submitting", { autoClose: false });
    userManagement.contract
      .createPoll(
        openingTimestamp,
        closingTimestamp,
        voteOptions,
        e.pollTitle,
        e.contentHash,
        e.publicKey,
        tallyingTimestamp
      )
      .then(() => {
        setShowSuccessMessage(true);
        setErrorMessage("");
        toast.dismiss();
        toast.success("Successfully submitted data", { autoClose: 5000 });
        document.documentElement.scrollTop = 0;
      })
      .catch((err) => {
        console.error(err);
        setErrorMessage(err.message);
        toast.dismiss();
        toast.error(err.message, { autoClose: 10000 });
      });
  };

  const cancelClicked = () => {
    history.goBack();
  };

  const voteOptionCountChanged = (e) => {
    setVoteOptionCount(Number(e.target.value));
  };

  useEffect(() => {
    //set the minimum dates of the inputs
    const date = new Date();
    const dateString =
      date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    document.getElementById("openingDate").setAttribute("min", dateString);
    document.getElementById("closingDate").setAttribute("min", dateString);
    document.getElementById("tallyingDate").setAttribute("min", dateString);

    //TODO: set minimum times
  });

  return (
    <>
      <div className="container container-margin-bottom">
        <div className="header">
          <h1>Create Poll</h1>
          <div className="right">
            {showSuccessMessage ? (
              <Button outline color="success" onClick={cancelClicked}>
                Done
              </Button>
            ) : (
              <Button outline color="danger" onClick={cancelClicked}>
                Cancel
              </Button>
            )}
          </div>
        </div>
        <Form onSubmit={handleSubmit(onSubmit)}>
          {errorMessage !== "" && (
            <>
              <br />
              <Alert color="danger">Error: {errorMessage}</Alert>
            </>
          )}

          {showSuccessMessage && (
            <>
              <br />
              <Alert color="success">
                Succesfully submitted data. The poll will be shown with the
                other polls as soon as the transaction has been processed. You
                can now go back.
              </Alert>
            </>
          )}

          <FormGroup>
            <Label for="pollTitle">Poll Title</Label>
            <Input
              className={`form-control ${errors.pollTitle && "is-invalid"}`}
              {...register("pollTitle", { required: true })}
              type="text"
              id="pollTitle"
            />
          </FormGroup>

          <legend>Vote Options</legend>
          <FormGroup>
            <Label for="selectVoteOptionCount">Vote Option Count</Label>
            <select
              className="form-control"
              defaultValue={2}
              type="select"
              {...register("voteOptionCount", { required: true })}
              id="selectVoteOptionCount"
              onChange={voteOptionCountChanged}
            >
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
            <Label>Vote Options</Label>
            <FormGroup>
              <input
                className={`form-control ${errors.option1 && "is-invalid"}`}
                {...register("option1", { required: true })}
                type="text"
                id="voteOption1"
                placeholder="Option #1"
              />
            </FormGroup>
            <FormGroup>
              <input
                className={`form-control ${errors.option2 && "is-invalid"}`}
                {...register("option2", { required: true })}
                type="text"
                id="voteOption2"
                placeholder="Option #2"
              />
            </FormGroup>
            {voteOptionCount > 2 && (
              <FormGroup>
                <input
                  className={`form-control ${errors.option3 && "is-invalid"}`}
                  {...register("option3", { required: voteOptionCount > 3 })}
                  type="text"
                  id="voteOption3"
                  placeholder="Option #3"
                />
              </FormGroup>
            )}
            {voteOptionCount > 3 && (
              <FormGroup>
                <input
                  className={`form-control ${errors.option4 && "is-invalid"}`}
                  {...register("option4", { required: voteOptionCount > 4 })}
                  type="text"
                  id="voteOption4"
                  placeholder="Option #4"
                />
              </FormGroup>
            )}
            {voteOptionCount > 4 && (
              <FormGroup>
                <input
                  className={`form-control ${errors.option5 && "is-invalid"}`}
                  {...register("option5", { required: voteOptionCount > 5 })}
                  type="text"
                  id="voteOption5"
                  placeholder="Option #5"
                />
              </FormGroup>
            )}
          </FormGroup>

          <legend>Poll Opening</legend>
          <Row form>
            <Col md={6}>
              <FormGroup>
                <Label for="openingDate">Opening Date</Label>
                <Input
                  type="date"
                  className={errors.openingDate && "is-invalid"}
                  id="openingDate"
                  placeholder=""
                  min={new Date().toISOString()}
                  {...register("openingDate", { required: true })}
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="openingTime">Opening Time</Label>
                <Input
                  type="time"
                  className={errors.openingTime && "is-invalid"}
                  id="openingTime"
                  placeholder=""
                  {...register("openingTime", { required: true })}
                />
              </FormGroup>
            </Col>
          </Row>

          <legend>Poll Closing</legend>
          <Row form>
            <Col md={6}>
              <FormGroup>
                <Label for="closingDate">Closing Date</Label>
                <Input
                  type="date"
                  className={errors.closingDate && "is-invalid"}
                  id="closingDate"
                  placeholder=""
                  {...register("closingDate", { required: true })}
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="closingTime">Closing Time</Label>
                <Input
                  type="time"
                  className={errors.closingTime && "is-invalid"}
                  id="closingTime"
                  placeholder=""
                  {...register("closingTime", { required: true })}
                />
              </FormGroup>
            </Col>
          </Row>

          <legend>Vote Tallying</legend>
          <Row form>
            <Col md={6}>
              <FormGroup>
                <Label for="tallyingDate">Tallying Date</Label>
                <Input
                  type="date"
                  className={errors.tallyingDate && "is-invalid"}
                  id="tallyingDate"
                  placeholder=""
                  {...register("tallyingDate", { required: true })}
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="tallyingTime">Tallying Time</Label>
                <Input
                  type="time"
                  className={errors.tallyingTime && "is-invalid"}
                  id="tallyingTime"
                  placeholder=""
                  {...register("tallyingTime", { required: true })}
                />
              </FormGroup>
            </Col>
          </Row>

          <legend>Poll Security</legend>
          <FormGroup>
            <Label for="contentHash">Content Hash</Label>
            <Input
              className={`form-control ${errors.contentHash && "is-invalid"}`}
              {...register("contentHash", {
                required: true,
                pattern: /^0x([A-Fa-f0-9]{64})$/,
              })}
              type="text"
              id="contentHash"
            />
          </FormGroup>
          <FormGroup>
            <Label for="publicKey">Public Key</Label>
            <textarea
              className={`form-control ${errors.publicKey && "is-invalid"}`}
              {...register("publicKey", { required: true })}
              id="publicKey"
              rows="14"
            ></textarea>
          </FormGroup>
          <Button
            color="primary"
            outline
            block
            onClick={handleSubmit(onSubmit)}
            disabled={showSuccessMessage}
          >
            Submit
          </Button>
        </Form>
      </div>
    </>
  );
}

export default CreatePoll;
