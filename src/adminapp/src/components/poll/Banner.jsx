import { Alert, Badge, Button } from "reactstrap";
import getPollStatus from "../../utils/getPollStatus";
import { ApplicationContext } from "../../helper/Context";
import { useContext } from "react";
import { toast } from "react-toastify";

//Use the banner to show active two third requests or call to action buttons

function Banner(props) {
  const status = getPollStatus(props.poll);

  const { userManagement } = useContext(ApplicationContext);

  if (
    userManagement.hasChairpersonAccess() &&
    status.name === "verification_hashes_added"
  ) {
    return (
      <>
        <ConfirmVoters poll={props.poll} request={props.request} />
      </>
    );
  } else if (
    userManagement.hasElectoralBoardAccess() &&
    status.name === "voters_confirmed"
  ) {
    //show banner asking to confirm the poll
    return (
      <>
        <ConfirmPoll poll={props.poll} request={props.request} />
      </>
    );
  } else {
    return <></>;
  }
}

function ConfirmPoll(props) {
  const { dataCenter, userManagement } = useContext(ApplicationContext);

  const makeConfirmationRequest = (id) => {
    userManagement.contract
      .confirmPoll(props.poll.pollID, id)
      .then(() => {
        dataCenter.setRequest((prev) => {
          return { ...prev, adminVoteSubmitted: true };
        });
        toast.success("Submitted vote");
      })
      .catch((err) => {
        console.error(err);
        toast.error(err.message);
      });
  };

  //TODO: CONSIDER DIRECTLY ACCESSING DATA VIA CONTEXT

  //check if a two third request is already open
  if (props.request.requestType === 7) {
    //request is already open
    //show banner asking to confirm the poll
    return (
      <Alert color="primary">
        <h4 className="alert-heading">Confirm Poll</h4>
        <p>A two third request for poll confirmation has been started:</p>
        <ul>
          <li>Request ID: {dataCenter.request.id}</li>
          <li>
            Votes: {dataCenter.request.voteCount}/6 (4 votes required to
            confirm){" "}
            {dataCenter.request.adminVoteSubmitted && (
              <Badge pill>Vote is processing</Badge>
            )}
            {dataCenter.request.adminVoted && <Badge pill>Voted</Badge>}
          </li>
        </ul>
        {!(
          dataCenter.request.adminVoted || dataCenter.request.adminVoteSubmitted
        ) && (
          <>
            <hr />
            <Button
              outline
              color="primary"
              onClick={() => makeConfirmationRequest(dataCenter.request.id)}
            >
              Confirm Poll
            </Button>
          </>
        )}
      </Alert>
    );
  } else {
    //no request is open
    return (
      <Alert color="primary">
        <h4 className="alert-heading">Confirm Poll</h4>
        <p>
          You can start a poll confirmation request.{" "}
          {dataCenter.request.adminVoteSubmitted && (
            <Badge color="secondary" pill>
              Vote is processing
            </Badge>
          )}
        </p>
        {!dataCenter.request.adminVoteSubmitted && (
          <>
            <hr />
            <Button
              outline
              color="primary"
              onClick={() => makeConfirmationRequest(0)}
            >
              Open Request
            </Button>
          </>
        )}
      </Alert>
    );
  }
}

function ConfirmVoters(props) {
  const { dataCenter, userManagement } = useContext(ApplicationContext);

  const makeConfirmationRequest = (id) => {
    userManagement.contract
      .confirmVoters(props.poll.pollID, id)
      .then(() => {
        //TODO: SHOW CONFIRMATION TOAST
        dataCenter.setRequest((prev) => {
          return { ...prev, adminVoteSubmitted: true };
        });
      })
      .catch((err) => {
        //TODO: HANDLE ERROR BETTER
        console.error(err);
      });
  };

  //check if a two third request is already open
  if (props.request.requestType === 6) {
    //request is already open
    //show banner asking to confirm the voters
    return (
      <Alert color="primary">
        <h4 className="alert-heading">Confirm Voters</h4>
        <p>A two third request for voter confirmation has been started:</p>
        <ul>
          <li>Request ID: {props.request.id}</li>
          <li>
            Votes: {props.request.voteCount}/3 (2 votes required to confirm){" "}
            {dataCenter.request.adminVoteSubmitted && (
              <Badge pill>Vote is processing</Badge>
            )}
            {dataCenter.request.adminVoted && <Badge pill>Voted</Badge>}
          </li>
        </ul>
        {!(
          dataCenter.request.adminVoted || dataCenter.request.adminVoteSubmitted
        ) && (
          <>
            <hr />
            <Button
              outline
              color="primary"
              onClick={() => makeConfirmationRequest(dataCenter.request.id)}
            >
              Confirm Voters
            </Button>
          </>
        )}
      </Alert>
    );
  } else {
    //no request is open
    return (
      <Alert color="primary">
        <h4 className="alert-heading">Confirm Voters</h4>
        <p>
          You can start a voter confirmation request.{" "}
          {dataCenter.request.adminVoteSubmitted && (
            <Badge color="secondary" pill>
              Vote is processing
            </Badge>
          )}
        </p>
        {!(
          dataCenter.request.adminVoted || dataCenter.request.adminVoteSubmitted
        ) && (
          <>
            <hr />
            <Button
              outline
              color="primary"
              onClick={() => makeConfirmationRequest(0)}
            >
              Open Request
            </Button>
          </>
        )}
      </Alert>
    );
  }
}

export default Banner;
