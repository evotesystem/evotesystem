import { useContext, useEffect } from "react";
import { Button, Table, Badge, Spinner, Alert } from "reactstrap";
import { useHistory, Link } from "react-router-dom";
import { ApplicationContext } from "../../helper/Context";
import { toast } from "react-toastify";
import Evote from "../../contract/Evote";
import getPollStatus from "../../utils/getPollStatus";

function Polls(props) {
  let history = useHistory();

  const { dataCenter, userManagement } = useContext(ApplicationContext);

  const createPollClicked = () => {
    history.push("/create-poll");
  };

  useEffect(() => {
    if (dataCenter.pollsLoaded) return;

    //get the polls if they haven't been loaded yet
    dataCenter.getPolls().catch((err) => {
      toast.error(err.message, { autoClose: 5000 });
    });

    // eslint-disable-next-line
  }, []);

  return (
    <>
      <div className="container">
        <div className="header">
          <div style={{ float: "left" }}>
            <h1>Polls</h1>
            <p className="text-muted">Contract Address: {Evote.address}</p>
          </div>
          {userManagement.hasChairpersonAccess() && (
            <div className="right">
              <Button outline color="primary" onClick={createPollClicked}>
                Create Poll
              </Button>
            </div>
          )}
        </div>
        <div style={{ clear: "both" }}></div>
        {dataCenter.pollsLoaded && dataCenter.polls.length === 0 ? (
          <Alert color="secondary">There are currently no polls.</Alert>
        ) : (
          <>
            {dataCenter.polls.length > 0 ? (
              <Table className="poll-table">
                <thead>
                  <tr>
                    <th>#ID</th>
                    <th>Title</th>
                    <th>Vote Options</th>
                    <th>Status</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {dataCenter.polls.map((poll) => (
                    <tr key={poll.pollID}>
                      <th scope="row">{poll.pollID}</th>
                      <td>{poll.pollName}</td>
                      <td>{poll.voteOptionsCount}</td>
                      <td>
                        <PollStatus poll={poll} />
                      </td>
                      <td>
                        <Link to={`/poll/${poll.pollID}`}>View Poll</Link>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            ) : (
              <div className="align-content-center">
                <Spinner color="primary" />
              </div>
            )}
          </>
        )}
      </div>
    </>
  );
}

function PollStatus(props) {
  switch (getPollStatus(props.poll).name) {
    case "created":
      return (
        <Badge color="info" pill>
          Created
        </Badge>
      );
    case "verification_hashes_added":
      return (
        <Badge color="info" pill>
          Verification Hashes Added
        </Badge>
      );
    case "voters_confirmed":
      return (
        <Badge color="info" pill>
          Voters Confirmed
        </Badge>
      );
    case "approved":
      return (
        <Badge color="primary" pill>
          Approved
        </Badge>
      );
    case "in_progress":
      return (
        <Badge color="success" pill>
          In Progress
        </Badge>
      );
    case "closed":
      return (
        <Badge color="primary" pill>
          Closed
        </Badge>
      );
    case "tally_reached":
      return (
        <Badge color="primary" pill>
          Tally time reached
        </Badge>
      );
    case "failed":
      return (
        <Badge color="danger" pill>
          Failed
        </Badge>
      );
    default:
      return (
        <Badge color="danger" pill>
          An error occured
        </Badge>
      );
  }
}

export default Polls;
