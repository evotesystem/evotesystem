import { useEffect, useContext, useState } from "react";
import { ApplicationContext } from "../../helper/Context";
import {
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText,
  List,
  Badge,
  Button,
  Collapse,
} from "reactstrap";
import { ethers } from "ethers";
import { toast } from "react-toastify";
import InfiniteScroll from "react-infinite-scroll-component";

function Votes(props) {
  const {
    userManagement: { contract },
  } = useContext(ApplicationContext);

  const [votes, setVotes] = useState([]);

  const getVotes = () => {
    const EncryptedVoteFilter = contract.filters.EncryptedVote(
      props.poll.pollID
    );
    contract
      .queryFilter(EncryptedVoteFilter)
      .then((events) => {
        setVotes(events);
      })
      .catch((err) => {
        console.error(err);
        toast.error("Failed to load votes", { autoClose: 5000 });
      });
  };

  useEffect(() => {
    if (props.poll.pollID) {
      getVotes();

      const EncryptedVoteFilter = contract.filters.EncryptedVote(
        props.poll.pollID
      );
      contract.on(EncryptedVoteFilter, getVotes);
    }

    // eslint-disable-next-line
  }, []);

  //react infinite scroll
  const [loadedVotes, setLoadedVotes] = useState([]);
  const [hasMore, setHasMore] = useState(true);

  const loadMore = () => {
    if (votes.length === 0 && loadedVotes.length !== 0)
      return setHasMore(false);
    setLoadedVotes((prev) => [...prev, ...votes.slice(0, 20)]);
    setVotes((prev) => prev.slice(20));
  };

  return (
    <>
      <legend>
        Votes <Badge>{votes.length + loadedVotes.length}</Badge>
      </legend>
      {loadedVotes.length + votes.length !== 0 && (
        <InfiniteScroll
          dataLength={loadedVotes.length}
          next={loadMore}
          hasMore={hasMore}
          loader={<h4>Loading...</h4>}
        >
          <ListGroup className="word-break">
            {loadedVotes.map((v) => (
              <VoteItem key={v.args._voteHash} pid={props.poll.pollID} v={v} />
            ))}
          </ListGroup>
        </InfiniteScroll>
      )}
    </>
  );
}

function VoteItem(props) {
  const { userManagement } = useContext(ApplicationContext);

  const [collapse, setCollapse] = useState(false);
  const [counted, setCounted] = useState(false);
  const toggle = () => setCollapse(!collapse);

  const checkIfCounted = () => {
    userManagement.contract
      .getVoteHashStatus(props.pid, props.v.args._voteHash)
      .then((s) => {
        let cs = ethers.BigNumber.from(s).toNumber();
        switch (cs) {
          case 0:
            setCounted({ message: "No vote hash" });
            break;
          case 1:
            setCounted({ message: "Not counted", color: "secondary" });
            break;
          case 2:
            setCounted({ message: "Counted", color: "success" });
            break;
          default:
            setCounted({
              message: "Couldn't get vote status",
              color: "danger",
            });
            break;
        }
      })
      .catch((err) => {
        setCounted({ message: "Couldn't get vote status", color: "danger" });
        console.error(err);
      });
  };

  useEffect(() => {
    checkIfCounted();
    // eslint-disable-next-line
  }, []);

  return (
    <>
      <ListGroupItem>
        <ListGroupItemHeading>
          From: {props.v.args._voter}{" "}
          {counted && (
            <Badge pill color={counted.color}>
              {counted.message}
            </Badge>
          )}
        </ListGroupItemHeading>
        <Collapse isOpen={collapse}>
          <ListGroupItemText>
            <h5>Vote Hash:</h5>
            <p>{props.v.args._voteHash}</p>
            <h5>Encrypted Vote: </h5>
            <p>{props.v.args._vote}</p>
            <h5>Transaction</h5>
            <List>
              <li>
                <strong>Block Number:</strong> {props.v.blockNumber}
              </li>
              <li>
                <strong>Block Hash:</strong> {props.v.blockHash}
              </li>
              <li>
                <strong>Transaction Hash:</strong> {props.v.transactionHash}
              </li>
              <li>
                <strong>Transaction Index:</strong> {props.v.transactionIndex}
              </li>
            </List>
          </ListGroupItemText>
        </Collapse>
        <Button outline size="sm" onClick={toggle}>
          {collapse ? "Show less" : "Show more"}
        </Button>
      </ListGroupItem>
    </>
  );
}

export default Votes;
