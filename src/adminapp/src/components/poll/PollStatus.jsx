import { ListGroup, ListGroupItem, Badge, Row, Col, Alert } from "reactstrap";
import getPollStatus from "../../utils/getPollStatus";

function PollStatus(props) {
  return (
    <>
      <legend>Poll Status</legend>
      {getPollStatus(props.poll).id === 7 ? (
        <Alert color="danger">Poll failed!</Alert>
      ) : (
        <>
          <Row>
            <Col md={6}>
              <ListGroup>
                <Item
                  poll={props.poll}
                  itemID={1}
                  text="Verification Hashes Added"
                />
                <Item poll={props.poll} itemID={2} text="Voters Confirmed" />
                <Item poll={props.poll} itemID={3} text="Poll Confirmed" />
              </ListGroup>
            </Col>
            <Col md={6}>
              <ListGroup>
                <Item poll={props.poll} itemID={4} text="Poll in Progress" />
                <Item poll={props.poll} itemID={5} text="Poll Finished" />
                <Item poll={props.poll} itemID={6} text="Tally Time Reached" />
              </ListGroup>
            </Col>
          </Row>
        </>
      )}
      <br />
    </>
  );
}

function Item(props) {
  const statusID = getPollStatus(props.poll).id;

  const disabled = statusID < props.itemID - 1;

  if (disabled) {
    //item should be disabled
    return (
      <ListGroupItem disabled>
        {props.text}{" "}
        <Badge pill color="secondary">
          False
        </Badge>
      </ListGroupItem>
    );
  } else if (statusID >= props.itemID) {
    //item should display success
    /*color="success"*/
    return (
      <ListGroupItem>
        {props.text}{" "}
        <Badge pill color="success">
          True
        </Badge>
      </ListGroupItem>
    );
  } else if (statusID === props.itemID - 1) {
    //item is next up
    return (
      <ListGroupItem color="primary">
        {props.text}{" "}
        <Badge pill color="primary">
          Next
        </Badge>
      </ListGroupItem>
    );
  }
}

export default PollStatus;
