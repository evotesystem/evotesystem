import { useContext, useEffect, useState } from "react";
import {
  Label,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  InputGroup,
  ListGroup,
  ListGroupItem,
  Alert,
  Spinner,
} from "reactstrap";
import { ApplicationContext } from "../../helper/Context";
import { utils } from "ethers";
import getPollStatus from "../../utils/getPollStatus";
import { toast } from "react-toastify";
import InfiniteScroll from "react-infinite-scroll-component";

function Voters(props) {
  const { userManagement } = useContext(ApplicationContext);

  const [voters, setVoters] = useState([]);
  const [votersLoaded, setVotersLoaded] = useState(false);

  const [addVoterModal, setAddVoterModal] = useState(false);
  const [removeVoterModal, setRemoveVoterModal] = useState(false);

  const toggleAddVoterModal = () => setAddVoterModal(!addVoterModal);
  const toggleRemoveVoterModal = () => setRemoveVoterModal(!removeVoterModal);

  const getVoters = () => {
    return new Promise((resolve, reject) => {
      let provisionalVoters = [];

      //Create event filters
      const voterAddedFilter = userManagement.contract.filters.VoterAdded(
        null,
        null,
        props.poll.pollID
      );
      const voterRemovedFilter = userManagement.contract.filters.VoterRemoved(
        null,
        null,
        props.poll.pollID
      );

      //Get all the voters that have been added
      userManagement.contract
        .queryFilter(voterAddedFilter)
        .then((events) => {
          for (var i = 0; i < events.length; i++) {
            provisionalVoters.push({
              voter: events[i].args[0],
              block: events[i].blockNumber,
            });
          }
          userManagement.contract
            .queryFilter(voterRemovedFilter)
            .then((evs) => {
              for (let i = 0; i < evs.length; i++) {
                //Remove voters that have been removed from the array
                provisionalVoters = provisionalVoters.filter((e) => {
                  //Check if the voter has been in a VoterRemoved event
                  if (e.voter === evs[i].args[0]) {
                    //TODO: Update this, block number alone is not reliable
                    //Make sure the VoterRemoved event has been emitted is after the VoterAdded event
                    if (e.block < evs[i].blockNumber) return false;
                    return true;
                  } else {
                    return true;
                  }
                });
              }

              setVoters(provisionalVoters);
              resolve();
            })
            .catch((err) => reject(err));
        })
        .catch((err) => reject(err));
    });
  };

  const listenForUpdatedVoters = () => {
    //Listen for voter added events
    const voterAddedFilter = userManagement.contract.filters.VoterAdded(
      null,
      null,
      props.poll.pollID
    );
    userManagement.contract.on(
      voterAddedFilter,
      (voter, chairperson, pollID) => {
        //remove the voter from the complete list of voters and add it to the loaded voters
        setVoters((prev) => {
          let updatedVoters = prev.filter((v) => v.voter !== voter); //prevent duplication
          return updatedVoters;
        });
        setLoadedVoters((prev) => {
          let updatedVoters = prev.filter((v) => v.voter !== voter); //prevent duplication
          updatedVoters.push({ voter: voter, block: 0 });
          return updatedVoters;
        });
      }
    );

    //Listen for voter removed events
    const voterRemovedFilter = userManagement.contract.filters.VoterRemoved(
      null,
      null,
      props.poll.pollID
    );
    userManagement.contract.on(
      voterRemovedFilter,
      (voter, chairperson, pollID) => {
        setVoters((prev) => prev.filter((e) => e.voter !== voter));
        setLoadedVoters((prev) => prev.filter((e) => e.voter !== voter));
      }
    );
  };

  useEffect(() => {
    getVoters()
      .catch((err) => {
        console.error(err);
        toast.error(err.message, { autoClose: 5000 });
      })
      .then(() => {
        setVotersLoaded(true);
        listenForUpdatedVoters();
      });
    //TODO: REMOVE LISTENERS IN CLEANUP FUNCTION
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (voters.length > 0) setVotersLoaded(true);
  }, [voters]);

  //react infinite scroll
  const [loadedVoters, setLoadedVoters] = useState([]);
  const [hasMore, setHasMore] = useState(true);

  const loadMore = () => {
    if (voters.length === 0 && loadedVoters.length !== 0)
      return setHasMore(false);
    setLoadedVoters((prev) => [...prev, ...voters.slice(0, 20)]);
    setVoters((prev) => prev.slice(20));
  };

  if (getPollStatus(props.poll).id > 0) {
    return (
      <>
        <div>
          {!votersLoaded && (
            <div className="align-content-center">
              <Spinner color="primary" />
              <br />
            </div>
          )}
          {votersLoaded && voters.length + loadedVoters.length === 0 ? (
            <Alert color="secondary">No voters have been added yet</Alert>
          ) : (
            <p>
              There are {voters.length + loadedVoters.length} voters registered
              to vote on this poll.
            </p>
          )}
          {userManagement.hasChairpersonAccess() &&
            getPollStatus(props.poll).id < 2 && (
              <>
                <AddVoterModal
                  poll={props.poll}
                  voters={voters}
                  modal={addVoterModal}
                  toggle={toggleAddVoterModal}
                />
                <RemoveVoterModal
                  poll={props.poll}
                  modal={removeVoterModal}
                  toggle={toggleRemoveVoterModal}
                />
                <Button size="sm" color="primary" onClick={toggleAddVoterModal}>
                  Add Voters
                </Button>{" "}
                {voters.length > 0 && (
                  <>
                    <Button
                      size="sm"
                      outline
                      color="danger"
                      onClick={toggleRemoveVoterModal}
                    >
                      Remove Voters
                    </Button>
                  </>
                )}
                <br />
              </>
            )}
          {voters.length + loadedVoters.length !== 0 && (
            <ListGroup className="voter-list word-break">
              <InfiniteScroll
                dataLength={loadedVoters.length}
                next={loadMore}
                hasMore={hasMore}
                loader={<h4>Loading...</h4>}
              >
                {loadedVoters.map((voter) => (
                  <ListGroupItem key={voter.voter}>{voter.voter}</ListGroupItem>
                ))}
              </InfiniteScroll>
            </ListGroup>
          )}
        </div>
      </>
    );
  } else {
    return <></>;
  }
}

function AddVoterModal(props) {
  const [voters, setVoters] = useState([]);
  const [progress, setProgress] = useState({
    totalPackages: 0,
    submittedPackages: 0,
  });
  const [displayedVoters, setDisplayedVoters] = useState([]);

  const {
    userManagement: { contract },
  } = useContext(ApplicationContext);

  const fileSelected = (e) => {
    e.target.files[0]
      .text()
      .then((data) => {
        let d = JSON.parse(data);
        d.shift();
        const filteredVoters = d.filter((v) => {
          return !props.voters.some((e) => v === e);
        });
        setVoters(filteredVoters);
        setDisplayedVoters(filteredVoters.slice(0, 5));
      })
      .catch((err) => {
        console.error(err);
        toast.error(err.message);
      });
  };

  const addVotersClicked = async () => {
    //TODO: CONSIDER HANDLING ERRORS BETTER

    const packageCount = Math.ceil(voters.length / 200);
    var packages = [];

    for (var i = 0; i < packageCount; i++) {
      let currentPackage = voters.slice(i * 200, (i + 1) * 200);
      packages.push(currentPackage);
    }

    setProgress((prev) => {
      return { ...prev, totalPackages: packageCount };
    });

    for (let i = 0; i < packageCount; i++) {
      await contract.addVoters(packages[i], props.poll.pollID).catch((err) => {
        console.error(err);
        console.error("Failed to submit package " + (i + 1));
        toast.error(err.message, { autoClose: 5000 });
      });

      setProgress((prev) => {
        return { ...prev, submittedPackages: prev.submittedPackages + 1 };
      });
    }

    toast.info("Finished submitting voters");
    props.toggle();
    setVoters([]);
    setProgress({
      totalPackages: 0,
      submittedPackages: 0,
    });
  };

  const cancelClicked = () => {
    setVoters([]);
    setProgress({
      totalPackages: 0,
      submittedPackages: 0,
    });
    props.toggle();
  };

  return (
    <Modal isOpen={props.modal} toggle={props.toggle}>
      <ModalHeader toggle={props.toggle}>Add Voters</ModalHeader>
      <ModalBody>
        {progress.totalPackages > 0 ? (
          <div style={{ textAlign: "center" }}>
            <Alert color="primary">
              You might be asked to confirm multiple transactions
            </Alert>
            <br />
            <Spinner color="primary" />
            <p>
              Submitted {progress.submittedPackages} out of{" "}
              {progress.totalPackages} packages
            </p>
          </div>
        ) : (
          <>
            {voters.length === 0 ? (
              <Input type="file" accept=".json" onChange={fileSelected} />
            ) : (
              <>
                {displayedVoters.map((voter) => (
                  <div key={voter}>
                    <InputGroup>
                      <Input type="text" disabled value={voter} />
                    </InputGroup>
                    <br />
                  </div>
                ))}
                {displayedVoters.length < voters.length && (
                  <div style={{ textAlign: "center" }}>
                    <h6>
                      There are only 5 out of {voters.length} voters displayed
                    </h6>
                  </div>
                )}
              </>
            )}
          </>
        )}
      </ModalBody>
      <ModalFooter>
        <Button outline color="danger" onClick={cancelClicked}>
          Cancel
        </Button>{" "}
        <Button
          color="primary"
          disabled={voters.length === 0 || progress.totalPackages > 0}
          onClick={addVotersClicked}
        >
          Add Voters
        </Button>
      </ModalFooter>
    </Modal>
  );
}

function RemoveVoterModal(props) {
  const {
    userManagement: { contract },
  } = useContext(ApplicationContext);

  const [voter, setVoter] = useState("");
  const [buttonEnabled, setButtonEnabled] = useState(false);

  const removeVoterClicked = () => {
    contract
      .removeVoters([voter], props.poll.pollID)
      .then(() => {
        props.toggle();
      })
      .catch((err) => {
        console.error(err);
        toast.error(err.message, { autoClose: 5000 });
      });
  };

  const cancelClicked = () => {
    setVoter("");
    props.toggle();
  };

  useEffect(() => {
    if (utils.isHexString(voter) && voter.length === 42) {
      setButtonEnabled(true);
    } else {
      setButtonEnabled(false);
    }
  }, [voter]);

  return (
    <Modal isOpen={props.modal} toggle={props.toggle}>
      <ModalHeader toggle={props.toggle}>Remove Voters</ModalHeader>
      <ModalBody>
        <Label for="removeVoterInput">Voter Address</Label>
        <Input
          autoComplete="off"
          id="removeVoterInput"
          type="text"
          value={voter}
          onChange={(e) => setVoter(e.target.value)}
        />
      </ModalBody>
      <ModalFooter>
        <Button outline color="danger" onClick={cancelClicked}>
          Cancel
        </Button>{" "}
        <Button
          color="primary"
          disabled={!buttonEnabled}
          onClick={removeVoterClicked}
        >
          Remove Voter
        </Button>
      </ModalFooter>
    </Modal>
  );
}

export default Voters;
