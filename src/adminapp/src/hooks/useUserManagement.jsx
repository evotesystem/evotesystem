import { ethers } from "ethers";
import { useState } from "react";
import Evote from "../contract/Evote.js";
import web3CorrectlyConnected from "../utils/web3CorrectlyConnected";

function useUserManagement() {
  const [loggedIn, setLoggedIn] = useState(false);
  const [contract, setContract] = useState(null);
  const [level, setLevel] = useState(null);

  /// ADMIN ACCESS ///
  const hasRegistrarAccess = () => {
    if (level === "ALL" || level === "REGISTRAR") {
      return true;
    } else {
      return false;
    }
  };

  const hasChairpersonAccess = () => {
    if (level === "ALL" || level === "CHAIRPERSON") {
      return true;
    } else {
      return false;
    }
  };

  const hasElectoralBoardAccess = () => {
    if (level === "ALL" || level === "ELECTORAL_BOARD") {
      return true;
    } else {
      return false;
    }
  };

  /// LOGIN ///

  //should be used to re-login when the page is reloaded after previous normal login
  const backgroundLogin = async () => {
    return new Promise((resolve, reject) => {
      if (window.ethereum.selectedAddress && !loggedIn) {
        checkIfCorrectlyConnected().catch((err) => reject(err));
      }
      resolve();
    });
  };

  // should be used when wanting to log in with a button
  const login = () => {
    return new Promise((resolve, reject) => {
      window.ethereum
        .request({ method: "eth_requestAccounts" })
        .then(() => {
          checkIfCorrectlyConnected()
            .then(() => resolve())
            .catch((err) => reject(err));
        })
        .catch((err) => reject(err));
    });
  };

  const checkIfCorrectlyConnected = () => {
    return new Promise((resolve, reject) => {
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      let ctrct = new ethers.Contract(
        Evote.address,
        Evote.abi,
        provider.getSigner()
      );
      web3CorrectlyConnected(ctrct)
        .then((lvl) => {
          setLevel(lvl);
          setContract(ctrct);
          //window.ethereum.removeListener("connect", checkIfCorrectlyConnected);
          listenForLogout();
          setLoggedIn(true);
          resolve();
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  /// LOGOUT ///

  const logOut = () => {
    setLoggedIn(false);
    setLevel(false);
    setContract(null);
    window.ethereum.removeListener("accountsChanged", logOut);
    window.ethereum.removeListener("accountsChanged", logOut);
    window.ethereum.removeListener("accountsChanged", logOut);
    window.location.reload();
  };

  const listenForLogout = () => {
    //listen for network changes
    window.ethereum.on("chainChanged", logOut);
    //listen for account changes
    window.ethereum.on("accountsChanged", logOut);
    //listen for disconnect event
    window.ethereum.on("disconnect", logOut);
  };

  return {
    loggedIn,
    contract,
    level,
    hasRegistrarAccess,
    hasChairpersonAccess,
    hasElectoralBoardAccess,
    backgroundLogin,
    login,
  };
}

export default useUserManagement;
