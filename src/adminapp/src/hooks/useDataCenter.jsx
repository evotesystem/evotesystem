import { ethers } from "ethers";
import { useState } from "react";

function useDataCenter(
  contract,
  hasRegistrarAccess,
  hasChairpersonAccess,
  hasElectoralBoardAccess
) {
  /// ADMIN CENTER ///
  const [admins, setAdmins] = useState({
    registrars: [],
    chairpeople: [],
    electoralBoard: [],
  }); //public

  const getAdmins = () => {
    return new Promise((resolve, reject) => {
      //get the admins
      getAdmin(0)
        .catch((err) => reject(err))
        .then(() => getAdmin(1))
        .catch((err) => reject(err))
        .then(() => getAdmin(2))
        .catch((err) => reject(err))
        .then(() => resolve());
    });
  }; //public

  const getAdmin = (level) => {
    return new Promise((res, rej) => {
      var provisionalAdmins = [];

      //filter the contracts event for admin added events
      const AdminAddedFilter = contract.filters.AdminAdded(null, level);
      contract
        .queryFilter(AdminAddedFilter)
        .then((events) => {
          //add all the admins that have been added along with the block number and log index to the provisional admins
          for (var i = 0; i < events.length; i++) {
            provisionalAdmins.push({
              address: events[i].args[0],
              blockNumber: events[i].blockNumber,
              logIndex: events[i].logIndex,
            });
          }

          //filter the contract for admin removed events
          const AdminRemovedFilter = contract.filters.AdminRemoved(null, level);
          contract
            .queryFilter(AdminRemovedFilter)
            .then((evs) => {
              for (let i = 0; i < evs.length; i++) {
                //filter the provisional admins TODO: improve this with some
                provisionalAdmins = provisionalAdmins.filter((a) => {
                  //check if the admin from the admin removed event is in the provisional admins
                  if (a.address === evs[i].args[0]) {
                    //chech if the block number (and log index if the blockNumbers are the same) of the admin removed event is bigger than the block number from the admin added event
                    if (a.blockNumber < evs[i].blockNumber) {
                      return false;
                    } else if (
                      a.blockNumber === evs[i].blockNumber &&
                      a.logIndex < evs[i].logIndex
                    ) {
                      return false;
                    } else {
                      return true;
                    }
                  } else {
                    return true;
                  }
                });
              }

              //store the admins
              switch (level) {
                case 0:
                  setAdmins((prev) => {
                    return {
                      ...prev,
                      registrars: provisionalAdmins,
                    };
                  });
                  break;

                case 1:
                  setAdmins((prev) => {
                    return {
                      ...prev,
                      chairpeople: provisionalAdmins,
                    };
                  });
                  break;

                case 2:
                  setAdmins((prev) => {
                    return {
                      ...prev,
                      electoralBoard: provisionalAdmins,
                    };
                  });
                  break;
                default:
                  break;
              }

              res();
            })
            .catch((err) => rej(err));
        })
        .catch((err) => rej(err));
    });
  };

  const listenForAdminEvents = () => {
    //listen for admin added or admin removed events
    const AdminAddedFilter = contract.filters.AdminAdded(null, null);
    contract.on(AdminAddedFilter, (admin, adminLevel) => {
      const adminLevelNumber = ethers.BigNumber.from(adminLevel).toNumber();

      switch (adminLevelNumber) {
        case 0:
          setAdmins((prev) => {
            return { ...prev, registrars: [prev.registrars, admin] };
          });
          break;

        case 1:
          setAdmins((prev) => {
            return { ...prev, chairpeople: [prev.chairpeople, admin] };
          });
          break;

        case 2:
          setAdmins((prev) => {
            return { ...prev, electoralBoard: [prev.electoralBoard, admin] };
          });
          break;

        default:
          break;
      }
    });

    const AdminRemovedFilter = contract.filters.AdminRemoved(null, null);
    contract.on(AdminRemovedFilter, (admin, adminLevel) => {
      const adminLevelNumber = ethers.BigNumber.from(adminLevel).toNumber();
      var index;

      switch (adminLevelNumber) {
        case 0:
          index = admins.registrars.indexOf(admin);
          if (index === -1) return;
          setAdmins((prev) => {
            return { ...prev, registrars: prev.registrars.splice(index, 1) };
          });
          break;

        case 1:
          index = admins.chairpeople.indexOf(admin);
          if (index === -1) return;
          setAdmins((prev) => {
            return { ...prev, chairpeople: prev.chairpeople.splice(index, 1) };
          });
          break;

        case 2:
          index = admins.electoralBoard.indexOf(admin);
          if (index === -1) return;
          setAdmins((prev) => {
            return {
              ...prev,
              electoralBoard: prev.electoralBoard.splice(index, 1),
            };
          });
          break;

        default:
          break;
      }
    });
  }; //public

  /// REQUEST CENTER ///
  const [request, setRequest] = useState({
    id: 0,
    loaded: false,
    createdAt: null,
    adminAddress: null,
    pollID: null,
    requestType: null,
    voteCount: 0,
    adminCouldVote: false,
    adminVoted: false,
    adminVoteSubmitted: false,
    finished: false,
  }); //public

  const getRequest = () => {
    return new Promise((resolve, reject) => {
      getRequestID()
        .catch((err) => reject(err))
        .then((id) => {
          if (id > 0) {
            setRequest((prev) => {
              return { ...prev, id: id };
            });
            return getRequestDetails(id);
          } else {
            setRequest((prev) => {
              return { ...prev, id: id, loaded: true };
            });
            resolve();
          }
        })
        .catch((err) => reject(err))
        .then((id) => {
          return adminHasVotedOnRequest(id);
        })
        .catch((err) => reject(err))
        .then(() => resolve());
    });
  }; //public

  const getRequestID = () => {
    return new Promise((resolve, reject) => {
      contract
        .requestCount()
        .then((count) => {
          const id = ethers.BigNumber.from(count).toNumber();
          resolve(id);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  const getRequestDetails = (id) => {
    return new Promise((resolve, reject) => {
      contract
        .idToTwoThirdRequest(id)
        .then((req) => {
          const couldVote = adminCouldVoteOnRequest(req.requestType);
          const requestFinished = checkIfRequestFinished(
            id,
            ethers.BigNumber.from(req.voteCount).toNumber()
          );

          setRequest((prev) => {
            return {
              ...prev,
              createdAt: ethers.BigNumber.from(req.createdAt).toNumber(),
              adminAddress: req.adminAddress,
              pollID: req.pollID,
              requestType: req.requestType,
              voteCount: ethers.BigNumber.from(req.voteCount).toNumber(),
              loaded: true,
              adminCouldVote: couldVote,
              finished: requestFinished,
            };
          });

          resolve(id);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  const adminCouldVoteOnRequest = (type) => {
    switch (type) {
      case 0:
      case 1:
      case 2:
      case 3:
      case 7:
        if (hasElectoralBoardAccess()) return true;
        return false;
      case 4:
      case 5:
        if (hasRegistrarAccess() || hasChairpersonAccess()) return true;
        return false;
      case 6:
        if (hasChairpersonAccess()) return true;
        return false;
      default:
        return false;
    }
  };

  const adminHasVotedOnRequest = (id) => {
    return new Promise((resolve, reject) => {
      contract
        .votedOnRequest(id)
        .then((v) => {
          setRequest((prev) => {
            return {
              ...prev,
              adminVoted: v,
              adminVoteSubmitted: false,
            };
          });
          resolve();
        })
        .catch((err) => reject(err));
    });
  };

  const checkIfRequestFinished = (id, voteCount) => {
    if (id === 6) {
      //confirm poll request only requires 2 votes to be completed
      if (voteCount >= 2) return true;
      return false;
    } else {
      if (voteCount >= 4) return true;
      return false;
    }
  };

  const listenForRequestChanges = () => {
    //filter for AdminVoted events
    const AdminVotedFilter = contract.filters.AdminVoted();
    contract.on(AdminVotedFilter, () => {
      getRequest().catch((err) => console.error(err));
    });

    //filter for RequestCreated events
    const RequestCreatedFilter = contract.filters.RequestCreated();
    contract.on(RequestCreatedFilter, () => {
      getRequest().catch((err) => console.error(err));
    });
  }; //public

  /// VOTE CENTER ///
  const [polls, setPolls] = useState([]);
  const [pollsLoaded, setPollsLoaded] = useState(false);

  //get the polls from the smart contract
  const getPolls = () => {
    return new Promise((resolve, reject) => {
      setPolls([]);

      contract
        .pollCount()
        .then((res) => {
          const pollCount = ethers.BigNumber.from(res).toNumber();

          for (var i = 0; i < pollCount; i++) {
            getPoll(i + 1)
              .then((p) => {
                if (!polls.some((e) => e.pollID === p.pollID)) {
                  //update the polls
                  setPolls((prev) => {
                    let updatedPolls = [...prev, p];
                    updatedPolls.sort((a, b) => b.pollID - a.pollID);
                    return updatedPolls;
                  });
                }
              })
              .catch((err) => {
                reject(err);
              });
          }

          setTimeout(() => setPollsLoaded(true), 200);
          resolve();
        })
        .catch((err) => {
          reject(err);
        });
    });
  }; //public

  //get a single poll from the smart contract
  const getPoll = (id) => {
    return new Promise((resolve, reject) => {
      contract
        .idToPoll(id)
        .then((res) => {
          var p = { ...res };
          p.pollID = ethers.BigNumber.from(p.pollID).toNumber();

          //Convert big numbers to regular js numbers
          p.closingTime = ethers.BigNumber.from(p.closingTime).toNumber();
          p.countTime = ethers.BigNumber.from(res.countTime).toNumber();
          p.openingTime = ethers.BigNumber.from(res.openingTime).toNumber();
          p.voteOptionsCount = ethers.BigNumber.from(
            p.voteOptionsCount
          ).toNumber();

          resolve(p);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  const listenForPollChanges = () => {
    //listen for poll created events
    const PollCreatedFilter = contract.filters.PollCreated(null);
    contract.on(PollCreatedFilter, (_pollID) => {
      const id = ethers.BigNumber.from(_pollID).toNumber();
      getPoll(id)
        .then((p) => {
          if (!polls.some((e) => e.pollID === p.pollID)) {
            //update the polls
            setPolls((prev) => {
              let updatedPolls = [...prev, p];
              updatedPolls.sort((a, b) => b.pollID - a.pollID);
              return updatedPolls;
            });
          }
        })
        .catch((err) => console.error(err));
    });

    //listen for poll updated events
    const PollUpdatedFilter = contract.filters.PollStatusChanged(null);
    contract.on(PollUpdatedFilter, (_pollID) => {
      const id = ethers.BigNumber.from(_pollID).toNumber();
      getPoll(id)
        .then((p) => {
          setPolls((prev) => {
            // eslint-disable-next-line
            let updatedPolls = prev.filter((e) => e.pollID != p.pollID); //remove the old version of the poll if it exists
            updatedPolls.push(p); //add the updated poll
            updatedPolls.sort((a, b) => b.pollID - a.pollID); //sort the polls
            return updatedPolls;
          });
        })
        .catch((err) => console.error(err));
    });
  };

  //TODO: ADD CLEANUP HOOK IN A USEEFFECT FUNCTION FOR LISTENERS

  const listenForChanges = () => {
    listenForAdminEvents();
    listenForRequestChanges();
    listenForPollChanges();
  };

  return {
    admins,
    setAdmins,
    getAdmins,
    request,
    setRequest,
    getRequest,
    polls,
    setPolls,
    pollsLoaded,
    getPolls,
    listenForChanges,
  };
}

export default useDataCenter;
