#!/usr/bin/env node
"use strict";
const React = require("react");
const importJsx = require("import-jsx");
const { render } = require("ink");
const meow = require("meow");

const ui = importJsx("./components/ui");

const chainvote = meow(`
	Usage
	  $ chainvote
`);

render(React.createElement(ui, chainvote.flags));

//https://www.twilio.com/blog/how-to-build-a-cli-with-node-js
