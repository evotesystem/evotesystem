const crypto = require("crypto");
const { Keccak } = require("sha3");
const { ethers } = require("ethers");
const { v4: uuidv4 } = require("uuid");
const fs = require("fs");
const path = require("path");

//decrypt the rsa private key with the passphrase
function decrypt(keyPath, passphrase) {
	return new Promise((res, rej) => {
		try {
			//load the encrypted rsa private key
			const encodedPrivateKey = fs.readFileSync(path.resolve(keyPath));

			//create keyObject from rsa private key
			const privateKey = crypto.createPrivateKey({
				key: encodedPrivateKey,
				format: "pem",
				type: "pkcs8",
				cipher: "aes-256-cbc",
				passphrase: passphrase,
			});

			//export keyObject without encrypting it
			const decryptedKey = privateKey.export({
				type: "pkcs8",
				format: "pem",
			});

			const keyFileName = uuidv4() + "_decryptedPrivateKey.pem";

			//save the decrypted rsa private key
			fs.writeFileSync(
				path.resolve(
					keyPath.substring(0, keyPath.lastIndexOf("/")),
					keyFileName
				),
				decryptedKey
			);

			res();
		} catch (err) {
			rej(err);
		}
	});
}

//create the passphrase for the rsa keys from the private keys from the election board
function createPassphrase(privateKeys, positionsFilePath) {
	return new Promise((res, rej) => {
		try {
			//load the files with the positions and admin addresses
			const rawData = fs.readFileSync(path.resolve(positionsFilePath));
			const data = JSON.parse(rawData);

			//sort the objects ascending by their position
			data.sort(comparePositions);

			for (var i = 0; i < privateKeys.length; i++) {
				//create a wallet with the private key
				const wallet = new ethers.Wallet(privateKeys[i]);

				//find the corresponding private key of the address stored in the positions file
				const found = data.find(
					(element) => element.address == wallet.address && !element.counted
				);

				//make sure that the private keys that have been entered match to the public addresses in the positions file
				if (!found)
					return rej(
						new Error(
							"Private keys entered don't match the public addresses in the positions file"
						)
					);

				//mark the object as counted and add the corresponding private key to it
				data[found.position].counted = true;
				data[found.position].privateKey = privateKeys[i];
			}

			//hash that is being created with the private keys
			var hash = "";

			for (var i = 0; i < data.length; i++) {
				//create a new hash with the private key and the previously created hash
				const hashSum = new Keccak(512);
				const stringToHash = hash + data[i].privateKey;
				hashSum.update(stringToHash);
				hash = String(hashSum.digest("hex"));
			}

			//resolve with the hash
			res(hash);
		} catch (err) {
			rej(err);
		}
	});
}

//compare the position of two private keys
function comparePositions(a, b) {
	if (a.position < b.position) return -1;
	if (a.position > b.position) return 1;
	return console.error("Positions file invalid");
}

function decryptRSAPrivateKey(privateKeys, positionsFilePath, keyFilePath) {
	return new Promise((res, rej) => {
		createPassphrase(privateKeys, positionsFilePath)
			.then((passphrase) => {
				return decrypt(keyFilePath, passphrase);
			})
			.catch((err) => rej(err))
			.then(() => res())
			.catch((err) => rej(err));
	});
}

module.exports = decryptRSAPrivateKey;
