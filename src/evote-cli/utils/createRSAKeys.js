const crypto = require("crypto");
const { Keccak } = require("sha3");
const { ethers } = require("ethers");
const { v4: uuidv4 } = require("uuid");
const fs = require("fs");
const path = require("path");

//create the RSA public and private key
function createRSAKeys(passphrase) {
	//create the keys with the nodejs crypto module
	const { publicKey, privateKey } = crypto.generateKeyPairSync("rsa", {
		modulusLength: 4096,
		publicKeyEncoding: {
			type: "spki",
			format: "pem",
		},
		privateKeyEncoding: {
			type: "pkcs8",
			format: "pem",
			cipher: "aes-256-cbc",
			passphrase: passphrase,
		},
	});

	return { publicKey: publicKey, privateKey: privateKey };
}

//generate a passphrase for the private key (compromised of a hash of the different private keys)
function generatePassphrase(privateKeys) {
	//hash that is being created with the private keys
	var hash = "";
	//array that stores the positions of the private keys in the hashing process
	var addressPositions = [];

	for (var i = 0; i < privateKeys.length; i++) {
		//create a wallet with the private key
		const wallet = new ethers.Wallet(privateKeys[i]);

		//save the position at which the private key gets placed before hashing; this is necessary to recreate the passphrase when the poll is over
		const addressPosition = {
			address: wallet.address,
			position: i,
		};
		addressPositions.push(addressPosition);

		//Create a new hash with the private key and the previously created hash
		const hashSum = new Keccak(512);
		const stringToHash = hash + privateKeys[i];
		hashSum.update(stringToHash);

		hash = String(hashSum.digest("hex"));
	}

	//return the passphrase and the array with the positions of the different private keys in the hashing process
	const returnObject = {
		passphrase: hash,
		positions: addressPositions,
	};

	return returnObject;
}

function main(privateKeys, filePath) {
	return new Promise((resolve, reject) => {
		//generate the passphrase wit the private keys
		const res = generatePassphrase(privateKeys);

		//create the rsa keys with the passphrase
		const rsaKeys = createRSAKeys(res.passphrase);

		//create a uuid for the file name of the generated files
		const uuid = uuidv4();

		try {
			//save the public key
			const publicKeyFileName = uuid + "_publicKey.pem";
			fs.writeFileSync(
				path.resolve(filePath, publicKeyFileName),
				rsaKeys.publicKey
			);
			//save the private key
			const privateKeyFileName = uuid + "_privateKey.pem";
			fs.writeFileSync(
				path.resolve(filePath, privateKeyFileName),
				rsaKeys.privateKey
			);
			//save the  positions file
			const positionsFileName = uuid + "_positions.json";
			fs.writeFileSync(
				path.resolve(filePath, positionsFileName),
				JSON.stringify(res.positions)
			);
			//resolve the promise
			resolve();
		} catch (err) {
			//if there is an error, the promise is resolved with an error
			reject(err);
		}
	});
}

module.exports = main;
