const { ethers } = require("ethers");
const fs = require("fs");
const path = require("path");
const Evote = require("../contract/Evote");

function verifymetaFile(filePath) {
	return new Promise(async (resolve, reject) => {
		try {
			//load the meta file
			const data = await loadMetaFile(filePath);

			//check if all the hashes in the meta file are hex strings
			if (
				!checkIfHexString([
					data.A,
					data.U,
					data.O,
					data.T,
					data.AU,
					data.OT,
					data.AUOT,
				])
			)
				return reject(new Error("No all hashes are valid keccak 256 hashes"));

			//check if the hashes are correct
			const hashesCheck = verifyHashes(data);

			//verify that the signatures are correct
			const signatureCheck = await verifySignatures(data);

			//compare the hashes to the ones stored on the blockchain
			const comparisonCheck = await compareToBlockchain(data);

			//resolve the promise with the results
			resolve({
				hashesCheck: hashesCheck,
				signatureCheck: signatureCheck,
				comparisonCheck: comparisonCheck,
			});
		} catch (err) {
			reject(err);
		}
	});
}

//load the content of the meta file into the application
function loadMetaFile(filePath) {
	return new Promise((resolve, reject) => {
		try {
			//read the file
			const rawData = fs.readFileSync(path.resolve(filePath));
			const data = JSON.parse(rawData);
			resolve(data);
		} catch (err) {
			reject(err);
		}
	});
}

//verify that the hash tree is correct
function verifyHashes(data) {
	var response = [
		{
			name: "AU hash calculation check",
			success: false,
		},
		{
			name: "OT hash calculation check",
			success: false,
		},
		{
			name: "AUOT hash calculation check",
			success: false,
		},
	];

	//calculate the combined hashes
	const calculatedAU = createCombinedHash(data.A.hash, data.U.hash);
	const calculatedOT = createCombinedHash(data.O.hash, data.T.hash);
	const calculatedAUOT = createCombinedHash(data.AU.hash, data.OT.hash);

	//compare the hash that is calculated from a and u to the au hash in the meta file
	if (calculatedAU === data.AU.hash) {
		//hash of a and u match au hash in the meta file
		response[0].success = true;
	}

	//compare the hash that is calculated from o and t to the ot hash in the meta file
	if (calculatedOT === data.OT.hash) {
		//hash of o and t match ot hash in the meta file
		response[1].success = true;
	}

	//compare the hash that is calculated from AU and OT to the AUOT hash in the meta file
	if (calculatedAUOT === data.AUOT.hash) {
		//hash of au and ot match auot hash in the meta file
		response[2].success = true;
	}

	return response;
}

//calculate a combined hash
function createCombinedHash(hash1, hash2) {
	const concatedHashes = ethers.utils.concat([hash1, hash2]);
	return ethers.utils.keccak256(concatedHashes);
}

//check whether the hashes in the meta file actually are hex strings
function checkIfHexString(hashes) {
	for (var i = 0; i < hashes.length; i++) {
		//return false if one of the hashes in the file isn't a hex string or have the wrong length
		if (
			!ethers.utils.isHexString(hashes[i].hash) ||
			hashes[i].hash.length !== 66
		)
			return false;
	}

	return true;
}

//verify the signatures of the hashes
function verifySignatures(data) {
	return new Promise(async (resolve, reject) => {
		//get the registrars from the blockchain
		const registrars = await getRegistrarsFromBlockchain();

		//reject promise if registrars aren't complete
		if (registrars.length !== 3)
			reject(new Error("The registrars aren't complete"));

		//store the addresses that are recovered from the signatures
		var recoveredAddresses = [];

		var signatureFormatCorrect = true;

		//get the signer address for every verification hash
		for (var i = 0; i < data.AUOT.signatures.length; i++) {
			let signature = data.AUOT.signatures[i];
			//verify that signature format is correct
			if (!(ethers.utils.isHexString(signature) && signature.length === 132)) {
				signatureFormatCorrect = false;
			} else {
				try {
					const recoveredAddress = ethers.utils.verifyMessage(
						data.AUOT.hash,
						signature
					);
					recoveredAddresses.push(recoveredAddress);
				} catch (err) {
					reject(err);
				}
			}
		}

		//check for every registrars if a recovered address matches their address
		registrars.forEach((r) => {
			let index = recoveredAddresses.indexOf(r);
			if (index > -1) {
				//remove the address from the recovered addresses, if it matches a registrar address
				recoveredAddresses.splice(index, 1);
			}
		});

		resolve({
			passed: signatureFormatCorrect && recoveredAddresses.length === 0,
			registrars: registrars,
		});
	});
}

//get the registrars from the blockchain
function getRegistrarsFromBlockchain() {
	return new Promise(async (resolve, reject) => {
		//create a contract instance
		const contract = getContractInstance();

		//filters that search for registrar added and registrar removed events
		const RegistrarAddedFilter = contract.filters.AdminAdded(null, 0);
		const RegistrarRemovedFilter = contract.filters.AdminRemoved(null, 0);

		//get the events matching the filters from the blockchain
		const addedRegistrarEvents = await contract.queryFilter(
			RegistrarAddedFilter
		);
		const removedRegistrarEvents = await contract.queryFilter(
			RegistrarRemovedFilter
		);

		//extract the admin accounts from the events
		const addedRegistrars = addedRegistrarEvents.map((r) => r.args._admin);
		const removedRegistrars = removedRegistrarEvents.map((r) => r.args._admin);

		//return only the added registrars that haven't been removed
		resolve(addedRegistrars.filter((r) => removedRegistrars.indexOf(r) < 0));
	});
}

//make sure the hashes in the meta file match the ones stored on the blockchain
function compareToBlockchain(data) {
	return new Promise(async (res, rej) => {
		//create a contract instance
		const contract = getContractInstance();

		var responseObject = {
			title: {
				message: "Title matches blockchain",
				match: false,
			},
			id: {
				messge: "ID matches blockchain",
				match: false,
			},
			hashes: {
				message: "Hashes match blockchain",
				match: false,
			},
			address: {
				messge: "Address doesn't match",
				match: false,
			},
		};

		//get the verification object for the poll id from the smart contract
		const verificationObject = await contract.pollIdToVerificationObject(
			data.pollID
		);

		//verify for the AUOT hash in the meta file that it matches to the AUOT hash on the smart contract
		if (verificationObject.auot === data.AUOT.hash)
			responseObject.hashes.match = true;

		//verify that the pollID is valid
		const pid = await contract.pollCount();
		const pollID = ethers.BigNumber.from(pid).toNumber();
		if (pollID >= data.pollID) responseObject.id = true;

		//verify that the poll name in the meta file matches the poll name on the smart contract
		const poll = await contract.idToPoll(data.pollID);
		if (poll.pollName === data.pollTitle) responseObject.title = true;

		//verify that the address in the meta file matches the address of the smart contract
		if (data.contractAddress === Evote.address)
			responseObject.address.match = true;

		res(responseObject);
	});
}

//creating a readOnly contract instance
function getContractInstance() {
	const provider = new ethers.providers.JsonRpcProvider(Evote.provider);

	const contract = new ethers.Contract(Evote.address, Evote.abi, provider);

	return contract;
}

module.exports = verifymetaFile;
