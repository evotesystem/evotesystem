const { ethers } = require("ethers");

function mnemonicToPrivateKey(mnemonic) {
	//create a wallet from the mnemonic
	const wallet = new ethers.Wallet.fromMnemonic(mnemonic);
	//return the address and private key created from the mnemonic
	const responseObject = {
		address: wallet.address,
		privateKey: wallet.privateKey,
	};
	return responseObject;
}

module.exports = mnemonicToPrivateKey;
