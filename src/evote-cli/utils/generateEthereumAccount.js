const { ethers } = require("ethers");
const bip39 = require("bip39");

function generateEthereumAccount() {
	//set the word list of bip39 to english
	bip39.setDefaultWordlist("english");
	//create a new mnemonic
	const mnemonic = bip39.generateMnemonic();
	//create a wallet from the mnemonic
	const wallet = new ethers.Wallet.fromMnemonic(mnemonic);

	//return the mnemonic, address and private key
	const responseObject = {
		mnemonic: mnemonic,
		address: wallet.address,
		privateKey: wallet.privateKey,
	};
	return responseObject;
}

module.exports = generateEthereumAccount;
