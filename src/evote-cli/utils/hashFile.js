const { Keccak } = require("sha3");
const fs = require("fs");

function hashFile(filePath) {
	return new Promise((res, rej) => {
		try {
			//Load the file
			const fileBuffer = fs.readFileSync(filePath);

			//Create the hash
			const hashSum = new Keccak(256);
			hashSum.update(fileBuffer);
			const hash = "0x" + String(hashSum.digest("hex"));

			//resolve with the hash
			res({ hash: hash });
		} catch (err) {
			//resolve with an error
			res({ error: err });
		}
	});
}

module.exports = hashFile;
