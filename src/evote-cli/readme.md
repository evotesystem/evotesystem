# Chainvote CLI Tool

## Before you start

1. Create a .env file in the root folder of the project and set the `PROVIDER_URL` to a JSONRpcProviderURL.
2. Open the `Evote.js` file in the `resources` folder and set the address field equal to the address of the eVote contract you would like to use.

## Installation

Run npm install to install the node modules:

```bash
$ npm install
```

To install a symlink linking to the project, run:

```bash
$ npm link
```

## Usage

### With Symlink

To start the cli tool enter

```bash
$ chainvote 
```

### Without Symlink

To start the cli tool enter

```bash
$ node chainvote
```