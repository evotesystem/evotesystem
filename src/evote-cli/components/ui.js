"use strict";
const React = require("react");
const importJsx = require("import-jsx");

const Menu = importJsx("./menu");

const CreateEthereumAccount = importJsx("./sub/generateEthereumAccount.jsx");
const MnemonicToPrivateKey = importJsx("./sub/mnemonicToPrivateKey.jsx");
const CreateRSAKeys = importJsx("./sub/createRSAKeys.jsx");
const DecryptRSAPrivateKey = importJsx("./sub/decryptRSAPrivateKey.jsx");
const VerifyMetaFile = importJsx("./sub/verifyMetaFile.jsx");
const HashFile = importJsx("./sub/hashFile.jsx");
const CountVotes = importJsx("./sub/countVotes.jsx");

const App = () => {
	//store which application is currently being shown
	const [applicationID, setApp] = React.useState(0);

	//enum with all the available applications
	const applications = () => ({
		0: <Menu setApp={setApp} />,
		1: <CreateEthereumAccount />,
		2: <MnemonicToPrivateKey />,
		3: <CreateRSAKeys />,
		4: <DecryptRSAPrivateKey />,
		5: <HashFile />,
		6: <VerifyMetaFile />,
		7: <CountVotes />,
	});

	return <>{applications()[applicationID]}</>;
};

module.exports = App;
