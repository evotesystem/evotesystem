"use strict";
const React = require("react");
const { Text } = require("ink");
const generateEthereumAccount = require("../../utils/generateEthereumAccount");

const CreateETHAccount = () => {
	//keep track of the applications state
	const [state, setState] = React.useState({ loaded: false });

	//code that gets executed once the application is loaded
	React.useEffect(() => {
		//call the function that creates the account
		const createdAccount = generateEthereumAccount();
		setState({
			loaded: true,
			address: createdAccount.address,
			mnemonic: createdAccount.mnemonic,
			privateKey: createdAccount.privateKey,
		});
	}, []);

	return (
		<>
			<Text inverse color="yellow">
				{" "}
				Make sure to write down the mnemonic{" "}
			</Text>
			<Text>Address: {state.address}</Text>
			<Text>
				Mnemonic: <Text color="green">{state.mnemonic}</Text>
			</Text>
			<Text>Private Key: {state.privateKey}</Text>
		</>
	);
};

module.exports = CreateETHAccount;
