"use strict";
const React = require("react");
const { Text } = require("ink");
const { ethers } = require("ethers");
const TextInput = require("ink-text-input").default;
const mnemonicToPrivateKey = require("../../utils/mnemonicToPrivateKey.js");

const MnemonicToPrivateKey = () => {
	//store the value of the text input
	const [value, setValue] = React.useState("");
	//keep track of the other state of the application
	const [state, setState] = React.useState({ loaded: false });
	//store the error message that should be displayed
	const [errorMessage, setErrorMessage] = React.useState("");

	//handle onSubmit from text input
	const onSubmit = (i) => {
		//verify that the mnemonic that has been entered is valid
		if (!ethers.utils.isValidMnemonic(i))
			return setErrorMessage("Mnemonic invalid");

		//call the function that converts the mnemonic to the private key and the address
		const result = mnemonicToPrivateKey(i);

		//update the state
		setState({
			loaded: true,
			address: result.address,
			privateKey: result.privateKey,
		});
	};

	return (
		<>
			{state.loaded ? (
				<>
					<Text>Address: {state.address}</Text>
					<Text>Private Key: {state.privateKey}</Text>
					<Text dimColor>Mnemonic: {value}</Text>
				</>
			) : (
				<>
					<Text color="green">Enter Mnemonic:</Text>
					<TextInput value={value} onChange={setValue} onSubmit={onSubmit} />
					<Text color="red">{errorMessage}</Text>
				</>
			)}
		</>
	);
};

module.exports = MnemonicToPrivateKey;
