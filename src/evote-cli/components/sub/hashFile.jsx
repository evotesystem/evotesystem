"use strict";
const React = require("react");
const { Text } = require("ink");
const TextInput = require("ink-text-input").default;
const Spinner = require("ink-spinner").default;
const hashFile = require("../../utils/hashFile");

const HashFile = () => {
	//keep track of the current section
	const [section, setSection] = React.useState("path");

	//store the path where the file is located at
	const [path, setPath] = React.useState("");

	//store the error message that should be displayed
	const [errorMessage, setErrorMessage] = React.useState("");

	//store the hash that is generated from the input file
	const [hash, setHash] = React.useState("");

	//called when enter is hit after entering the path where the rsa keys should be saved to
	const onPathSubmit = (i) => {
		//remove whitespace
		i.trim();

		//remove ' that gets added in the beginning and the end of the path when dropping files in the terminal
		if (i.charAt(0) === "'") i = i.slice(1);
		if (i.charAt(i.length - 1) === "'") i = i.slice(0, -1);

		//show the loading section
		setSection("loading");

		//call the function that hashes the file
		hashFile(i).then((res) => {
			if (res.error) {
				//show an error message if an error occurs
				setErrorMessage(
					"Error: " + res.error.message + ", press ctrl + c to exit"
				);
				//show the failure section
				setSection("failure");
			} else {
				//store the generated hash to the state
				setHash(res.hash);
				//show the success section if the keys are created successfully
				setSection("result");
			}
		});
	};

	//store all the different sections in an enum
	const sections = () => ({
		path: (
			<>
				<Text color="green">Enter the path of the file you want to hash:</Text>
				<TextInput value={path} onChange={setPath} onSubmit={onPathSubmit} />
			</>
		),
		result: (
			<>
				<Text color="green">The hash of the file is:</Text>
				<Text>{hash}</Text>
			</>
		),
		loading: (
			<Text>
				<Text color="green">
					<Spinner type="dots" />
				</Text>
				{" Processing"}
			</Text>
		),
		failure: <></>,
	});

	return (
		<>
			{sections()[section]}
			<Text color="red">{errorMessage}</Text>
		</>
	);
};

module.exports = HashFile;
