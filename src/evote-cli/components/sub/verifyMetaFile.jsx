"use strict";
const React = require("react");
const { Text, Newline } = require("ink");
const Spinner = require("ink-spinner").default;
const TextInput = require("ink-text-input").default;
const verifyMetaFile = require("../../utils/verifyMetaFile");

const VerifyMetaFile = () => {
	//keep track of the current section
	const [section, setSection] = React.useState("path");

	//store the path where the meta file is saved at
	const [path, setPath] = React.useState("");

	//store the error message that should be displayed
	const [errorMessage, setErrorMessage] = React.useState("");

	//store the result of the verifyMetaFile function
	const [result, setResult] = React.useState([]);

	//called when an admin hits enter after entering the path where the meta file is stored at
	const onPathSubmit = (i) => {
		//remove whitespace
		i.trim();

		//remove ' that gets added in the beginning and the end of the path when dropping files in the terminal
		if (i.charAt(0) === "'") i = i.slice(1);
		if (i.charAt(i.length - 1) === "'") i = i.slice(0, -1);

		//set the path in the state equal to the path that has been entered
		setPath(i);

		//remove any error messages
		setErrorMessage("");

		//go to the next section
		setSection("loading");

		//call the verifyMetaFile function
		verifyMetaFile(path)
			.then((res) => {
				//set the result in the application state and show the result section
				setResult(res);
				setSection("result");
			})
			.catch((err) => {
				//show the error section
				setSection("error");
				//show an error message if an error occurs
				setErrorMessage("Error: " + err.message + ", press ctrl + c to exit");
			});
	};

	//store all the different sections in an enum
	const sections = () => ({
		path: (
			<>
				<Text color="green">Enter the file path of the meta file:</Text>
				<TextInput value={path} onChange={setPath} onSubmit={onPathSubmit} />
			</>
		),
		loading: (
			<Text>
				<Text color="green">
					<Spinner type="dots" />
				</Text>
				{" Verifying"}
			</Text>
		),
		result: <ResultView />,
		error: <></>,
	});

	//View that displays the result of the verification function
	const ResultView = () => (
		<Text>
			<Text inverse color="green">
				{" "}
				Results{" "}
			</Text>
			<Newline />
			<Text inverse> Hashes Check </Text>
			{result.hashesCheck.map((i) => (
				<Text key={i.name}>
					<Newline />
					<Text>
						{i.name}:{" "}
						{i.success ? (
							<Text color="green">Passed</Text>
						) : (
							<Text color="red">Failed</Text>
						)}
					</Text>
				</Text>
			))}
			<Newline />
			<Text inverse> Signature Check </Text>
			<Newline />
			{result.signatureCheck.passed ? (
				<Text color="green">Passed</Text>
			) : (
				<Text color="red">Failed</Text>
			)}
			<Newline />
			<Text inverse> Blockchain Comparison Check </Text>
			<Newline />
			<Text>
				Poll title comparison:{" "}
				{result.comparisonCheck.title.match ? (
					<Text color="green">Passed</Text>
				) : (
					<Text color="red">Failed</Text>
				)}
			</Text>
			<Newline />
			<Text>
				Poll id comparison:{" "}
				{result.comparisonCheck.id.match ? (
					<Text color="green">Passed</Text>
				) : (
					<Text color="red">Failed</Text>
				)}
			</Text>
			<Newline />
			<Text>
				Verification hash comparison:{" "}
				{result.comparisonCheck.hashes.match ? (
					<Text color="green">Passed</Text>
				) : (
					<Text color="red">Failed</Text>
				)}
			</Text>
			<Newline />
			<Text>
				Smart contract address match:{" "}
				{result.comparisonCheck.address.match ? (
					<Text color="green">Passed</Text>
				) : (
					<Text color="red">Failed</Text>
				)}
			</Text>
		</Text>
	);

	return (
		<>
			{sections()[section]}
			<Text color="red">{errorMessage}</Text>
		</>
	);
};

module.exports = VerifyMetaFile;
