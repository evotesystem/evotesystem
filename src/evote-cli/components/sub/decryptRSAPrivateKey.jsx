"use strict";
const React = require("react");
const { Text, Box } = require("ink");
const { ethers } = require("ethers");
const TextInput = require("ink-text-input").default;
const Spinner = require("ink-spinner").default;
const decryptRSAPrivateKey = require("../../utils/decryptRSAPrivateKey");

const DecryptRSAPrivateKey = () => {
	//keep track of the current section
	const [section, setSection] = React.useState("positionsPath");

	//store the path where the position and key files are located
	const [positionsPath, setPositionsPath] = React.useState("");
	const [keyPath, setKeyPath] = React.useState("");

	//store the key that is currently being entered by the admin
	const [currentKey, setCurrentKey] = React.useState("");
	//store all the keys that have been entered by admins
	const [keys, setKeys] = React.useState([]);

	//store the error message that should be displayed
	const [errorMessage, setErrorMessage] = React.useState("");

	//called when an admin hits enter after entering the private key
	const onKeySubmit = (i) => {
		//check if input is a hex string
		if (!ethers.utils.isHexString(i))
			return setErrorMessage("Input is not a hex string");
		//make sure the same key hasn't been entered before
		if (keys.includes(i))
			return setErrorMessage("The same key can't be entered twice");
		//make sure the key length is correct
		if (i.length != 66) return setErrorMessage("Key length is incorrect");

		//if everything is ok, the new key is added to the other keys
		setKeys((prev) => [...prev, currentKey]);
		//remove error messages
		setErrorMessage("");
		//reset the text input field
		setCurrentKey("");
	};

	//called when an admin hits enter after entering the path where the key file is stored at
	const onKeyPathSubmit = (i) => {
		//remove whitespace
		i = i.trim();

		//remove ' that gets added in the beginning and the end of the string when dropping files in the terminal
		if (i.charAt(0) === "'") i = i.slice(1);
		if (i.charAt(i.length - 1) === "'") i = i.slice(0, -1);

		//set the path of the key file in the state equal to the path that has been entered
		setKeyPath(i);
		//go to the next section
		setSection("keys");
	};

	//called when an admin hits enter after entering the path where the positions file is stored at
	const onPositionsPathSubmit = (i) => {
		//remove whitespace
		i = i.trim();

		//remove ' that gets added in the beginning and the end of the string when dropping files in the terminal
		if (i.charAt(0) === "'") i = i.slice(1);
		if (i.charAt(i.length - 1) === "'") i = i.slice(0, -1);

		//set the path of the positions file in the state equal to the path that has been entered
		setPositionsPath(i);
		//go to the next section
		setSection("keyPath");
	};

	//store all the different sections in an enum
	const sections = () => ({
		positionsPath: (
			<>
				<Text color="green">Enter the path of the positions file:</Text>
				<TextInput
					value={positionsPath}
					onChange={setPositionsPath}
					onSubmit={onPositionsPathSubmit}
				/>
			</>
		),
		keyPath: (
			<>
				<Text color="green">Enter the path of the RSA private key file:</Text>
				<TextInput
					value={keyPath}
					onChange={setKeyPath}
					onSubmit={onKeyPathSubmit}
				/>
			</>
		),
		keys: (
			<>
				<Text color="green">
					Enter Keys <Text color="white">[{keys.length}/6]</Text>
				</Text>
				<TextInput
					value={currentKey}
					onChange={setCurrentKey}
					onSubmit={onKeySubmit}
				/>
			</>
		),
		loading: (
			<Text>
				<Text color="green">
					<Spinner type="dots" />
				</Text>
				{" Processing"}
			</Text>
		),
		success: (
			<Text color="green">
				Success: <Text color="white">Decrypted private key</Text>
			</Text>
		),
		failure: <></>,
	});

	//watch for changes in the keys
	React.useEffect(() => {
		if (keys.length === 6) {
			//if all the keys have been entered, the loading section is displayed, until the keys have been created
			setSection("loading");
			//remove any error messages that might still be shown
			setErrorMessage("");
			//call the function that decrypts the rsa private key
			decryptRSAPrivateKey(keys, positionsPath, keyPath)
				.then(() => {
					//show the success section if the private key is successfully decrypted
					setSection("success");
				})
				.catch((err) => {
					//show an error message if an error occurs
					setErrorMessage("Error: " + err.message + ", press ctrl + c to exit");
					//show the failure section
					setSection("failure");
				});
		}
	}, [keys]);

	return (
		<>
			{sections()[section]}
			<Text color="red">{errorMessage}</Text>
		</>
	);
};

module.exports = DecryptRSAPrivateKey;
