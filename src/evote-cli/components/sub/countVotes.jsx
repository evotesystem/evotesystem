"use strict";
const { ethers } = require("ethers");
const crypto = require("crypto");
const fs = require("fs");
const path = require("path");
const React = require("react");
const { Text, Newline } = require("ink");
const Spinner = require("ink-spinner").default;
const TextInput = require("ink-text-input").default;
const Evote = require("../../contract/Evote");

function CountVotes() {
	//keep track of the section that is currently being displayed
	const [section, setSection] = React.useState("ethereumkey");

	//store the pollID, keyPath and rsaPrivateKey in state
	const [pollID, setPollID] = React.useState("");
	const [keyPath, setKeyPath] = React.useState("");
	const [rsaPrivateKey, setRSAPrivateKey] = React.useState("");

	//store the ethereum private key of the account that is used to sign the transactions that count the votes.
	const [ethereumPrivateKey, setEthereumPrivateKey] = React.useState("");

	const [downloadedVoteCount, setDownloadedVoteCount] = React.useState(0);

	//store the votes
	const [validVotes, setValidVotes] = React.useState([]);
	const [countedVotes, setCountedVotes] = React.useState([]);
	const [errorVotes, setErrorVotes] = React.useState([]);
	const [decryptedVotes, setDecryptedVotes] = React.useState([]);

	//loading messages
	const [errorMessage, setErrorMessage] = React.useState("");
	const [loadingMessage, setLoadingMessage] = React.useState("");

	//create a contract instance
	const createContractInstance = (privateKey) => {
		const contract = new ethers.Contract(Evote.address, Evote.abi);
		const provider = new ethers.providers.JsonRpcProvider(Evote.provider);

		if (privateKey) {
			const wallet = new ethers.Wallet(privateKey, provider);
			return contract.connect(wallet);
		} else {
			return contract.connect(provider);
		}
	};

	//get the EncryptedVote events from the blockchain
	const getVotes = () => {
		//create a new contract instance
		const contract = createContractInstance();
		//create a filter to get all the encrypted vote events
		const EncryptedVoteFilter = contract.filters.EncryptedVote(Number(pollID));
		//get all events that match the filter
		contract
			.queryFilter(EncryptedVoteFilter)
			.then((events) => {
				if (events.length > 0) {
					//if there are any events, the application checks if the votes already have been counted
					setTimeout(() => checkIfVotesCounted(events), 10);
				} else {
					//if there are no events, an error is created
					let err = new Error("No vote events found");
					console.error(err);
					setErrorMessage(err.message);
					setSection("error");
				}
			})
			.catch((err) => {
				console.error(err);
				setErrorMessage(err.message);
				setSection("error");
			});
	};

	//check if the votes from the blockchain have already been counted
	const checkIfVotesCounted = (events) => {
		//set the downloaded vote count
		setDownloadedVoteCount(events.length);

		//create a new contract instance
		const contract = createContractInstance();

		//check for every vote if it has already been counted
		for (var i = 0; i < events.length; i++) {
			checkIfCounted(events[i], contract)
				.then((r) => {
					if (r.counted) {
						setCountedVotes((prev) => [...prev, r]);
					} else if (r.error) {
						setErrorVotes((prev) => [...prev, r]);
					} else {
						setValidVotes((prev) => [...prev, r]);
					}
				})
				.catch((err) => console.error(err));
		}
	};

	//check if a single vote has been counted
	const checkIfCounted = (e, contract) => {
		return new Promise((resolve, reject) => {
			//get the status of the vote hash from the blockchain
			contract
				.getVoteHashStatus(pollID, e.args._voteHash)
				.then((res) => {
					let status = ethers.BigNumber.from(res).toNumber();
					switch (status) {
						case 0:
							//vote hash does not exist
							resolve({
								error: new Error("Vote hash is not valid"),
								counted: false,
								event: e,
							});
							break;
						case 1:
							//the vote hasn't has been counted
							resolve({ error: null, counted: false, event: e });
							break;
						case 2:
							//vote has already been counted
							resolve({ error: null, counted: true, event: e });
							break;
						default:
							//something went wrong
							resolve({
								error: new Error("Failed to get hash status"),
								counted: false,
								event: e,
							});
							break;
					}
				})
				.catch((err) => {
					console.error(err);
					resolve({ error: err, counted: false, event: e });
				});
		});
	};

	React.useEffect(() => {
		if (downloadedVoteCount <= 0) return;

		if (
			downloadedVoteCount ===
			validVotes.length + countedVotes.length + errorVotes.length
		) {
			setSection("votes");
		}
	}, [validVotes, countedVotes, errorVotes]);

	//decrypt the votes fetched to the blockchain
	const decryptVotes = () => {
		//temporarily store decrypted votes in an array
		var decryptedVotes = [];

		//decrypt every vote
		for (var i = 0; i < validVotes.length; i++) {
			const decryptedVote = crypto.privateDecrypt(
				{ key: rsaPrivateKey },
				Buffer.from(validVotes[i].event.args._vote, "base64")
			);

			const voteString = decryptedVote.toString();
			const sub_items = voteString.split(";");

			let optionID = sub_items[1].split(":")[1];
			let uuid = sub_items[2].split(":")[1];

			decryptedVotes.push({
				optionID: optionID,
				uuid: uuid,
				...validVotes[i],
			});
		}

		//store the votes in the state
		setDecryptedVotes(decryptedVotes);

		setSection("votesdecrypted");
	};

	//counting votes

	const [countingState, setCountingState] = React.useState({
		successful: 0,
		failed: 0,
	});

	const countVotes = async () => {
		//create a contract instance
		const contract = createContractInstance(ethereumPrivateKey);

		//the amount of calls made to count votes
		const packages = Math.ceil(decryptedVotes.length / 80);

		//count the votes
		for (var i = 0; i < packages; i++) {
			const currentVotes = decryptedVotes.slice(i * 80, i * 80 + 80);
			const optionIDs = currentVotes.map((i) => i.optionID);
			const uuids = currentVotes.map((i) => i.uuid);

			try {
				//send (max) 80 votes at a time
				await contract.countVotes(pollID, optionIDs, uuids);
				setCountingState((prev) => ({
					...prev,
					successful: prev.successful + currentVotes.length,
				}));
			} catch (err) {
				setCountingState((prev) => ({ ...prev, failed: prev.failed + 1 }));
				console.error(err);
			}
		}
	};

	//Input Validation

	const onEthereumKeySubmit = (i) => {
		if (i.length === 66 && ethers.utils.isHexString(i)) {
			setErrorMessage("");
			setEthereumPrivateKey(i);
			setSection("pollid");
		} else {
			setErrorMessage("Private key is not valid");
		}
	};

	const onPollIDSubmit = (i) => {
		if (Number(i) > 0) {
			setErrorMessage("");
			setSection("keypath");
		} else {
			setErrorMessage("Poll id is not valid");
		}
	};

	const onKeyPathSubmit = (i) => {
		//remove whitespace
		i = i.trim();

		//remove ' that gets added in the beginning and the end of the path when dropping files in the terminal
		if (i.charAt(0) === "'") i = i.slice(1);
		if (i.charAt(i.length - 1) === "'") i = i.slice(0, -1);

		//read decryption key from file
		const pKey = fs.readFileSync(path.resolve(i));

		//save the private key to the state
		setRSAPrivateKey(pKey);

		//go to the next section
		setLoadingMessage("Getting votes from the blockchain");
		setSection("loading");

		//get the votes from the blockchain
		getVotes();
	};

	const onDecryptVotes = () => {
		setLoadingMessage("Decrypting votes");
		setSection("loading");

		//decrypt the votes
		decryptVotes();
	};

	const onCountVotes = () => {
		//show the counting section
		setSection("counting");

		//count the votes
		countVotes();
	};

	//store all the different sections in an enum

	const sections = () => ({
		ethereumkey: (
			<>
				<Text color="green">
					Enter the ethereum private key that should be used to count the votes:
				</Text>
				<TextInput
					value={ethereumPrivateKey}
					onChange={setEthereumPrivateKey}
					onSubmit={onEthereumKeySubmit}
				/>
			</>
		),
		pollid: (
			<>
				<Text color="green">Enter the id of the poll:</Text>
				<TextInput
					value={pollID}
					onChange={setPollID}
					onSubmit={onPollIDSubmit}
				/>
			</>
		),
		keypath: (
			<>
				<Text color="green">
					Enter the path of the decrypted private key file:
				</Text>
				<TextInput
					value={keyPath}
					onChange={setKeyPath}
					onSubmit={onKeyPathSubmit}
				/>
			</>
		),
		votes: (
			<Text>
				<Text>Downloaded {downloadedVoteCount} votes from the blockchain.</Text>
				<Newline />
				<Text color="green">
					{validVotes.length} votes haven't been counted yet.
				</Text>
				<Newline />
				<Text color="yellow">
					{countedVotes.length} votes have already been counted.
				</Text>
				<Newline />
				<Text color="red">{errorVotes.length} votes have an error.</Text>
				<Newline />
				<Text>Hit enter to decrypt the votes.</Text>
				<TextInput value="" onSubmit={onDecryptVotes} />
			</Text>
		),
		votesdecrypted: (
			<Text>
				<Text color="green">
					Decrypted {decryptedVotes.length} out of {validVotes.length} valid
					votes.
				</Text>
				<Newline />
				<Text>Hit enter to count the votes.</Text>
				<TextInput value="" onSubmit={onCountVotes} />
			</Text>
		),
		counting: (
			<Text>
				{!(
					countingState.failed + countingState.successful ===
					validVotes.length
				) && (
					<>
						<Text>
							<Text color="blue">
								<Spinner type="dots" />
							</Text>{" "}
							Counting votes
						</Text>
						<Newline />
					</>
				)}
				<Text color="green">
					Sucessfully counted {countingState.successful} out of{" "}
					{decryptedVotes.length} decrypted votes.
				</Text>
				{countingState.failed > 0 && (
					<>
						<Newline />
						<Text color="red">
							Failed to count {countingState.failed} decrypted votes.
						</Text>
					</>
				)}
			</Text>
		),
		loading: (
			<Text>
				<Text color="green">
					<Spinner type="dots" />
				</Text>{" "}
				{loadingMessage}
			</Text>
		),
		error: (
			<>
				<Text color="red">A breaking error has occured: {errorMessage}</Text>
			</>
		),
	});

	return (
		<>
			{sections()[section]}
			<Text color="red">{errorMessage}</Text>
		</>
	);
}
module.exports = CountVotes;
