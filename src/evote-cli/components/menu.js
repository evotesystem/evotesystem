"use strict";
const React = require("react");
const { Text } = require("ink");
const SelectInput = require("ink-select-input").default;

const Menu = (props) => {
	//activate the selected app by calling the setApp function in the parent file
	const handleSelect = (selected) => {
		props.setApp(selected.value);
	};

	//array with all the applications that can be selected
	const options = [
		{ label: "[1] Create Ethereum Account", value: 1 },
		{ label: "[2] Generate Ethereum Private Key from Mnemonic", value: 2 },
		{ label: "[3] Create RSA Keys", value: 3 },
		{ label: "[4] Decrypt RSA Private Key", value: 4 },
		{ label: "[5] Get the hash of a file", value: 5 },
		{ label: "[6] Verify Meta File", value: 6 },
		{ label: "[7] Count Votes", value: 7 },
	];

	return (
		<>
			<Text color="green">What do you want to do?</Text>
			<SelectInput items={options} onSelect={handleSelect} />
		</>
	);
};

module.exports = Menu;
