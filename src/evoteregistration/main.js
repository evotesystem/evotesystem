const { app, BrowserWindow, dialog } = require('electron')
const { resetAll } = require('./backend/reset')
const { setWindow } = require('./backend/ipc')

// create desktop and start menu references during windows installation
if(require('electron-squirrel-startup')) app.quit()

var DEV = false

function createWindow() {

    //create a new browser window
    const win = new BrowserWindow({
        icon: __dirname + "/icons/app-icon.png",
        autoHideMenuBar: true,
        width: 1000,
        height: 800,
        title: "eVote Registration",
        webPreferences: {
            nodeIntegration: false,
            contextIsolation: true,
            enableRemoteModule: false,
            preload: __dirname + '/preload.js'
        }
    })

    //load react app
    if (DEV) {
        // if the application is in development mode, it loads the frontend directly from the react development server at localhost:3000
        win.loadURL('http://localhost:3000/')
    } else {
        //if the application is in ipc mode, the index.html file gets loaded directly in the browser window
        win.loadFile('./frontend/index.html')
    }

    //focus the window
    win.focus()

    //pass the window to the ipc.js file, so it can send error events to the renderer process
    setWindow(win)
}

app.whenReady()
.then(() => {

    createWindow()
    
    
    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) {
            createWindow()
        }
    })
    
})

app.on('window-all-closed', () => {
    resetAll().then((err) => {
        if (err) {
            //reopen window and show alert if there is an error resetting the application
            createWindow()
            setTimeout(() => {
                dialog.showMessageBox({message: "Failed to reset Application", type: "error"})
            }, 500)
        } else {
            app.quit()
        }
    })
})