//Keep track of the status and progress of the application
module.exports.appStatus = {
    importedUsers: null,
    importComplete: false,
    jsonCreated: false,
    finishedPDFs: 0,
    expectedPDFs: 0,
    generatedMnemonics: 0,
    expectedMnemonics: 0,
    pollID: 0,
    pollTitle: "",
    address: "",
    pollDecription: "",
    votingLink: ""
}