//https://gist.github.com/GuillermoPena/9233069
//https://ilikekillnerds.com/2020/04/how-to-get-the-hash-of-a-file-in-node-js/

const fs = require('fs')
const path = require('path')
const { ethers } = require('ethers')
const { app } = require('electron')
const { Keccak } = require('sha3')
const archiver = require('archiver')
const appStatus = require('./applicationStatus')

const userDataPath = app.getPath('userData')

function saveToSigned(json, input, output) {
    return new Promise((resolve, reject) => {
        const data = JSON.stringify(json)
        //create a meta.json file
        fs.writeFileSync(path.join(input, 'meta.json'), data)

        //zip meta.json with its corresponding file and move it to the /signed directory
        const outputPath = output
        const outputDir = fs.createWriteStream(outputPath)
        const archive = archiver('zip')
        outputDir.on('close', err => err ? reject(err) : resolve())
        archive.on('error', err => reject(err))
        archive.pipe(outputDir)
        archive.directory(input, false)
        archive.finalize()
    })
}

function signFile(input, key1, key2, key3) {

    return new Promise( async (resolve, reject) => {
        //read file that should be hashed
        const fileBuffer = fs.readFileSync(input)
        sign(fileBuffer, key1, key2, key3)
        .then(data => resolve(data))
        .catch(err => reject(err))
    })
}

function sign(input, key1, key2, key3, combined) {

    return new Promise( async (resolve, reject) => {

        let hex

        if(combined) {
            //if 2 combined hashes should be hashed together, they are in uint8 array and we need to use ethers' keccak function instead of sha3's
            hex = ethers.utils.keccak256(input)
        } else {

            const hashSum = new Keccak(256)
            hashSum.update(input) //hash input

            //create a hex string
            hex = String(hashSum.digest('hex'))
            hex = "0x" + hex

        }

        try {

            //create wallets with the private keys
            const wallet1 = new ethers.Wallet(key1)
            const wallet2 = new ethers.Wallet(key2)
            const wallet3 = new ethers.Wallet(key3)

            //sign the hash
            const signature1 = await wallet1.signMessage(ethers.utils.arrayify(hex))
            const signature2 = await wallet2.signMessage(ethers.utils.arrayify(hex))
            const signature3 = await wallet3.signMessage(ethers.utils.arrayify(hex))

            //resolve promise with the hash and the signed hashes
            resolve({
                hash: hex,
                signatures: [signature1, signature2, signature3]
            })

        } catch(err) {
            reject(err)
        }

    })

}

function signAll(key1, key2, key3) {

    return new Promise(async (resolve, reject) => {

        var verificatinObject = {}

        //addd the pollID and pollTitle to the verification object
        verificatinObject.pollID = appStatus.pollID
        verificatinObject.pollTitle = appStatus.pollTitle
        verificatinObject.contractAddress = appStatus.address

        try {

            await writePollDotTXT('one')
            await writePollDotTXT('two')
            await zipPDF('one')
            await zipPDF('two')

            //sign addresses
            const sigAddresses = await signFile(path.join(userDataPath, 'addresses', 'addresses.json'), key1, key2, key3)
            verificatinObject.A = sigAddresses

            //sign users
            const sigUsers = await signFile(path.join(userDataPath, 'users', 'users.json'), key1, key2, key3)
            verificatinObject.U = sigUsers

            //sign 1st pdfs
            const sigOne = await signFile(path.join(userDataPath, 'presigned', 'one', 'one.zip'), key1, key2, key3)
            verificatinObject.O = sigOne

            //sign 2nd pdfs
            const sigTwo = await signFile(path.join(userDataPath, 'presigned', 'two', 'two.zip'), key1, key2, key3)
            verificatinObject.T = sigTwo

            //create and sign hash AU
            const sigAU = await sign(ethers.utils.concat([verificatinObject.A.hash, verificatinObject.U.hash]), key1, key2, key3, true)
            verificatinObject.AU = sigAU

            //create and sign hash OT
            const sigOT = await sign(ethers.utils.concat([verificatinObject.O.hash, verificatinObject.T.hash]), key1, key2, key3, true)
            verificatinObject.OT = sigOT

            //create and sign hash AUOT
            const sigAUOT = await sign(ethers.utils.concat([verificatinObject.AU.hash, verificatinObject.OT.hash]), key1, key2, key3, true)
            verificatinObject.AUOT = sigAUOT

            //save the first pdfs that have been signed
            await saveToSigned(verificatinObject, path.join(userDataPath, 'presigned', 'one'), path.join(userDataPath, 'signed', 'FirstActivationLetters.zip'))

            //save the 2nd pdfs that have been signed
            await saveToSigned(verificatinObject, path.join(userDataPath, 'presigned', 'two'), path.join(userDataPath, 'signed', 'SecondActivationLetters.zip'))

            //save the addresses that have been signed
            await saveToSigned(verificatinObject, path.join(userDataPath, 'addresses'), path.resolve(userDataPath, 'signed', 'addresses.zip'))

            //save the users that have been signed
            await saveToSigned(verificatinObject, path.join(userDataPath, 'users'), path.resolve(userDataPath, 'signed', 'users.zip'))

            resolve()

        } catch(err) {
            console.error(err)
            reject(err)
        }
    })
   
}

//zip the pdfs to the /presigned directory
function zipPDF(number) {
    return new Promise((resolve, reject) => {
        const outputPath = path.resolve(userDataPath, 'presigned', number, number + '.zip')
        const output = fs.createWriteStream(outputPath)
        const archive = archiver('zip')
        output.on('close', err => err ? reject(err) : resolve() )
        archive.on('error', err => reject(err))
        archive.pipe(output)
        archive.directory(path.resolve(userDataPath, number), false)
        archive.finalize()
    })
}

//Writes a poll.txt file to the presigned/one and presigned/two directories containing: pollID=[ID*]
function writePollDotTXT(number) {
    return new Promise((res, rej) => {
        const filePath = path.resolve(userDataPath, number, 'poll.txt')
        const fileContent = 'pollID=' + appStatus.pollID + ';pollTitle=' + appStatus.pollTitle + ';contractAddress=' + appStatus.address
        fs.writeFile(filePath, fileContent, (err) => err ? rej(err) : res())
    })
}

module.exports.signAll = signAll