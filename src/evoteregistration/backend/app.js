const fs = require('fs')
const path = require('path')
const { app } = require('electron')

const userDataPath = app.getPath('userData') //Path to the userData directory on the device

const createAccount = require('./createAccount.js')
const importUsers = require('./importCSV.js')
const createPDF = require('./createPDF.js')

const appStatus = require('./applicationStatus')
const { resolve } = require('path')

//import csv file
function startImport(filePath) {

    return new Promise((resolve, reject) => {
        importUsers(filePath)
        .then((json) => {
            //update appStatus
            appStatus.importedUsers = json
            appStatus.importComplete = true
            resolve()
        })
        .catch(err => reject(err))
    })

}

// source: https://snyk.io/blog/nodejs-how-even-quick-async-functions-can-block-the-event-loop-starve-io/
function createAccounts(users) {

    return new Promise(async (resolve, reject) => {

        //Set expected mnemonics in appStatus
        appStatus.expectedMnemonics = users.length

        var userArray = [] 
        var addressesArray = []

        var blockingSince = Date.now()

        //Create mnemonic and address for each user
        for (user in users) {
            //console.log("Imported #" + user)
            const currentUser = users[user]
            const accountDetails = createAccount()
            //Add users and addresses to an array
            addressesArray.push(accountDetails.address)
            userArray.push({...currentUser})
            //Add a mnemonic to the current user
            users[user].mnemonic = accountDetails.mnemonic
            //Increase the generated mnemonics in the appStatus by one to keep track of the progress
            appStatus.generatedMnemonics++
            
            //check if loop blocked app for more than 500ms and unblock if true        
            if ((blockingSince + 500) > Date.now()) {
                await setImmediatePromise()
                blockingSince = Date.now()
            }
        }

        //Shuffle the addresses array
        shuffle(addressesArray)

        //Add Poll id, poll title and smart contract address to arrays
        const metaString = "pollID=" + appStatus.pollID + ";pollTitle=" + appStatus.pollTitle + ";contractAddress=" + appStatus.address
        userArray.unshift(metaString)
        addressesArray.unshift(metaString)

        //Create json file out of the array with the users with addresses and save it
        createUserAndAddressJSONFile(userArray, addressesArray)
        .then(() => {
            createPDF(users)
            .catch(err => reject(err))
            .then(() => resolve())
        })
        .catch(err => reject(err))

    })

}

function setImmediatePromise() {
    //return a promise that is resolved with a setImmediate function (to unblock the event loop)
    return new Promise((resolve) => {
      setImmediate(() => resolve())
    })
}

function createUserAndAddressJSONFile(users, addresses) {

    return new Promise((resolve, reject) => {
        //convert javascript object to json
        const userJsonContent = JSON.stringify(users)
        const userJsonDirectoryName = path.resolve(userDataPath, 'users', 'users.json') //Path where users.json is saved to
        const addressesJsonContent = JSON.stringify(addresses)
        const addressesJsonDirectoryName = path.resolve(userDataPath, 'addresses', 'addresses.json') //Path where addresses.json is saved to

        try {

            fs.writeFileSync(userJsonDirectoryName, userJsonContent, 'utf8')
            fs.writeFileSync(addressesJsonDirectoryName, addressesJsonContent, 'utf8')

            appStatus.jsonCreated = true //update appStatus
            resolve()

        } catch(err) {
            reject(err)
        }

    })
}

//Create the directories where the temporary data is saved at
function createDataDirectories() {

    return new Promise((resolve, reject) => {
        const directories = ['/one', '/two', '/addresses', '/users', '/qrcodes', '/presigned', '/presigned/one', '/presigned/two', '/signed']

        //create all directories in the directories array synchronously in the userData folder
        for (dir in directories) {
            fs.mkdirSync(userDataPath + directories[dir], err => err && reject(err))
        }

        resolve()
    })

}

//https://javascript.info/task/shuffle#:~:text=Write%20the%20function%20shuffle(array,%2C%202%5D%20%2F%2F%20...
function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
}

module.exports.startImport = startImport
module.exports.createAccounts = createAccounts
module.exports.createDataDirectories = createDataDirectories