const fs = require('fs')
const path = require('path')
const { app } = require('electron')

const userDataPath = app.getPath('userData')

const appStatus = require('./applicationStatus')

//delete all the temorary directories that were created
function deleteOutput() {

    return new Promise((resolve, reject) => {
        const directories = ['addresses', 'users', 'qrcodes', 'one', 'two', 'presigned', 'presigned/one', 'presigned/two', 'signed']

        for (dir in directories) {
            fs.rmdirSync(path.resolve(userDataPath, directories[dir]), { recursive: true} , err => err && reject(err))
        }

        resolve()
    })

}

//reset the applicaton status
function resetApplicationStatus() {
    appStatus.importedUsers = null
    appStatus.importComplete = false
    appStatus.jsonCreated = false
    appStatus.finishedPDFs = 0
    appStatus.expectedPDFs = 0
    appStatus.generatedMnemonics = 0
    appStatus.expectedMnemonics = 0
    appStatus.pollID = 0
    appStatus.pollTitle = ""
    appStatus.address = ""
    appStatus.pollDecription = ""
    appStatus.link = ""
}

//resets the application status and deletes the output
function resetAll() {
    return new Promise((resolve, reject) => {
        resetApplicationStatus()
        deleteOutput()
        .then(() => resolve())
        .catch(err => {
            console.error(err)
            reject(err)
        })
    })
}

module.exports.deleteOutput = deleteOutput
module.exports.resetAll = resetAll
module.exports.resetStatus = resetApplicationStatus