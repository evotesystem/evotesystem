const bip39 = require('bip39')
const { ethers } = require('ethers')
bip39.setDefaultWordlist('english')


function createAccount() {
    const mnemonic = bip39.generateMnemonic() //Generate random mnemonic
    const walletMnemonic = new ethers.Wallet.fromMnemonic(mnemonic) //Create wallet from mnemonic
    const address = walletMnemonic.address //Get address from wallet
    return {mnemonic: mnemonic, address: address} //Return mnemonic and public address
}

module.exports = createAccount