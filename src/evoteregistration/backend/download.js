const fs = require('fs')
const path = require('path')
const { app } = require('electron')

const downloadPath = app.getPath('downloads')
const userDataPath = app.getPath('userData')

//Move addresses.zip file from /signed to downloads
function downloadAddresses() {
    return new Promise((resolve, reject) => {
        fs.copyFile(path.resolve(userDataPath, 'signed/addresses.zip'), path.resolve(downloadPath, 'addresses.zip'), (err) => {
            resolve(err)
        })
    })
}

//Move users.zip from /signed to downloads
function downloadUsers() {
    return new Promise((resolve, reject) => {
        fs.copyFile(path.resolve(userDataPath, 'signed/users.zip'), path.resolve(downloadPath, 'users.zip'), (err) => {
            resolve(err)
        })
    })
}

//Move zipped pdfs from /signed to downloads
function downloadPDF(number) {
    return new Promise((resolve, reject) => {
        let fileName 
        if (number === 'one') {
            fileName = 'FirstActivationLetters'
        } else {
            fileName = 'SecondActivationLetters'
        }
        fs.copyFile(path.join(userDataPath, 'signed', fileName + '.zip'), path.join(downloadPath, fileName + '.zip'), (err) => {
            resolve(err)
        })
    })
}


module.exports.downloadPDF = downloadPDF
module.exports.downloadUsers = downloadUsers
module.exports.downloadAddresses = downloadAddresses