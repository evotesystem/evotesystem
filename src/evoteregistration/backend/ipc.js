const { ipcMain, dialog } = require('electron')

const { startImport, createAccounts, createDataDirectories } = require('./app')
const { resetAll } = require('./reset.js')
const appStatus = require('./applicationStatus')
const { downloadPDF, downloadAddresses, downloadUsers } = require('./download')
const { signAll } = require('./signOutput')

var window

//Reset Application on start
resetAll().catch(err => handleError(err))

//Saves the poll info
ipcMain.on('SAVE_POLL_INFO', (e, args) => {

    //make sure the application was reset
    resetAll()
    .then(() => {

        //set the app status
        appStatus.pollTitle = args.title
        appStatus.pollID = args.id
        appStatus.address = args.address
        appStatus.pollDescription = args.description
        appStatus.votingLink = args.link

        //return true
        e.returnValue = true
    })
    .catch(err => handleError(err))

})

//Starts the user import
ipcMain.on('START_IMPORT', (e, args) => {
    if (args) {
        //confirm the frontend that the import is started
        e.returnValue = 200
        //start import of the csv file
        startImport(args).catch(err => handleError(err))
        //Create temporary data directories
        createDataDirectories().catch(err => handleError(err))
    } else {
        //tell the frontend that something went wrong
        e.returnValue = 500
        handleError(new Error("Failed to start user import"))
    }
})

//returns the imported users
ipcMain.on('GET_IMPORTED_USERS', (e, args) => {
    if (appStatus.importedUsers && appStatus.importComplete) {
        e.returnValue = appStatus.importedUsers
    } else {
        e.returnValue = null
    }
})

ipcMain.on('CREATE_ACCOUNTS', (e, args) => {
    if (appStatus.importedUsers && appStatus.importComplete) {
        //only start import when the appStatus exists
        if (appStatus.expectedMnemonics > 0) {
            //if there are more then 0 expected mnemonics, create accounts has already been called and an error is returned
            e.returnValue = {error: true}
        } else {
            e.returnValue = {error: false}
            //start account creation
            createAccounts(appStatus.importedUsers)
            .catch(err => handleError(err))
        }
    } else {
        e.returnValue = {error: true}
    }
})

//send appStatus to renderer process
ipcMain.on('APP_STATUS', (e, args) => {
    const response = {
        importComplete: appStatus.importComplete,
        jsonCreated: appStatus.jsonCreated,
        finishedPDFs: appStatus.finishedPDFs,
        expectedPDFs: appStatus.expectedPDFs,
        generatedMnemonics: appStatus.generatedMnemonics,
        expectedMnemonics: appStatus.expectedMnemonics,
        pollID: appStatus.pollID,
        pollTitle: appStatus.pollTitle
    }
    e.reply('APP_STATUS_RESPONSE', response)
    e.returnValue = response
})

//sign output
ipcMain.on('SIGN_OUTPUT', (e, args) => {
    const key1 = args.key1
    const key2 = args.key2
    const key3 = args.key3
    signAll(key1, key2, key3)
    .then(() => {
        e.reply('OUTPUT_SIGNED', null)
    })
    .catch(err => {
        handleError(err)
        e.reply('OUTPUT_SIGNED', err)
    })
})

//download files
ipcMain.on('DOWNLOAD_FILES', (e, args) => {
    const requested = args.requested //requested is the file that should be downloaded

    if (requested === 'one' || requested === 'two') {

        //download pdf
        downloadPDF(requested).then((err) => {
            e.reply('FILE_SAVED', {error: err, requested: requested, message: args.message})
            err && handleError(err)
        })

    } else if (requested === 'addresses') {
        //download addresses
        downloadAddresses().then((err) => {
            e.reply('FILE_SAVED', {error: err, requested: requested, message: args.message})
            err && handleError(err)
        })
    } else if (requested === 'users') { 
        //download users
        downloadUsers().then((err) => {
            e.reply('FILE_SAVED', {error: err, requested: requested, message: args.message})
            err && handleError(err)
        })
    } else {

        //send error when neither a pdf nor the addresses are requested
        e.reply('FILE_SAVED', {error: new Error("No file requested")})

    }
})

//reset application
ipcMain.on('RESET', (e, args) => {
    resetAll().catch(err => handleError(err))
    e.returnValue = null
})

//Handle Errors that may be encountered
function handleError(err) {
    dialog.showMessageBox({message: "Error: " + err.message, type: "error"}) //show message box with the error message
    console.error(err) //log the error
    if (window) window.webContents.send("ERROR", "") //send reset event to the renderer process
    resetAll().catch(err => console.error(err)) //reset application
}

//assign window to window variable
function setWindow(win) {
    window = win
}

module.exports.setWindow = setWindow