const fs = require('fs')
const path = require('path')
const csv = require('csvtojson')

//import users from csv file and convert them to json
function importUsers(filePath) {
    return new Promise((resolve, reject) => {
        csv().fromFile(path.resolve(__dirname, filePath))
            .then((json) => {
                //check if headers are correct
                const headers = ['firstName', 'lastName', 'street', 'zip', 'city', 'canton', 'country']
                for(var i = 0; i < headers.length; i++) {
                    if(!json[0].hasOwnProperty(headers[i])) return reject(new Error(`${headers[i]} header is missing in the csv file`))
                }
                //check that no values are missing
                for(var i = 0; i < json.length; i++) {
                    if(!(json[i].firstName.length > 0)) return reject(new Error(`User with index ${i} has a missing firstName value`))
                    if(!(json[i].lastName.length > 0)) return reject(new Error(`User with index ${i} has a missing lastName value`))
                    if(!(json[i].street.length > 0)) return reject(new Error(`User with index ${i} has a missing street value`))
                    if(!(json[i].zip.length > 0)) return reject(new Error(`User with index ${i} has a missing zip value`))
                    if(!(json[i].city.length > 0)) return reject(new Error(`User with index ${i} has a missing city value`))
                    if(!(json[i].canton.length > 0)) return reject(new Error(`User with index ${i} has a missing canton value`))
                    if(!(json[i].country.length > 0)) return reject(new Error(`User with index ${i} has a missing country value`))
                }
                resolve(json)
            })
            .catch(err => reject(err))
    })
}

module.exports = importUsers