const PDFDocument = require('pdfkit')
const fs = require('fs')
const path = require('path')
const QRCode = require('qrcode')
const { v4: uuidv4 } = require('uuid')
const { app } = require('electron')

const userDataPath = app.getPath('userData')

const appStatus = require('./applicationStatus')


//create two pdfs for each user
function createDocuments(users) {

    return new Promise(async (resolve, reject) => {

        //update the appStatus
        appStatus.expectedPDFs = 2 * users.length //the application generates two pdfs for each user
        appStatus.finishedPDFs = 0 //initially 0 pdfs are finished

        for (user in users) {
            const currentUser = users[user]
            const processedMnemonic = splitMnemonic(currentUser.mnemonic) //split mnemonic in two parts
            try {
                await createDocument('one', currentUser, processedMnemonic.mnemonicOne) //wait until the first pdf is saved and then continue
                await createDocument('two', currentUser, processedMnemonic.mnemonicTwo) //wait until the second pdf is saved and then continue
            } catch(err) {
                reject(err)
            }
        }

        resolve()

    })
        
}

function createDocument(number, user, mnemonic) {

    return new Promise(async (resolve, reject) => {

        const document = new PDFDocument({
            size: [1240,1754],
            font: 'Helvetica'
        })

        const fileName = number + "_" + user.zip + "_" + user.firstName + "_" + user.lastName + "_" + uuidv4() + ".pdf" //filename of the pdf that will be generated
        const filePath = path.resolve(userDataPath, number, fileName) //path of the file that will be generated

        //create a writeStream
        document.pipe(fs.createWriteStream(filePath).on('finish', () => {
            //when the write stream is finished (pdf is saved) the finishedPdfs are increased by 1 
            appStatus.finishedPDFs++
            resolve() //Resolve Promise once the document finished saving the file
        }))

        //title text
        document.font(path.resolve(__dirname, 'resources/Helvetica-Neue-Bold.ttf')).fontSize(70).fillColor('black').text("eVote Access", 108, 258)

        //letter number
        var letterNumber = "One"
        if (number === "two") letterNumber = "Two"
        document.fontSize(50).fillColor('#656565').text("Letter " + letterNumber)

        //greeting text
        document.fontSize(30).fillColor('#7E88FF').text("Dear " + user.firstName + " " + user.lastName, 108, 494)

        //poll Description
        document.font(path.resolve(__dirname, 'resources/Helvetica-Neue-Medium.ttf')).fontSize(30).fillColor('#898989').text(appStatus.pollDescription, 108, 543, { width: 1024, align: 'left'})
        const descriptionHeight = document.fontSize(30).heightOfString(appStatus.pollDescription, { width: 1024, align: 'left' })
        const pollNameTextY = 543 + descriptionHeight + 30

        //place the voting link 
        if(appStatus.votingLink !== "") {
            let linkWidth = document.widthOfString(`Visit ${appStatus.votingLink} to vote`)
            let linkX = (1240/2) - (linkWidth/2)
            document.fillColor('#898989').text(`Visit `, linkX, 1625, {continued: true}).fillColor('#7E88FF').text(appStatus.votingLink, { continued: true}).fillColor('#898989').text(' to vote')
        }

        //the address of the user
        const addressX = 700
        const addressStartingY = 265
        document.fontSize(25).fillColor('black').text(user.firstName + ' ' + user.lastName, addressX, addressStartingY)
        document.fontSize(25).fillColor('black').text(user.street, addressX)
        document.fontSize(25).fillColor('black').text(user.zip + ' ' + user.city)
        document.fontSize(25).fillColor('black').text(user.country)

        //poll name text
        document.fontSize(30).fillColor('#656565').text("Poll Name", 108, pollNameTextY)
        //poll id text
        document.fontSize(30).fillColor('#656565').text("Poll ID", 932, pollNameTextY)

        //poll name background
        document.rect(108, pollNameTextY + 51, 774, 87).fillAndStroke("#EFEFEF")
        //poll id background
        document.rect(932, pollNameTextY + 51, 200, 87).fillAndStroke("#EFEFEF")
        //mnemonic background
        document.rect(108, pollNameTextY + 237, 1024, 87).fillAndStroke("#EFEFEF")

        //place poll name and poll id on the pdf

        var pollNameTextSize = 30 //make sure the pollTitle fits in the rectangle
        while (document.fontSize(pollNameTextSize).widthOfString(appStatus.pollTitle) > (774 - 50)) {
            pollNameTextSize--    
        }
        document.fontSize(pollNameTextSize).fillColor('black').text(appStatus.pollTitle, 133, pollNameTextY + 74)
        document.fontSize(30).fillColor('black').text(appStatus.pollID, 957, pollNameTextY + 74)

        //place mnemonic on the pdf
        document.fontSize(30).fillColor('black').text(mnemonic, 0, pollNameTextY + 260, {
            align: 'center'
        })

        //mnemonic text
        document.fontSize(30).fillColor('#656565').text("Mnemonic " + letterNumber, 108, pollNameTextY + 188)

        //qrcode text
        document.text("Mnemonic QR Code", 108, pollNameTextY + 188 + 185)

        //tell the qrcode which part of the mnemonic it contains
        let part = 1
        if (number === 'two') part = 2
        //wait until the qrcode is created
        const fName = await createQRCode(mnemonic, part, appStatus.address).catch(err => reject(err))

        const qrCodePath = path.resolve(userDataPath, 'qrcodes', fName)

        //Place qrcode on the pdf
        document.image(qrCodePath, 108, pollNameTextY + 188 + 185 + 50, {width: 275, size: 275})

        //delete qrcode once it is placed on the pdf
        fs.unlinkSync(qrCodePath, (err) => err && reject(err))

        //end the creation of the pdf
        document.end()

    })
}

//Split mnemonic in two parts for the activation letters
function splitMnemonic(mnemonic) {
    const wordsArray = mnemonic.split(" ")
    const wordsOneArray = wordsArray.splice(0, 6)

    const wordsOne = wordsOneArray.join(' ')
    const wordsTwo = wordsArray.join(' ')

    return {
        mnemonicOne: wordsOne,
        mnemonicTwo: wordsTwo,
        mnemonicOneArray: wordsOneArray,
        mnemonicTwoArray: wordsArray
    }
}

//Added promise to this function to make it asynchronous
function createQRCode(mnemonic, part, address) {
    return new Promise(async (resolve, reject) => {
        const fileName = uuidv4() + '.png' //give the qrcode a random filename
        const fileData = {
            pollID: appStatus.pollID,
            part: part,
            mnemonic: mnemonic,
            address: address
        }
        const fileJSON = JSON.stringify(fileData)
        await QRCode.toFile(
            path.resolve(userDataPath, 'qrcodes', fileName),
            fileJSON,
            { 
                errorCorrectionLevel: 'M',
                margin: 0,
                color: {
                    light: '#0000'
                }
            }
        )
        .catch(err => reject(err))

        resolve(fileName) //resolve with the filename
    })
}

module.exports = createDocuments