const {
    contextBridge,
    ipcRenderer
} = require("electron");

//https://dennistretyakov.com/ipc-render-in-cra-managed-app

// expose parts of the ipcRenderer API to the renderer process
contextBridge.exposeInMainWorld(
    "electron", {
        send: (channel, data) => {
            //ipcRenderer's send function
            ipcRenderer.send(channel, data)
        },
        sendSync: (channel, data) => {
            //ipcRenderer's sendSync function
            return ipcRenderer.sendSync(channel, data)
        },
        subscribe: (channel, listener) => {
            const subscription = (event, ...args) => listener(...args);
            //ipcRenderer's on funciton
            ipcRenderer.on(channel, subscription)

            return () => {
                ipcRenderer.removeListener(channel, subscription)
            }
        }
    }
)