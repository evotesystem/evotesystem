import { useContext, useState, useEffect } from "react";
import {
  Jumbotron,
  Container,
  Alert,
  ListGroup,
  ListGroupItem,
  FormText,
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
  Spinner,
} from "reactstrap";
import { toast } from "react-toastify";

import { ApplicationContext } from "../helper/Context";

function Poll() {
  const { vc } = useContext(ApplicationContext);

  const [pageMessage, setPageMessage] = useState(false);

  const [selectedOption, setSelectedOption] = useState(0);

  //confirm vote modal
  const [modal, setModal] = useState(false);
  const toggleModal = () => {
    setModal(!modal);
  };
  const [modalLoading, setModalLoading] = useState(false);

  const vote = () => {
    setPageMessage(false);
    setModalLoading(true);

    vc.voteOnPoll(selectedOption)
      .then(() => {
        setPageMessage(false);
        toast.info(
          "Transaction has been relayed. Please wait until it is processed."
        );
      })
      .catch((err) => {
        if (err.message.length > 50) {
          toast.error("Failed to relay request");
          setPageMessage({
            color: "danger",
            message: `Failed to relay request`,
          });
        } else {
          toast.error(err.message);
          setPageMessage({
            color: "danger",
            message: `Failed to relay request: ${err.message}`,
          });
        }

        console.error(err);

        setModal(false);
        setModalLoading(false);
      });
  };

  const downloadVote = () => {
    let chainvoteFileContent = {
      ...vc.vote,
    };

    //delete unneccessary content
    delete chainvoteFileContent.cast;
    delete chainvoteFileContent.submitted;
    delete chainvoteFileContent.counted;

    //create blob
    const blob = new Blob([JSON.stringify(chainvoteFileContent)]);
    const fileDownloadUrl = URL.createObjectURL(blob);

    // Code from: https://stackoverflow.com/a/63965930
    const link = document.createElement("a");
    link.href = fileDownloadUrl;
    link.setAttribute(
      "download",
      `${vc.poll.pollName}_${vc.vote.address}.chainvote`
    );

    // Append to html link element page
    document.body.appendChild(link);

    // Start download
    link.click();

    // Clean up and remove the link
    link.parentNode.removeChild(link);

    // Go to the verify vote page
    vc.setVoterStatus(2);
  };

  useEffect(() => {
    if (!vc.vote) return;

    if (vc.vote.cast) {
      setModal(false);
      setModalLoading(false);
    }
  }, [vc.vote]);

  useEffect(() => {
    if (!vc.poll) return;

    if (vc.poll.status.id > 4) {
      setPageMessage({
        color: "warning",
        message: "Poll is closed. You can't vote on it anymore.",
      });
    } else if (vc.poll.status.id === 4) {
      setPageMessage(false);
    } else if (vc.poll.status.id < 4) {
      setPageMessage({
        color: "warning",
        message: "Poll isn't open yet. You can't vote on it yet.",
      });
    }
  }, [vc.poll]);

  return (
    <>
      <Jumbotron fluid>
        {selectedOption > 0 && (
          <SubmitPollModal
            modal={modal}
            toggle={toggleModal}
            loading={modalLoading}
            option={vc.poll.options[selectedOption - 1]}
            vote={vote}
          />
        )}

        <Container>
          <h2>{vc.poll ? vc.poll.pollName : "Vote"}</h2>

          {pageMessage && (
            <>
              <Alert color={pageMessage.color}>{pageMessage.message}</Alert>
            </>
          )}

          {vc.poll ? (
            <>
              {!vc.vote.cast ? (
                <>
                  <PollInfo vc={vc} />
                  <VoteOptions
                    vc={vc}
                    selectedOption={selectedOption}
                    setSelectedOption={setSelectedOption}
                    toggleModal={toggleModal}
                  />
                </>
              ) : (
                <VoteCast
                  vc={vc}
                  downloadVote={downloadVote}
                  selectedOption={selectedOption}
                />
              )}
            </>
          ) : (
            <div className="align-content-center">
              <Spinner color="primary" />
            </div>
          )}
        </Container>
      </Jumbotron>
    </>
  );
}

function PollInfo(props) {
  return (
    <>
      <h5>Poll Info</h5>
      <ListGroup>
        <ListGroupItem>
          Poll ID: <strong>#{props.vc.pollID}</strong>
        </ListGroupItem>
        <ListGroupItem>
          Opening Time:{" "}
          <strong>
            {new Date(props.vc.poll.openingTime * 1000).toLocaleString()}
          </strong>
        </ListGroupItem>
        <ListGroupItem>
          Closing Time:{" "}
          <strong>
            {new Date(props.vc.poll.closingTime * 1000).toLocaleString()}
          </strong>
        </ListGroupItem>
        <ListGroupItem>
          Tallying Time:{" "}
          <strong>
            {new Date(props.vc.poll.countTime * 1000).toLocaleString()}
          </strong>
        </ListGroupItem>
      </ListGroup>
    </>
  );
}

function VoteOptions(props) {
  return (
    <>
      <br />
      <h5>Vote Options</h5>
      {props.vc.poll.options ? (
        <>
          <ListGroup>
            {props.vc.poll.options.map((o) => (
              <ListGroupItem
                key={o.id}
                tag="button"
                action
                active={props.selectedOption === o.id}
                onClick={() => props.setSelectedOption(o.id)}
              >
                Option #{o.id}: <strong>{o.optionName}</strong>
              </ListGroupItem>
            ))}
          </ListGroup>
          <FormText color="muted">Click to select an option</FormText>
        </>
      ) : (
        <div className="align-content-center">
          <Spinner color="primary" />
        </div>
      )}
      {props.vc.poll.status.id === 4 && (
        <div className="vote-button">
          <Button
            block
            color="primary"
            disabled={!(props.selectedOption > 0)}
            onClick={props.toggleModal}
          >
            Submit Vote
          </Button>
        </div>
      )}
    </>
  );
}

function VoteCast(props) {
  return (
    <div>
      <Alert color="success">
        <h4>Vote Cast Successfully</h4>
      </Alert>
      <ListGroup className="word-break">
        <ListGroupItem>
          You voted for{" "}
          <strong>
            option #{props.vc.poll.options[props.selectedOption - 1].id}
            {": "}
            {props.vc.poll.options[props.selectedOption - 1].optionName}
          </strong>
        </ListGroupItem>
        <ListGroupItem>
          The uuid that was used to generate your vote is:{" "}
          <strong>{props.vc.vote.uuid}</strong>
        </ListGroupItem>
        <ListGroupItem>
          The hash of your vote is: <strong>{props.vc.vote.hash}</strong>
        </ListGroupItem>
        <ListGroupItem>
          Your vote was cast from address:{" "}
          <strong>{props.vc.vote.address}</strong>
        </ListGroupItem>
      </ListGroup>
      <br />
      <p>
        If you want to completely verify that your vote has been counted at the
        end of the election, you can download all this information by clicking
        the button below.
      </p>
      <Button color="success" block onClick={props.downloadVote}>
        Download Vote and View Details
      </Button>
    </div>
  );
}

function SubmitPollModal(props) {
  return (
    <>
      <Modal isOpen={props.modal} toggle={props.toggle}>
        {props.loading ? (
          <>
            <ModalHeader toggle={props.toggle}>Vote is being cast</ModalHeader>
            <ModalBody className="align-content-center">
              <Spinner color="primary" />
              <p>Please wait. This may take a while.</p>
            </ModalBody>
          </>
        ) : (
          <>
            <ModalHeader toggle={props.toggle}>Confirm Vote</ModalHeader>
            <ModalBody>
              <h5>
                You are going to vote for option {props.option.id}: "
                {props.option.optionName}"
              </h5>
            </ModalBody>
            <ModalFooter>
              <Button outline color="danger" onClick={props.toggle}>
                Cancel
              </Button>{" "}
              <Button color="primary" onClick={props.vote}>
                Definetly Submit Vote
              </Button>
            </ModalFooter>
          </>
        )}
      </Modal>
    </>
  );
}

export default Poll;
