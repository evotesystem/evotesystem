import { useEffect, useState } from "react";
import QrReader from "react-qr-reader";
import { Container, Jumbotron, Alert, Button } from "reactstrap";

function QRCode(props) {
  const [status, setStatus] = useState({
    one: {
      scanned: false,
      pollID: 0,
      mnemonic: "",
    },
    two: {
      scanned: false,
      pollID: 0,
      mnemonic: "",
    },
  });

  const handleError = (err) => {
    console.error(err);
    props.setError({ message: err.message, color: "danger" });
    props.showScanner(false);
  };

  const handleScan = (data) => {
    if (data) {
      console.log(data);
      let parsedData = JSON.parse(data);

      if (parsedData.pollID && parsedData.part && parsedData.mnemonic) {
        if (parsedData.part === 1) {
          setStatus((prev) => {
            return {
              ...prev,
              one: {
                scanned: true,
                pollID: Number(parsedData.pollID),
                mnemonic: parsedData.mnemonic,
              },
            };
          });
        } else if (parsedData.part === 2) {
          setStatus((prev) => {
            return {
              ...prev,
              two: {
                scanned: true,
                pollID: Number(parsedData.pollID),
                mnemonic: parsedData.mnemonic,
              },
            };
          });
        } else {
          props.setError({ message: "Invalid QR Code", color: "danger" });
          props.showScanner(false);
        }
      } else {
        props.setError({ message: "Invalid QR Code", color: "danger" });
        props.showScanner(false);
      }
    }
  };

  useEffect(() => {
    if (status.one.scanned && status.two.scanned) {
      if (status.one.pollID === status.two.pollID) {
        props.login({
          pollID: status.one.pollID,
          mnemonicOne: status.one.mnemonic,
          mnemonicTwo: status.two.mnemonic,
        });
        props.showScanner(false);
      } else {
        props.showScanner(false);
        props.setError({ message: "Poll IDs don't match", color: "danger" });
      }
    }

    // eslint-disable-next-line
  }, [status]);

  useEffect(() => {
    var parentElement = document.getElementById("qrdiv");
    parentElement.firstChild.classList.add("qrsection");
  }, []);

  return (
    <div id="overlay">
      <Jumbotron fluid>
        <Container className="align-content-center">
          <h1 style={{ paddingBottom: "15px" }}>Scan QR Codes</h1>
          {(status.one.scanned || status.two.scanned) && (
            <Alert
              style={{ maxWidth: "400px", display: "inline-block" }}
              color="primary"
            >
              Scanned activation letter #{status.one.scanned ? "One" : "Two"}.
              Please scan activation letter #
              {status.one.scanned ? "Two" : "One"} now.
            </Alert>
          )}
          <div id="qrdiv">
            <QrReader
              delay={500}
              onError={handleError}
              onScan={handleScan}
              style={{ width: "100%", maxWidth: "400px" }}
            />
          </div>
          <Button
            style={{ maxWidth: "400px", display: "inline-block" }}
            block
            outline
            color="danger"
            className="qrcode-cancel-button"
            onClick={() => props.showScanner(false)}
          >
            Cancel
          </Button>
        </Container>
      </Jumbotron>
    </div>
  );
}

export default QRCode;
