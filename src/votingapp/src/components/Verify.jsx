import { useContext, useEffect, useState } from "react";
import {
  Alert,
  Container,
  Collapse,
  Jumbotron,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText,
  Spinner,
  List,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  Badge,
} from "reactstrap";
import { ApplicationContext } from "../helper/Context";
import { toast } from "react-toastify";
import verifyMetaFile from "../utils/verifyMetaFile";

function Verify() {
  const { vc } = useContext(ApplicationContext);

  const [errorMessage, setErrorMessage] = useState(false);

  const [fileUploadModal, setFileUploadModal] = useState(false);
  const toggleFileUploadModal = () => {
    setFileUploadModal(!fileUploadModal);
  };

  const [showVoteDetails, setShowVoteDetails] = useState(false);
  const toggleShowVoteDetails = () => setShowVoteDetails(!showVoteDetails);

  const [showVoteCount, setShowVoteCount] = useState(false);

  const checkIfVotesCounted = () => {
    let overallCountedVotes = 0;
    for (let i = 0; i < vc.poll.options.length; i++) {
      overallCountedVotes += vc.poll.options[i].votes;
    }
    if (overallCountedVotes > 0) return true;
    return false;
  };

  useEffect(() => {
    //get the voted event on component mount
    toast.loading("Getting vote");
    vc.getVotedEvent()
      .then(() => toast.dismiss())
      .catch((err) => {
        console.error(err);
        toast.dismiss();
        toast.error(err.message);
        setErrorMessage(err.message);
        toast.info("You will be logged out in 10 seconds", { autoClose: 10 });
        setTimeout(() => vc.logout(), 10000);
      });

    //cleanup function
    return () => toast.dismiss();

    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (!vc.poll.options) return;
    checkIfVotesCounted() ? setShowVoteCount(true) : setShowVoteCount(false);

    // eslint-disable-next-line
  }, [vc.poll]);

  return (
    <>
      <Jumbotron fluid>
        <Container>
          <h1>Verify Vote</h1>

          {!vc.vote.loaded ? (
            <div className="align-content-center">
              <Spinner color="primary" />
            </div>
          ) : (
            <>
              {vc.vote.counted ? (
                <Alert color="success">
                  Your vote has been successfully counted
                </Alert>
              ) : (
                <Alert color="primary">Your vote has been cast</Alert>
              )}
            </>
          )}
          {errorMessage && <Alert color="danger">Error: {errorMessage}</Alert>}

          {vc.vote.loaded && (
            <>
              <UploadFileModal
                modal={fileUploadModal}
                toggle={toggleFileUploadModal}
                vote={vc.vote.args}
              />
              <h3>Poll Info</h3>
              <ListGroup className="word-break">
                <ListGroupItem>
                  <strong>Name:</strong> {vc.poll.pollName}
                </ListGroupItem>
                <ListGroupItem>
                  <strong>ID:</strong> {vc.poll.pollID}
                </ListGroupItem>
                <ListGroupItem>
                  <strong>Tallying Time:</strong>{" "}
                  {new Date(vc.poll.countTime * 1000).toLocaleString()}
                </ListGroupItem>
                {vc.poll.options && (
                  <ListGroupItem>
                    <strong>Vote Options</strong>
                    <ListGroupItemText>
                      <List>
                        {vc.poll.options.map((o) => (
                          <li key={o.id}>
                            Option #{o.id}: {o.optionName}{" "}
                            {showVoteCount && <>({o.votes} Votes)</>}
                          </li>
                        ))}
                      </List>
                    </ListGroupItemText>
                  </ListGroupItem>
                )}
              </ListGroup>
              <br />
              <h3>Vote Details</h3>
              <ListGroup className="word-break">
                <ListGroupItem>
                  <ListGroupItemHeading>
                    Voter Address{" "}
                    <Badge color="info" pill>
                      Your Address
                    </Badge>{" "}
                  </ListGroupItemHeading>
                  <ListGroupItemText>{vc.vote.args._voter}</ListGroupItemText>
                </ListGroupItem>

                <ListGroupItem>
                  <ListGroupItemHeading>Vote Hash</ListGroupItemHeading>
                  <ListGroupItemText>
                    {vc.vote.args._voteHash}
                  </ListGroupItemText>
                </ListGroupItem>
                <Collapse isOpen={showVoteDetails}>
                  <ListGroupItem>
                    <ListGroupItemHeading>Encrypted Vote</ListGroupItemHeading>
                    <ListGroupItemText>{vc.vote.args._vote}</ListGroupItemText>
                  </ListGroupItem>

                  <ListGroupItem>
                    <ListGroupItemHeading>Block</ListGroupItemHeading>
                    <ListGroupItemText>
                      <List>
                        <li>Block Number: {vc.vote.blockNumber}</li>
                        <li>Block Hash: {vc.vote.blockHash}</li>
                      </List>
                    </ListGroupItemText>
                  </ListGroupItem>

                  <ListGroupItem>
                    <ListGroupItemHeading>Transaction</ListGroupItemHeading>
                    <ListGroupItemText>
                      <List>
                        <li>Transaction Hash: {vc.vote.transactionHash}</li>
                        <li>Transaction Index: {vc.vote.transactionIndex}</li>
                      </List>
                    </ListGroupItemText>
                  </ListGroupItem>
                </Collapse>
              </ListGroup>
              <p>
                <Button color="link" size="sm" onClick={toggleShowVoteDetails}>
                  {showVoteDetails ? "Show less" : "Show more"}
                </Button>
              </p>
              <Button
                color="primary"
                block
                onClick={() => toggleFileUploadModal()}
              >
                Verify .chainvote File
              </Button>
            </>
          )}
        </Container>
      </Jumbotron>
    </>
  );
}

function UploadFileModal(props) {
  const { vc } = useContext(ApplicationContext);

  const [message, setMessage] = useState(false);

  const [result, setResult] = useState(false);

  const [file, setFile] = useState(false);

  const [spinner, showSpinner] = useState(false);

  const fileSelected = (e) => {
    showSpinner(true);

    e.target.files[0].text().then((data) => {
      const voteFile = JSON.parse(data);
      setFile(voteFile);

      verifyMetaFile(voteFile, vc)
        .then((res) => {
          showSpinner(false);
          setResult(res);

          if (
            res.hash &&
            res.voteString &&
            res.signature &&
            res.encryptedVote
          ) {
            setMessage({
              message: "Vote successfully verified",
              color: "success",
            });
          } else {
            setMessage({ message: "Vote isn't verified", color: "danger" });
          }
        })
        .catch((err) => {
          console.log(err);
          showSpinner(false);
          setMessage({ message: err.message, color: "danger" });
          toast.error(err.message);
        });
    });
  };

  const cancelClicked = () => {
    setFile(false);
    setResult(false);
    showSpinner(false);
    setMessage(false);
    props.toggle();
  };

  return (
    <>
      <Modal isOpen={props.modal} toggle={props.toggle}>
        <ModalHeader toggle={props.toggle}>Verify .chainvote File</ModalHeader>
        <ModalBody>
          {message && (
            <Alert color={message.color}>
              {message.color === "danger" && "Error:"} {message.message}
            </Alert>
          )}

          {spinner ? (
            <div className="align-content-center">
              <Spinner color="primary" />
            </div>
          ) : (
            <>
              {!result ? (
                <Input
                  type="file"
                  accept=".chainvote"
                  onChange={fileSelected}
                />
              ) : (
                <>
                  <ListGroup className="word-break">
                    <ListGroupItem>
                      <ListGroupItemHeading>Vote Info</ListGroupItemHeading>
                      <ListGroupItemText>
                        <List>
                          <li>Poll ID: {file.pollID}</li>
                          <li>Vote Option ID: {file.optionID}</li>
                          <li>UUID: {file.uuid}</li>
                        </List>
                      </ListGroupItemText>
                    </ListGroupItem>
                    <ListGroupItem>
                      Vote Hash{" "}
                      {result.hash ? (
                        <Badge pill color="success">
                          Correct
                        </Badge>
                      ) : (
                        <Badge pill color="danger">
                          Incorrect
                        </Badge>
                      )}
                    </ListGroupItem>
                    <ListGroupItem>
                      Encrypted Vote{" "}
                      {result.encryptedVote ? (
                        <Badge pill color="success">
                          Correct
                        </Badge>
                      ) : (
                        <Badge pill color="danger">
                          Incorrect
                        </Badge>
                      )}
                    </ListGroupItem>
                    <ListGroupItem>
                      Signature{" "}
                      {result.signature ? (
                        <Badge pill color="success">
                          Correct
                        </Badge>
                      ) : (
                        <Badge pill color="danger">
                          Incorrect
                        </Badge>
                      )}
                    </ListGroupItem>
                  </ListGroup>
                </>
              )}
            </>
          )}
        </ModalBody>
        <ModalFooter>
          <Button color="danger" outline onClick={cancelClicked}>
            Cancel
          </Button>{" "}
          <Button color="primary" disabled={!result} onClick={cancelClicked}>
            Done
          </Button>
        </ModalFooter>
      </Modal>
    </>
  );
}

export default Verify;
