import { Spinner } from "reactstrap";

function Loading() {
  return (
    <div className="align-content-center">
      <br />
      <Spinner color="primary" />
    </div>
  );
}

export default Loading;
