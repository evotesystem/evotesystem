import {
  Jumbotron,
  Container,
  Form,
  FormGroup,
  FormText,
  Label,
  Button,
  Alert,
  Spinner,
} from "reactstrap";
import { useForm } from "react-hook-form";
import { useContext, useState } from "react";
import { toast } from "react-toastify";

import QRCode from "./QRCode";

import { ApplicationContext } from "../helper/Context";

function Login(props) {
  const { vc } = useContext(ApplicationContext);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const [showQrScanner, setShowQrScanner] = useState(false);

  const [loadingSpinner, showLoadingSpinner] = useState(false);

  const [errorMessage, setErrorMessage] = useState(false);

  const onSubmit = (data) => {
    setErrorMessage(false);
    showLoadingSpinner(true);

    setTimeout(() => {
      vc.login(data.pollID, data.mnemonicOne, data.mnemonicTwo).catch((err) => {
        console.error(err);
        showLoadingSpinner(false);
        setErrorMessage({ message: err.message, color: "danger" });
        toast.error("Failed to login");
      });
    }, 50);
  };

  return (
    <div>
      {showQrScanner && (
        <QRCode
          setError={setErrorMessage}
          showScanner={setShowQrScanner}
          login={onSubmit}
        />
      )}
      <Jumbotron className="jumbotron-full-height" fluid>
        <Container>
          <h1>Login</h1>
          {errorMessage && (
            <>
              <Alert color={errorMessage.color}>
                Error: {errorMessage.message}
              </Alert>
            </>
          )}

          {loadingSpinner ? (
            <div className="align-content-center">
              <Spinner color="primary" />
            </div>
          ) : (
            <>
              <h6>Please enter the login credentials you received by mail</h6>
              <Form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
                <FormGroup>
                  <Label for="pollIdInput">Poll ID</Label>
                  <input
                    id="pollIdInput"
                    type="number"
                    className={`form-control ${errors.pollID && "is-invalid"}`}
                    {...register("pollID", { required: true })}
                  />
                  {errors.pollID && (
                    <div className="invalid-feedback">
                      Please enter a valid poll id
                    </div>
                  )}
                  <FormText color="muted">
                    The number in the poll id field of the activation letter
                  </FormText>
                </FormGroup>
                <FormGroup>
                  <Label for="mnemonicOneInput">Mnemonic One</Label>
                  <input
                    id="mnemonicOneInput"
                    type="text"
                    className={`form-control ${
                      errors.mnemonicOne && "is-invalid"
                    }`}
                    {...register("mnemonicOne", { required: true })}
                  />
                  {errors.mnemonicOne && (
                    <div className="invalid-feedback">Field required</div>
                  )}
                  <FormText color="muted">
                    The mnemomic on the first activation letter
                  </FormText>
                </FormGroup>
                <FormGroup>
                  <Label for="mnemonicTwoInput">Mnemonic Two</Label>
                  <input
                    id="mnemonicTwoInput"
                    type="text"
                    className={`form-control ${
                      errors.mnemonicTwo && "is-invalid"
                    }`}
                    {...register("mnemonicTwo", { required: true })}
                  />
                  {errors.mnemonicTwo && (
                    <div className="invalid-feedback">Field required</div>
                  )}
                  <FormText color="muted">
                    The mnemomic on the second activation letter
                  </FormText>
                </FormGroup>
                <Button onClick={handleSubmit(onSubmit)}>Login</Button>
              </Form>
              <hr />
              <Button outline onClick={() => setShowQrScanner(true)}>
                Scan QR Code
              </Button>
            </>
          )}
        </Container>
      </Jumbotron>
    </div>
  );
}

export default Login;
