import { ethers } from "ethers";
import { useState, useEffect } from "react";
import loginAndVerify from "../utils/login";
import generateVote from "../utils/generateVote";
import getPollStatus from "../utils/getPollStatus";
import getVoteOptions from "../utils/getVoteOptions";
import getPoll from "../utils/getPoll";
import { v4 as uuidv4 } from "uuid";
import generateMetaTX from "../utils/generateMetaTX";

function useVoteCenter() {
  const [contract, setContract] = useState(false);
  const [pollID, setPollID] = useState(false);
  const [voterStatus, setVoterStatus] = useState(false);
  const [poll, setPoll] = useState(false);
  const [vote, setVote] = useState(false);

  const login = (pollID, mnemonicOne, mnemonicTwo) => {
    return new Promise((resolve, reject) => {
      loginAndVerify(pollID, mnemonicOne, mnemonicTwo)
        .then((res) => {
          setVoterStatus(res.status);
          setContract(res.contract);
          setPollID(Number(pollID));
          resolve();
        })
        .catch((err) => reject(err));
    });
  }; //public

  const logout = () => {
    setContract(false);
    setPollID(false);
    setVoterStatus(false);
    setPoll(false);
    setVote(false);
  }; //public

  const voteOnPoll = (optionID) => {
    return new Promise((resolve, reject) => {
      var voteObject = {
        pollID: pollID,
        optionID: optionID,
        uuid: uuidv4(),
      };

      voteObject = generateVote(voteObject, poll.publicKey);

      generateMetaTX(contract, voteObject)
        .then((metaTX) => {
          //submit vote to the relay
          const requestOptions = {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(metaTX),
          };

          fetch("/relay-transaction", requestOptions)
            .then(async (res) => {
              if (!(res.status === 200)) {
                const errorMessage = await res.text();
                reject(new Error(errorMessage));
              } else {
                // get the account address
                const voterAddress = await contract.signer.getAddress();

                setVote({
                  ...voteObject,
                  signature: metaTX.signature,
                  cast: false,
                  submitted: true,
                  counted: false,
                  address: voterAddress,
                });
                listenForVotedEvent(voteObject);
                resolve();
              }
            })
            .catch((err) => reject(err));
        })
        .catch((err) => reject(err));
    });
  }; //public

  /// VOTED EVENTS ///

  const listenForVotedEvent = (voteObject) => {
    const EncryptedVoteFilter = contract.filters.EncryptedVote(
      pollID,
      null,
      voteObject.hash,
      contract.signer.address
    );
    contract.on(
      EncryptedVoteFilter,
      (_pollID, _encryptedVote, _hash, _address) => {
        setVote((prev) => {
          return {
            ...prev,
            cast: true,
          };
        });
      }
    );
  };

  const getVotedEvent = () => {
    return new Promise(async (resolve, reject) => {
      const voterAddress = await contract.signer
        .getAddress()
        .catch((err) => reject(err));

      const EncryptedVoteEvent = contract.filters.EncryptedVote(
        pollID,
        null,
        null,
        voterAddress
      );
      contract
        .queryFilter(EncryptedVoteEvent)
        .catch((err) => reject(err))
        .then((events) => {
          return checkIfCounted(events[0]);
        })
        .catch((err) => reject(err))
        .then((res) => {
          setVote({
            ...res[1],
            counted: res[0],
            loaded: true,
          });
          resolve();
        });
    });
  }; //public

  const checkIfCounted = (e) => {
    return new Promise((resolve, reject) => {
      contract
        .getVoteHashStatus(pollID, e.args._voteHash)
        .then((res) => {
          const voteHashStatus = ethers.BigNumber.from(res).toNumber();

          switch (voteHashStatus) {
            case 0:
              reject(new Error("Vote hash doesn't exist"));
              break;
            case 1:
              resolve([false, e]);
              break;
            case 2:
              resolve([true, e]);
              break;
            default:
              reject(new Error("Failed to get vote hash status"));
              break;
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  /// GETTING POLL DATA ///
  const getPollData = () => {
    return new Promise((resolve, reject) => {
      getPoll(contract, pollID)
        .catch((err) => reject(err))
        .then((p) => {
          const pollStatus = getPollStatus(p);
          setPoll({
            ...p,
            status: pollStatus,
            options: false,
            loaded: false,
          });
          return getOptions(p.voteOptionsCount);
        })
        .catch((err) => reject(err))
        .then(() => resolve());
    });
  }; //public

  const getOptions = (voteOptionsCount) => {
    return new Promise((resolve, reject) => {
      getVoteOptions(contract, pollID, voteOptionsCount)
        .then((options) => {
          setPoll((prev) => {
            return {
              ...prev,
              options: options,
              loaded: true,
            };
          });
          resolve();
        })
        .catch((err) => reject(err));
    });
  };

  useEffect(() => {
    if (!pollID) return;

    getPollData().catch((err) => console.error(err));

    // eslint-disable-next-line
  }, [pollID]);

  return {
    contract,
    pollID,
    voterStatus,
    poll,
    vote,
    login,
    logout,
    voteOnPoll,
    getVotedEvent,
    setVoterStatus,
  };
}

export default useVoteCenter;
