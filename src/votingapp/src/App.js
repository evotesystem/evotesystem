import { Button, Navbar, NavbarBrand, NavbarText } from "reactstrap";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";
import Loading from "./components/Loading";
import React, { Suspense } from "react";

import { ApplicationContext } from "./helper/Context";
import useVoteCenter from "./hooks/useVoteCenter";

import Login from "./components/Login";
const Poll = React.lazy(() => import("./components/Poll"));
const Verify = React.lazy(() => import("./components/Verify"));

function App() {
  const vc = useVoteCenter();

  return (
    <BrowserRouter basename="/client">
      <ApplicationContext.Provider value={{ vc }}>
        <div>
          <Navbar className="fixed-top" color="light" light>
            <div className="container">
              <NavbarBrand>eVote Client</NavbarBrand>
              <NavbarText>
                {vc.contract && (
                  <Button onClick={vc.logout} outline color="danger" size="sm">
                    Logout
                  </Button>
                )}
              </NavbarText>
            </div>
          </Navbar>
          <br />
          <Switch>
            {!(vc.contract && vc.pollID) ? (
              <>
                <Route path="/login">
                  <Login />
                </Route>
                <Route path="*">
                  <Redirect to="/login" />
                </Route>
              </>
            ) : (
              <>
                {vc.voterStatus === 1 && (
                  <Suspense fallback={<Loading />}>
                    <Route path="/poll">
                      <Poll />
                    </Route>
                    <Route path="*">
                      <Redirect to="/poll" />
                    </Route>
                  </Suspense>
                )}
                {vc.voterStatus === 2 && (
                  <Suspense fallback={<Loading />}>
                    <Route path="/verify">
                      <Verify />
                    </Route>
                    <Route path="*">
                      <Redirect to="/verify" />
                    </Route>
                  </Suspense>
                )}
              </>
            )}

            <Route path="*">
              <Redirect to="/login" />
            </Route>
          </Switch>
        </div>
        <ToastContainer
          theme="dark"
          autoClose={5000}
          draggable={true}
          pauseOnHover={true}
          position="bottom-right"
        />
      </ApplicationContext.Provider>
    </BrowserRouter>
  );
}

export default App;
