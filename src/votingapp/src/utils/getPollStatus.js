function getPollStatus(poll) {
  const date = Math.round(Date.now() / 1000);

  if (poll.approved && poll.countTime < date) {
    return { name: "tally_reached", id: 6 };
  } else if (poll.approved && poll.closingTime < date) {
    return { name: "closed", id: 5 };
  } else if (!poll.approved && poll.closingTime < date) {
    return { name: "failed", id: 7 };
  } else if (
    poll.approved &&
    poll.openingTime < date &&
    poll.closingTime > date
  ) {
    return { name: "in_progress", id: 4 };
  } else if (poll.approved) {
    return { name: "approved", id: 3 };
  } else if (poll.votersConfirmed) {
    return { name: "voters_confirmed", id: 2 };
  } else if (poll.verificationHashesAdded) {
    return { name: "verification_hashes_added", id: 1 };
  } else {
    return { name: "created", id: 0 };
  }
}

export default getPollStatus;
