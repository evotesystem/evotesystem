import { ethers } from "ethers";
import Evote from "../contract/Evote";

function login(pollID, mnemonicOne, mnemonicTwo) {
  return new Promise((resolve, reject) => {
    // #1 Check if mnemomnic is valid
    const mnemonic = mnemonicOne.trim() + " " + mnemonicTwo.trim();

    if (ethers.utils.isValidMnemonic(mnemonic, ethers.wordlists.en)) {
      //mnemonic is valid

      // #2 Create a wallet

      const walletMnemonic = ethers.Wallet.fromMnemonic(mnemonic);

      // #3 Connect a provider

      const provider = new ethers.providers.JsonRpcProvider(Evote.provider);

      const wallet = walletMnemonic.connect(provider);

      // #4 Instantiate the contract

      const contract = new ethers.Contract(Evote.address, Evote.abi, wallet);

      // #5 Check if the user is a registered voter

      contract
        .viewVoterStatus(pollID, wallet.address)
        .then((status) => {
          //convert big number to regular number
          const numberStatus = ethers.BigNumber.from(status).toNumber();

          if (numberStatus === 0) {
            reject(new Error("Account is not registered to vote"));
          } else if (numberStatus === 1 || numberStatus === 2) {
            //user is eligible to vote or already voted
            resolve({ contract: contract, status: numberStatus });
          } else {
            reject(new Error("getVoterStatus returned unknown value"));
          }
        })
        .catch((err) => {
          reject(err);
        });
    } else {
      //mnemonic is not valid
      reject(new Error("Mnemonic is not valid"));
    }
  });
}

export default login;
