import { ethers } from "ethers";

function getVoteOptions(contract, id, optionCount) {
  return new Promise(async (resolve, reject) => {
    var options = [];

    for (var i = 0; i < optionCount; i++) {
      await getOption(contract, id, i + 1)
        .then((o) => {
          options.push(o);
        })
        .catch((err) => {
          reject(err);
        });
    }

    options.sort((a, b) => a.id - b.id);

    resolve(options);
  });
}

function getOption(contract, id, optionID) {
  return new Promise((resolve, reject) => {
    contract
      .viewVoteOption(id, optionID)
      .then((o) => {
        resolve({
          optionName: o.optionName,
          id: optionID,
          votes: ethers.BigNumber.from(o.votes).toNumber(),
        });
      })
      .then((err) => {
        reject(err);
      });
  });
}

export default getVoteOptions;
