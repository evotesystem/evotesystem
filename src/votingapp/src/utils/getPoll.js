import { ethers } from "ethers";

function getPoll(contract, id) {
  return new Promise((resolve, reject) => {
    contract
      .idToPoll(id)
      .then((res) => {
        let p = { ...res };

        p.pollID = toNumber(res.pollID);
        p.openingTime = toNumber(p.openingTime);
        p.closingTime = toNumber(p.closingTime);
        p.countTime = toNumber(p.countTime);
        p.voteOptionsCount = toNumber(p.voteOptionsCount);

        resolve(p);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

const toNumber = (b) => {
  return ethers.BigNumber.from(b).toNumber();
};

export default getPoll;
