import crypto from "crypto";
import { Keccak } from "sha3";

function generateVote(v, publicKey) {
  var voteObject = { ...v };

  //#1 hash vote
  const voteString =
    "pollID:" +
    voteObject.pollID +
    ";optionID:" +
    voteObject.optionID +
    ";uuid:" +
    voteObject.uuid;
  voteObject.voteString = voteString;
  const hash = new Keccak(256);

  const finalHash = "0x" + String(hash.update(voteString).digest("hex"));
  voteObject.hash = finalHash;

  //#2 encrypt vote
  const encryptedVote = crypto
    .publicEncrypt({ key: publicKey }, Buffer.from(voteString))
    .toString("base64");
  voteObject.encryptedVote = encryptedVote;

  return voteObject;
}

export default generateVote;
