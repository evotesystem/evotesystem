import { ethers } from "ethers";
import Evote from "../contract/Evote";

function generateMetaTX(contract, voteObject) {
  return new Promise(async (resolve, reject) => {
    //get the address of the voter
    const voterAddress = await contract.signer
      .getAddress()
      .catch((err) => reject(err));

    let metaTx = {
      from: voterAddress,
      to: Evote.address,
      pollID: voteObject.pollID,
      voteHash: voteObject.hash,
      vote: voteObject.encryptedVote,
      signature: "",
    };

    //generate the messageHash of the meta transaction
    const messageHash = ethers.utils.solidityKeccak256(
      ["address", "uint", "bytes32", "address", "string"],
      [metaTx.to, metaTx.pollID, metaTx.voteHash, metaTx.from, metaTx.vote]
    );

    //sign the message hash
    const signature = await contract.signer
      .signMessage(ethers.utils.arrayify(messageHash))
      .catch((err) => reject(err));
    metaTx.signature = signature;

    resolve(metaTx);
  });
}

export default generateMetaTX;
