import { version as uuidVersion, validate as uuidValidate } from "uuid";
import generateVote from "./generateVote";
import { ethers } from "ethers";

//verify the meta file
const verifyVote = (f, vc) => {
  return new Promise(async (resolve, reject) => {
    const voterAddress = await vc.contract.signer.getAddress().catch(reject);

    if (f.pollID !== vc.poll.pollID) {
      reject(
        new Error(
          "Poll id in the vote file doesn't match the poll id linked to your account."
        )
      );
    } else if (f.address !== voterAddress) {
      reject(
        new Error(
          "Address in the vote file doesn't match your account's address."
        )
      );
    } else if (
      f.encryptedVote &&
      ethers.utils.isHexString(f.hash) &&
      f.hash.length === 66 &&
      uuidValidateV4(f.uuid) &&
      f.voteString
    ) {
      const voteObject = generateVote(
        {
          pollID: f.pollID,
          optionID: f.optionID,
          uuid: f.uuid,
        },
        vc.poll.publicKey
      );

      var result = {
        hash: false,
        voteString: false,
        signature: false,
        encryptedVote: false,
      };

      //check if the vote hash of the chainvote file matches the one on the blockchain and the calculated one
      if (voteObject.hash === f.hash && f.hash === vc.vote.args._voteHash)
        result.hash = true;

      //check that the vote string in the chainvote file matches the calculated vote string
      if (voteObject.voteString === f.voteString) result.voteString = true;

      //check that the encrypted vote in the chainvote file matches the one on the blockchain
      if (f.encryptedVote === vc.vote.args._vote) result.encryptedVote = true;

      //check that the signature is correct
      const messageHash = ethers.utils.solidityKeccak256(
        ["address", "uint", "bytes32", "address", "string"],
        [vc.contract.address, f.pollID, f.hash, voterAddress, f.encryptedVote]
      );
      if (
        ethers.utils.verifyMessage(
          ethers.utils.arrayify(messageHash),
          f.signature
        ) === voterAddress
      ) {
        if (result.hash) result.signature = true;
      }

      resolve(result);
    } else {
      reject(new Error("Chainvote file is not correct."));
    }
  });
};

//check if the uuid is valid
const uuidValidateV4 = (uuid) => {
  return uuidValidate(uuid) && uuidVersion(uuid) === 4;
};

export default verifyVote;
