const util = require("util");
const exec = util.promisify(require("child_process").exec);
const fs = require("fs");
const path = require("path");
const chalk = require("chalk");
const log = console.log;

// Install packages using npm ci in:
// - adminapp
// - votingapp
// - evote-cli
// - evote registration
// - server
// - smart contract

const commands = [
  {
    command: "cd src/adminapp && npm ci",
    description: "Installing npm packages in the admin app",
  },
  {
    command: "cd src/votingapp && npm ci",
    description: "Installing npm packages in the voting app",
  },
  {
    command: "cd server && npm ci",
    description: "Installing npm packages in the server",
  },
  {
    command: "cd contract && npm ci",
    description: "Installing npm packages in the smart contract",
  },
  {
    command: "cd src/evote-cli && npm ci",
    description: "Installing npm packages in the cli tool",
  },
  {
    command: "cd src/evote-cli && sudo npm link",
    description: "Linking the cli tool",
  }
];

async function executeAllCommands() {
  for (var i = 0; i < commands.length; i++) {
    await executeCommand(commands[i]);
  }

  log(chalk.blue("Creating folders"));

  try {
    fs.mkdirSync(path.resolve(__dirname, "config/temp"));
    fs.mkdirSync(path.resolve(__dirname, "server/contract"));
    fs.mkdirSync(path.resolve(__dirname, "src/adminapp/src/contract"));
    fs.mkdirSync(path.resolve(__dirname, "src/votingapp/src/contract"));
    fs.mkdirSync(path.resolve(__dirname, "src/evote-cli/contract"));
    log(chalk.green("Successfully created folders"));
  } catch (error) {
    log(chalk.red("Failed to create folders"));
    console.error(error);
  }
}

function executeCommand(c) {
  return new Promise(async (resolve, reject) => {
    try {
      log(chalk.blue("Executing command: ") + c.description);

      const { stdout, stderr } = await exec(c.command);
      stdout && console.log(chalk.blue("stdout:"), stdout);
      stderr && console.error(chalk.red("stderr:"), stderr);

      log(chalk.green("Finished executing command: ") + c.description);

      resolve();
    } catch (e) {
      log(chalk.red("Failed to execute command: ") + c.description);
      console.error(e);
      resolve();
    }
  });
}

executeAllCommands();
