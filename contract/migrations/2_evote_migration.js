const Evote = artifacts.require("Evote")
const {ethers} = require("ethers")
const fs = require("fs")
const path = require("path")

require('dotenv').config()

var accounts = []

function createAccounts() {

  return new Promise((res, rej) => {

    //Check if the contract should be deployed with already created admin accounts, or if new ones should be created
    if(process.env.LOAD_ADMINS === "TRUE") {
      const data = fs.readFileSync(path.resolve(__dirname, "Admin_Accounts.json"))
      const admins = JSON.parse(data)
      accounts = admins
      res()
    } else {

      for (var i = 0; i < 12; i++) {
        const wallet = new ethers.Wallet.createRandom()
        const account = {
          address: wallet.address,
          privateKey: wallet.privateKey
        }
        accounts.push(account)
      }
  
      //Save addresses and private keys of initial admins to a file if the contract is deployed to a public testnet
      if(process.env.PUBLICNET === "TRUE" && process.env.LOAD_ADMINS !== "TRUE") {
        const data = JSON.stringify(accounts)
        fs.writeFileSync(path.resolve(__dirname, "Admin_Accounts.json"), data)
      }
      res()
    }

  })
  
}

module.exports = function (deployer) {
  createAccounts().then(
    deployer.deploy(Evote, 
      [accounts[0].address, accounts[1].address, accounts[2].address], 
      [accounts[3].address, accounts[4].address, accounts[5].address],
      [accounts[6].address, accounts[7].address, accounts[8].address, accounts[9].address, accounts[10].address, accounts[11].address])
  )
};
