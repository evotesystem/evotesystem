const { ethers } = require("ethers");
const Web3 = require("web3");

//From: https://medium.com/sablier/writing-accurate-time-dependent-truffle-tests-8febc827acb5

const web3 = new Web3("http://localhost:7545");

const advanceBlockAtTime = (time) => {
  return new Promise((resolve, reject) => {
    web3.currentProvider.send(
      {
        jsonrpc: "2.0",
        method: "evm_mine",
        params: [time],
        id: new Date().getTime(),
      },
      (err, _) => {
        if (err) {
          return reject(err);
        }
        const newBlockHash = web3.eth.getBlock("latest").hash;

        return resolve(newBlockHash);
      }
    );
  });
};

const generateSignatures = (verificationHashes, registrars) => {
  return new Promise(async (resolve, reject) => {
    const provider = new ethers.providers.JsonRpcProvider(
      "http://localhost:7545"
    );

    const signer1 = provider.getSigner(registrars[0]);
    const signer2 = provider.getSigner(registrars[1]);
    const signer3 = provider.getSigner(registrars[2]);

    const message = ethers.utils.arrayify(verificationHashes[6]);

    //BUG: The last two digits of the signatures are wrong
    const signature1 = await signer1.signMessage(message);
    const signature2 = await signer2.signMessage(message);
    const signature3 = await signer3.signMessage(message);

    const signatures = [
      ethers.utils.joinSignature(signature1), 
      ethers.utils.joinSignature(signature2), 
      ethers.utils.joinSignature(signature3)
  ]

    resolve(signatures);
  });
};

const verificationHashes = [
  "0xed12ed92ad0e6e1e3d7cbaf26c9fdaac64e05d0d7b30fb9c1576df3e5fa75fad",
  "0x5754665e53f009990219cb1032bbdad4b528a86a4831d6a0bd545ab3b3582868",
  "0x85e7cf88d3d71fdb4b52b4200bc1260115b564ef35409b4560288cc5d07a1cca",
  "0x984a2f486e510bb3008385e2dbd5743e0a69e393a001b3a4a1dd268c31d10c61",
  "0xee1947fe53924feb0677e1ded82771e11f2418f72ead14366414607ec5fbb2b6",
  "0xaac2050ee7a3702ea939c386a322f3c7d0950d53b60411ca5d2e9bfb24f0c00a",
  "0x7c1c9e59fdf41222af1a97c775433eb07207539f5e7546a823745ce14edfa76d",
];

const getAdmins = (accounts) => {
  return {
    registrars: [accounts[10], accounts[11], accounts[12]],
    chairpeople: [accounts[20], accounts[21], accounts[22]],
    electoralBoard: [
      accounts[30],
      accounts[31],
      accounts[32],
      accounts[33],
      accounts[34],
      accounts[35],
    ],
  };
};

module.exports.advanceBlockAtTime = advanceBlockAtTime;
module.exports.generateSignatures = generateSignatures;
module.exports.verificationHashes = verificationHashes;
module.exports.getAdmins = getAdmins;
