const eVoteContract = artifacts.require("Evote");
const ta = require("truffle-assertions");
const { v4: uuidv4 } = require("uuid");
const keccak256 = require("keccak256");
const crypto = require("crypto");
const path = require("path");
const fs = require("fs");

const ethers = require("ethers");
const provider = new ethers.providers.JsonRpcProvider("http://localhost:7545");
const {
  advanceBlockAtTime,
  generateSignatures,
  getAdmins,
  verificationHashes,
} = require("./helper");

contract("Evote", async (accounts) => {
  //get the admins
  const admins = getAdmins(accounts);

  //set the start date of the blockchain to now
  const blockchainStartTime = Math.round(new Date().getTime() / 1000);

  let instance;

  const startTime = blockchainStartTime + 10 * 60;
  const endTime = blockchainStartTime + 60 * 60;
  const tallyTime = blockchainStartTime + 90 * 60;

  const optionOne = "Yes";
  const optionTwo = "No";
  const optionThree = "I don't care";
  const pollName = "Should drugs be legalized?";
  const hash =
    "0x42bb176eb31c9c20c3889601d6533eb9e7de06a6040dd2f9ea73381c5b21e0ce";
  const publicKey =
    "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEArhnCBK2N2tspya7+17RSl0F6ghZySFTtRT1oIBln4CsZZo9NqW4D0hO4Me48fTK0LS+OLmUCNTzZklS3P2l4qK58yGD4qCP70LN7LjWSDR5fEPt5LHBq0ww6HOBtUkiDvzw8FEecgFYcf7m4HzvGTFsRE79oRMJlwT8wgCEmhL/TUHRE7qTKb+pk75IUirfF6PmFncrZDf9bjhX7/2JZMLM9dHp/aYOxQ15C+D2cx80ONvSkRY5AzN8lZNQSYihsPRi9YDBQWrjL+TH+tcclaHuM+wnxSWEum6te0yhpgKxEFzjcoli4gszOsgt0CRj1czItj+EU6IlSevp2M2Y13jev4Qw3qNtd/+OxnKG5WeEl+zHECteP2t0BWuEkyGJp//DAjikziLhDBzaKlryJkimF8VArFrfAJN21XJgN3uwRDyYGf1FUEPfFcUA2CKHNfV2dCAoa4W6zkTOE3G+VJ/MKaU2K0gdam9rbAyCM+KYdDBpdWRAGLIEqWa1eH+iElKYo1XdFx0s+i2M/S97iFTRSm+IcrXG4dE209kEJTXJ5Geb2rTQwjwHDlNjaGfSTvaTrvQCFt1toDwd0wCwjQnEYhLeL4+bLDdc3veKT35ea06cQSN4jt/wfpSEDAU60K7KVUAr8NN4TQIT5NTNQYM/2AbHts6TUv+PbGrTQwf0CAwEAAQ==";
  const signatures = await generateSignatures(
    verificationHashes,
    admins.registrars
  );

  /// Testing the constructor ///
  describe("Constructor", () => {
    beforeEach(async () => {
      await advanceBlockAtTime(blockchainStartTime);
    });

    it("should test the constructor with valid input || positive", async () => {
      await ta.passes(
        eVoteContract.new(
          admins.registrars,
          admins.chairpeople,
          admins.electoralBoard
        )
      );
    });

    it("should test the constructor by calling it with the wrong amount of registrars || negative", async () => {
      await ta.reverts(
        eVoteContract.new(
          [admins.registrars[0], admins.registrars[1]],
          admins.chairpeople,
          admins.electoralBoard
        ),
        "Too many or too few registrars specified"
      );
    });

    it("should test the constructor by calling it with the wrong amount of chairpeople || negative", async () => {
      await ta.reverts(
        eVoteContract.new(
          admins.registrars,
          [
            admins.chairpeople[0],
            admins.chairpeople[1],
            accounts[22],
            accounts[23],
          ],
          admins.electoralBoard
        ),
        "Too many or too few chairpeople specified"
      );
    });

    it("should test the constructor by calling it with the wrong amount of election board members || negative", async () => {
      await ta.reverts(
        eVoteContract.new(admins.registrars, admins.chairpeople, [
          admins.electoralBoard[0],
          accounts[31],
          accounts[32],
          accounts[33],
          accounts[34],
        ]),
        "Too many or too few election board members specified"
      );
    });
  });

  /// createPoll function ///
  describe("createPoll function", () => {
    beforeEach(async () => {
      await advanceBlockAtTime(blockchainStartTime);

      instance = await eVoteContract.new(
        admins.registrars,
        admins.chairpeople,
        admins.electoralBoard
      );
    });

    it("should test the createPoll function by calling it with valid input || positive", async () => {
      await ta.passes(
        instance.createPoll(
          startTime,
          endTime,
          [optionOne, optionTwo, optionThree],
          pollName,
          hash,
          publicKey,
          tallyTime,
          { from: admins.chairpeople[0] }
        )
      );

      const poll = await instance.idToPoll(1);

      assert.equal(poll.openingTime, startTime);
      assert.equal(poll.closingTime, endTime);
      assert.equal(poll.pollName, pollName);
      assert.equal(poll.contentHash, hash);
      assert.equal(poll.publicKey, publicKey);
      assert.equal(poll.countTime, tallyTime);

      assert.equal(poll.voteOptionsCount, 3);
      assert.equal(poll.verificationHashesAdded, false);
      assert.equal(poll.votersConfirmed, false);
      assert.equal(poll.approved, false);
    });

    it("should test the createPoll function by calling it with too few vote options || negative", async () => {
      await ta.reverts(
        instance.createPoll(
          startTime,
          endTime,
          [optionOne],
          pollName,
          hash,
          publicKey,
          tallyTime,
          { from: admins.chairpeople[0] }
        ),
        "Too few vote options submitted"
      );
    });

    it("should test the createPoll function by calling it with too many vote options || negative", async () => {
      await ta.reverts(
        instance.createPoll(
          startTime,
          endTime,
          ["1", "2", "3", "4", "5", "6"],
          pollName,
          hash,
          publicKey,
          tallyTime,
          { from: admins.chairpeople[0] }
        ),
        "Too many vote options submitted"
      );
    });

    it("should test the createPoll function by calling it with a tally time that is before the closing time || negative", async () => {
      const wrongTallyTime = endTime - 10 * 60;

      await ta.reverts(
        instance.createPoll(
          startTime,
          endTime,
          [optionOne, optionTwo, optionThree],
          pollName,
          hash,
          publicKey,
          wrongTallyTime,
          { from: admins.chairpeople[0] }
        ),
        "Counting time is before closing time"
      );
    });

    it("should test the createPoll function by calling it with closing time in the past || negative", async () => {
      const wrongEndTime = blockchainStartTime - 10 * 60;

      await ta.reverts(
        instance.createPoll(
          startTime,
          wrongEndTime,
          [optionOne, optionTwo, optionThree],
          pollName,
          hash,
          publicKey,
          tallyTime,
          { from: admins.chairpeople[0] }
        ),
        "Closing time is in the past"
      );
    });

    it("should test the createPoll function by calling it with an opening time in the past || negative", async () => {
      const wrongStartTime = blockchainStartTime - 10 * 60;

      await ta.reverts(
        instance.createPoll(
          wrongStartTime,
          endTime,
          [optionOne, optionTwo, optionThree],
          pollName,
          hash,
          publicKey,
          tallyTime,
          { from: admins.chairpeople[0] }
        ),
        "Opening time is in the past"
      );
    });
  });

  /// addVerificationHash function ///

  describe("addVerificationHashes function", () => {
    beforeEach(async () => {
      await advanceBlockAtTime(blockchainStartTime);

      instance = await eVoteContract.new(
        admins.registrars,
        admins.chairpeople,
        admins.electoralBoard
      );

      await ta.passes(
        instance.createPoll(
          startTime,
          endTime,
          [optionOne, optionTwo, optionThree],
          pollName,
          hash,
          publicKey,
          tallyTime,
          { from: admins.chairpeople[0] }
        )
      );
    });

    it("should test the addVerificationHashes function by calling it with valid input || positive", async () => {
      await ta.passes(
        instance.addVerificationHashes(1, verificationHashes, signatures, {
          from: admins.registrars[0],
        })
      );
    });

    it("should test the addVerificationHashes function by calling it with wrong verification hashes for au || negative", async () => {
      const verificationHashesWrong = [
        "0xed12ed92ad0e6e1e3d7cbaf26c9fdaac64e05d0d7b30fb9c1576df3e5fa75bad",
        "0x5754665e53f009990219cb1032bbdad4b528a86a4831d6a0bd545ab3b3582868",
        "0x85e7cf88d3d71fdb4b52b4200bc1260115b564ef35409b4560288cc5d07a1cca",
        "0x984a2f486e510bb3008385e2dbd5743e0a69e393a001b3a4a1dd268c31d10c61",
        "0xee1947fe53924feb0677e1ded82771e11f2418f72ead14366414607ec5fbb2b6",
        "0xaac2050ee7a3702ea939c386a322f3c7d0950d53b60411ca5d2e9bfb24f0c00a",
        "0x7c1c9e59fdf41222af1a97c775433eb07207539f5e7546a823745ce14edfa76d",
      ];

      await ta.reverts(
        instance.addVerificationHashes(1, verificationHashesWrong, signatures, {
          from: admins.registrars[0],
        }),
        "Hash au incorrect"
      );
    });

    it("should test the addVerificationHashes function by calling it with wrong verification hashes for ot || negative", async () => {
      const verificationHashesWrong = [
        "0xed12ed92ad0e6e1e3d7cbaf26c9fdaac64e05d0d7b30fb9c1576df3e5fa75fad",
        "0x5754665e53f009990219cb1032bbdad4b528a86a4831d6a0bd545ab3b3582868",
        "0x85e7cf88d3d71fdb4b52b4200bc1260115b564ef35409b4560288cc5d07a1fca",
        "0x984a2f486e510bb3008385e2dbd5743e0a69e393a001b3a4a1dd268c31d10c61",
        "0xee1947fe53924feb0677e1ded82771e11f2418f72ead14366414607ec5fbb2b6",
        "0xaac2050ee7a3702ea939c386a322f3c7d0950d53b60411ca5d2e9bfb24f0c00a",
        "0x7c1c9e59fdf41222af1a97c775433eb07207539f5e7546a823745ce14edfa76d",
      ];

      await ta.reverts(
        instance.addVerificationHashes(1, verificationHashesWrong, signatures, {
          from: admins.registrars[0],
        }),
        "Hash ot incorrect"
      );
    });

    it("should test the addVerificationHashes function by calling it with wrong verification hashes for auot || negative", async () => {
      const verificationHashesWrong = [
        "0xed12ed92ad0e6e1e3d7cbaf26c9fdaac64e05d0d7b30fb9c1576df3e5fa75fad",
        "0x5754665e53f009990219cb1032bbdad4b528a86a4831d6a0bd545ab3b3582868",
        "0x85e7cf88d3d71fdb4b52b4200bc1260115b564ef35409b4560288cc5d07a1cca",
        "0x984a2f486e510bb3008385e2dbd5743e0a69e393a001b3a4a1dd268c31d10c61",
        "0xee1947fe53924feb0677e1ded82771e11f2418f72ead14366414607ec5fbb2b6",
        "0xaac2050ee7a3702ea939c386a322f3c7d0950d53b60411ca5d2e9bfb24f0c00a",
        "0x7c1c9e59fdf41222af1a97c775433eb07207539f5e7546a823745ce14edfb76d",
      ];

      await ta.reverts(
        instance.addVerificationHashes(1, verificationHashesWrong, signatures, {
          from: admins.registrars[0],
        }),
        "Hash auot incorrect"
      );
    });

    it("should test the addVerificationHashes function by calling it with a wrong signature || negative", async () => {
      const wrongSignatures = [
        "0x6f3de1e24994a52a3749a5f9d162ace2096e7237fa91b7611afb23c4740af4e739cbc1db333b5f72fa7047a0cc5ab58d4e62bd1719f69047199b444da4c4ada41b",
        signatures[0],
        signatures[1],
      ];

      await ta.reverts(
        instance.addVerificationHashes(1, verificationHashes, wrongSignatures, {
          from: admins.registrars[0],
        }),
        "Signer is not a registrar"
      );
    });

    it("should test the addVerificationHashes function by calling it with 2 times the same signature || negative", async () => {
      const wrongSignatures = [signatures[0], signatures[0], signatures[1]];

      await ta.reverts(
        instance.addVerificationHashes(1, verificationHashes, wrongSignatures, {
          from: admins.registrars[0],
        }),
        "Can't sign hash twice"
      );
    });

    it("should test the addVerificationHashes function by calling it with wrong amount of verification hashes || negative", async () => {
      const verificationHashesWrong1 = [
        "0xed12ed92ad0e6e1e3d7cbaf26c9fdaac64e05d0d7b30fb9c1576df3e5fa75bad",
        "0x5754665e53f009990219cb1032bbdad4b528a86a4831d6a0bd545ab3b3582868",
        "0x85e7cf88d3d71fdb4b52b4200bc1260115b564ef35409b4560288cc5d07a1cca",
        "0x984a2f486e510bb3008385e2dbd5743e0a69e393a001b3a4a1dd268c31d10c61",
        "0xee1947fe53924feb0677e1ded82771e11f2418f72ead14366414607ec5fbb2b6",
        "0xaac2050ee7a3702ea939c386a322f3c7d0950d53b60411ca5d2e9bfb24f0c00a",
      ];

      await ta.reverts(
        instance.addVerificationHashes(
          1,
          verificationHashesWrong1,
          signatures,
          { from: admins.registrars[0] }
        ),
        "Not all hashes have been specified"
      );

      const verificationHashesWrong2 = [
        "0xed12ed92ad0e6e1e3d7cbaf26c9fdaac64e05d0d7b30fb9c1576df3e5fa75bad",
        "0x5754665e53f009990219cb1032bbdad4b528a86a4831d6a0bd545ab3b3582868",
        "0x85e7cf88d3d71fdb4b52b4200bc1260115b564ef35409b4560288cc5d07a1cca",
        "0x984a2f486e510bb3008385e2dbd5743e0a69e393a001b3a4a1dd268c31d10c61",
        "0xee1947fe53924feb0677e1ded82771e11f2418f72ead14366414607ec5fbb2b6",
        "0xaac2050ee7a3702ea939c386a322f3c7d0950d53b60411ca5d2e9bfb24f0c00a",
        "0x7c1c9e59fdf41222af1a97c775433eb07207539f5e7546a823745ce14edfa76d",
        "0x7c1c9e59fdf41222af1a97c775433eb07207539f5e7546a823745ce14edfa76d",
      ];

      await ta.reverts(
        instance.addVerificationHashes(
          1,
          verificationHashesWrong2,
          signatures,
          { from: admins.registrars[0] }
        ),
        "Not all hashes have been specified"
      );
    });

    it("should test the addVerificationHashes function by calling it on a poll where the verification hashes have already been added || negative", async () => {
      await ta.passes(
        instance.addVerificationHashes(1, verificationHashes, signatures, {
          from: admins.registrars[0],
        })
      );

      await ta.reverts(
        instance.addVerificationHashes(1, verificationHashes, signatures, {
          from: admins.registrars[0],
        }),
        "Verification hashes already added"
      );
    });
  });

  /// addVoters function ///

  describe("addVoters function", () => {
    beforeEach(async () => {
      await advanceBlockAtTime(blockchainStartTime);

      instance = await eVoteContract.new(
        admins.registrars,
        admins.chairpeople,
        admins.electoralBoard
      );

      await instance.createPoll(
        startTime,
        endTime,
        [optionOne, optionTwo, optionThree],
        pollName,
        hash,
        publicKey,
        tallyTime,
        { from: admins.chairpeople[0] }
      );

      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });
    });

    it("should test the addVoters function by adding 4 voters || positive", async () => {
      await ta.passes(
        instance.addVoters(
          [accounts[1], accounts[2], accounts[3], accounts[4]],
          1,
          { from: admins.chairpeople[0] }
        )
      );

      //Confirm voters
      await instance.confirmVoters(1, 0, { from: admins.chairpeople[1] });
      await instance.confirmVoters(1, 1, { from: admins.chairpeople[2] });

      //Confirm poll
      await instance.confirmPoll(1, 0, { from: admins.electoralBoard[0] });
      for (i = 0; i < 3; i++) {
        await instance.confirmPoll(1, 2, {
          from: admins.electoralBoard[1 + i],
        });
      }

      //Check that the voters were successfully added
      for (var i = 0; i < 4; i++) {
        assert.equal(1, await instance.viewVoterStatus(1, accounts[1 + i]));
      }

      //Check that addresses which weren't added as voters haven't been registered
      for (var i = 0; i < 3; i++) {
        assert.equal(0, await instance.viewVoterStatus(1, accounts[5 + i]));
      }
    });

    it("should test the addVoters function by adding the same user twice || negative", async () => {
      await instance.addVoters([accounts[1]], 1, {
        from: admins.chairpeople[0],
      });
      await ta.reverts(
        instance.addVoters([accounts[1]], 1, { from: admins.chairpeople[0] }),
        "User is already registered"
      );
    });
  });

  /// removeVoters function ///

  describe("removeVoters function", () => {
    const voters = [accounts[1], accounts[2], accounts[3], accounts[4]];

    beforeEach(async () => {
      await advanceBlockAtTime(blockchainStartTime);

      instance = await eVoteContract.new(
        admins.registrars,
        admins.chairpeople,
        admins.electoralBoard
      );

      await instance.createPoll(
        startTime,
        endTime,
        [optionOne, optionTwo, optionThree],
        pollName,
        hash,
        publicKey,
        tallyTime,
        { from: admins.chairpeople[0] }
      );

      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      await instance.addVoters(voters, 1, { from: admins.chairpeople[0] });
    });

    it("should test the removeVoters function by removing two 2 voters || positive", async () => {
      await ta.passes(
        instance.removeVoters([voters[0], voters[1]], 1, {
          from: admins.chairpeople[0],
        })
      );

      //Confirm voters
      await instance.confirmVoters(1, 0, { from: admins.chairpeople[1] });
      await instance.confirmVoters(1, 1, { from: admins.chairpeople[2] });

      //Confirm poll
      await instance.confirmPoll(1, 0, { from: admins.electoralBoard[0] });
      for (i = 0; i < 3; i++) {
        await instance.confirmPoll(1, 2, {
          from: admins.electoralBoard[1 + i],
        });
      }

      for (var i = 0; i < 2; i++) {
        assert.equal(0, await instance.viewVoterStatus(1, voters[i]));
      }

      for (var i = 0; i < 2; i++) {
        assert.equal(1, await instance.viewVoterStatus(1, voters[2 + i]));
      }
    });

    it("should test the removeVoters function by removing an account that isn't a voter || negative", async () => {
      await ta.reverts(
        instance.removeVoters([accounts[38]], 1, {
          from: admins.chairpeople[0],
        }),
        "User can't be removed"
      );
    });
  });

  /// confirmVoters function ///

  describe("confirmVoters function", () => {
    const voters = [accounts[1], accounts[2], accounts[3], accounts[4]];

    beforeEach(async () => {
      await advanceBlockAtTime(blockchainStartTime);

      instance = await eVoteContract.new(
        admins.registrars,
        admins.chairpeople,
        admins.electoralBoard
      );

      await instance.createPoll(
        startTime,
        endTime,
        [optionOne, optionTwo, optionThree],
        pollName,
        hash,
        publicKey,
        tallyTime,
        { from: admins.chairpeople[0] }
      );

      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      await instance.addVoters(voters, 1, { from: admins.chairpeople[0] });
    });

    it("should test the confirmVoters function by calling it with valid input || positive", async () => {
      //Confirm voters
      await ta.passes(
        instance.confirmVoters(1, 0, { from: admins.chairpeople[0] })
      );
      await ta.passes(
        instance.confirmVoters(1, 1, { from: admins.chairpeople[1] })
      );

      //Check that the voters are confirmed
      const poll = await instance.idToPoll(1);
      assert.isTrue(poll.votersConfirmed);
    });
  });

  /// confirmPoll function ///

  describe("confirmPoll function", () => {
    beforeEach(async () => {
      await advanceBlockAtTime(blockchainStartTime);

      instance = await eVoteContract.new(
        admins.registrars,
        admins.chairpeople,
        admins.electoralBoard
      );

      await instance.createPoll(
        startTime,
        endTime,
        [optionOne, optionTwo, optionThree],
        pollName,
        hash,
        publicKey,
        tallyTime,
        { from: admins.chairpeople[0] }
      );

      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      await instance.addVoters(
        [accounts[1], accounts[2], accounts[3], accounts[4]],
        1,
        { from: admins.chairpeople[0] }
      );

      //Confirm voters
      await ta.passes(
        instance.confirmVoters(1, 0, { from: admins.chairpeople[0] })
      );
      await ta.passes(
        instance.confirmVoters(1, 1, { from: admins.chairpeople[1] })
      );
    });

    it("should test the confirmPoll function by calling it with valid input || positive", async () => {
      //Confirm poll
      await ta.passes(
        instance.confirmPoll(1, 0, { from: admins.electoralBoard[0] })
      );
      for (i = 0; i < 3; i++) {
        await ta.passes(
          instance.confirmPoll(1, 2, { from: admins.electoralBoard[1 + i] })
        );
      }

      const poll = await instance.idToPoll(1);
      assert.isTrue(poll.approved);
    });
  });

  /// viewVoteOption function ///

  describe("viewVoteOption function", () => {
    beforeEach(async () => {
      await advanceBlockAtTime(blockchainStartTime);

      instance = await eVoteContract.new(
        admins.registrars,
        admins.chairpeople,
        admins.electoralBoard
      );

      await instance.createPoll(
        startTime,
        endTime,
        [optionOne, optionTwo, optionThree],
        pollName,
        hash,
        publicKey,
        tallyTime,
        { from: admins.chairpeople[0] }
      );

      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      await instance.addVoters(
        [accounts[1], accounts[2], accounts[3], accounts[4]],
        1,
        { from: admins.chairpeople[0] }
      );

      //Confirm voters
      await ta.passes(
        instance.confirmVoters(1, 0, { from: admins.chairpeople[0] })
      );
      await ta.passes(
        instance.confirmVoters(1, 1, { from: admins.chairpeople[1] })
      );

      //Confirm poll
      await instance.confirmPoll(1, 0, { from: admins.electoralBoard[0] });
      for (i = 0; i < 3; i++) {
        await instance.confirmPoll(1, 2, {
          from: admins.electoralBoard[1 + i],
        });
      }
    });

    it("should test the viewVoteOption function by calling it with valid input || positive", async () => {
      const optionNames = ["Yes", "No", "I don't care"];

      for (var i = 0; i < 3; i++) {
        const option = await instance.viewVoteOption(1, 1 + i);
        assert.equal(option.optionName, optionNames[i]);
        assert.equal(option.votes, 0);
      }
    });

    it("should test the viewVoteOption function by calling it on a vote option that doesn't exist (1) || negative", async () => {
      await ta.reverts(
        instance.viewVoteOption(1, 8),
        "Vote option doesn't exist"
      );
    });

    it("should test the viewVoteOption function by calling it on a vote option that doesn't exist (2) || negative", async () => {
      await ta.reverts(
        instance.viewVoteOption(1, 0),
        "Vote option doesn't exist"
      );
    });
  });

  /// vote function ///

  describe("vote function", () => {
    const voters = [accounts[1], accounts[2], accounts[3], accounts[4]];

    beforeEach(async () => {
      await advanceBlockAtTime(blockchainStartTime);
      instance = await eVoteContract.new(
        admins.registrars,
        admins.chairpeople,
        admins.electoralBoard
      );

      await instance.createPoll(
        startTime,
        endTime,
        [optionOne, optionTwo, optionThree],
        pollName,
        hash,
        publicKey,
        tallyTime,
        { from: admins.chairpeople[0] }
      );

      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      await instance.addVoters(voters, 1, { from: admins.chairpeople[0] });

      //Confirm voters
      await instance.confirmVoters(1, 0, { from: admins.chairpeople[0] });
      await instance.confirmVoters(1, 1, { from: admins.chairpeople[1] });

      //Confirm poll
      await instance.confirmPoll(1, 0, { from: admins.electoralBoard[0] });
      for (i = 0; i < 3; i++) {
        await instance.confirmPoll(1, 2, {
          from: admins.electoralBoard[1 + i],
        });
      }

      await advanceBlockAtTime(startTime + 2 * 60);
    });

    it("should test the vote function by calling it from multiple accounts with valid input || positive", async () => {
      //1: Create the votes
      var votes = [];
      votes.push({
        optionID: 1,
        uuid: uuidv4(),
      });
      votes.push({
        optionID: 2,
        uuid: uuidv4(),
      });
      votes.push({
        optionID: 3,
        uuid: uuidv4(),
      });
      votes.push({
        optionID: 2,
        uuid: uuidv4(),
      });
      //2: Hash the votes
      for (var i = 0; i < 4; i++) {
        const stringToHash =
          "pollID:1;optionID:" + votes[i].optionID + ";uuid:" + votes[i].uuid;
        const hash = keccak256(stringToHash).toString("hex");
        const hashWithPrefix = "0x" + hash;
        votes[i].voteToEncrypt = stringToHash;
        votes[i].hash = hashWithPrefix;
      }
      //3: Encrypt the votes and generate hashes
      const publicKey = fs.readFileSync(
        path.resolve(__dirname, "publicKey.pem")
      );
      for (var i = 0; i < 4; i++) {
        const encryptedVote = crypto.publicEncrypt(
          { key: publicKey },
          Buffer.from(votes[i].voteToEncrypt)
        );
        votes[i].encryptedVote = encryptedVote.toString("base64");

        votes[i].messageHash = ethers.utils.solidityKeccak256(
          ["address", "uint", "bytes32", "address", "string"],
          [
            instance.address,
            1,
            votes[i].hash,
            voters[i],
            votes[i].encryptedVote,
          ]
        );
      }

      //5: Cast the votes
      for (var i = 0; i < 4; i++) {
        //create the vote signatures
        const messageHashBytes = ethers.utils.arrayify(votes[i].messageHash);

        const signer = provider.getSigner(voters[i]);
        const signature = await signer.signMessage(messageHashBytes);

        let tx;

        //run the test
        await ta.passes(
          (tx = await instance.vote(
            1,
            votes[i].hash,
            votes[i].encryptedVote,
            voters[i],
            ethers.utils.joinSignature(signature),
            { from: accounts[43] }
          ))
        );

        //check that the EncryptedVote event has been emitted successfully
        ta.eventEmitted(
          tx,
          "EncryptedVote",
          (ev) => {
            return (
              ev._vote === votes[i].encryptedVote &&
              ev._voteHash === votes[i].hash &&
              ev._voter === voters[i]
            );
          },
          "Contract should return the correct EncryptedVote event"
        );
      }
    });

    it("should test the vote function by calling it from a user that isn't registered to vote || negative", async () => {
      const messageHash = ethers.utils.solidityKeccak256(
        ["address", "uint", "bytes32", "address", "string"],
        [
          instance.address,
          1,
          "0x4bdcdfc31155544948c6cc905abf63e803053cd92540426d43df38a53d1b3698",
          accounts[46],
          "test_encrypted_vote",
        ]
      );

      const signer = provider.getSigner(accounts[46]);
      const signature = await signer.signMessage(
        ethers.utils.arrayify(messageHash)
      );

      await ta.reverts(
        instance.vote(
          1,
          "0x4bdcdfc31155544948c6cc905abf63e803053cd92540426d43df38a53d1b3698",
          "test_encrypted_vote",
          accounts[46],
          ethers.utils.joinSignature(signature)
        ),
        "User not eligible to vote"
      );
    });

    it("should test the vote function by calling it from a user that has already voted || negative", async () => {
      const messageHash = ethers.utils.solidityKeccak256(
        ["address", "uint", "bytes32", "address", "string"],
        [
          instance.address,
          1,
          "0x4bdcdfc31155544948c6cc905abf63e803053cd92540426d43df38a53d1b3698",
          voters[1],
          "test_encrypted_vote",
        ]
      );

      const signer = provider.getSigner(voters[1]);
      const signature = await signer.signMessage(
        ethers.utils.arrayify(messageHash)
      );

      await ta.passes(
        instance.vote(
          1,
          "0x4bdcdfc31155544948c6cc905abf63e803053cd92540426d43df38a53d1b3698",
          "test_encrypted_vote",
          voters[1],
          ethers.utils.joinSignature(signature)
        )
      );
      await ta.reverts(
        instance.vote(
          1,
          "0x4bdcdfc31155544948c6cc905abf63e803053cd92540426d43df38a53d1b3698",
          "test_encrypted_vote",
          voters[1],
          ethers.utils.joinSignature(signature)
        ),
        "User not eligible to vote"
      );
    });
  });

  /// countVotes function ///

  describe("countVotes function", () => {
    let votes = [];
    const voters = [accounts[1], accounts[2], accounts[3], accounts[4]];

    beforeEach(async () => {
      await advanceBlockAtTime(blockchainStartTime);

      instance = await eVoteContract.new(
        admins.registrars,
        admins.chairpeople,
        admins.electoralBoard
      );

      await instance.createPoll(
        startTime,
        endTime,
        [optionOne, optionTwo, optionThree],
        pollName,
        hash,
        publicKey,
        tallyTime,
        { from: admins.chairpeople[0] }
      );

      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      await instance.addVoters(voters, 1, { from: admins.chairpeople[0] });

      //Confirm voters
      instance.confirmVoters(1, 0, { from: admins.chairpeople[0] });
      instance.confirmVoters(1, 1, { from: admins.chairpeople[1] });

      //Confirm poll
      await instance.confirmPoll(1, 0, { from: admins.electoralBoard[0] });
      for (i = 0; i < 3; i++) {
        await instance.confirmPoll(1, 2, {
          from: admins.electoralBoard[1 + i],
        });
      }

      //Create the votes
      votes.push({
        optionID: 1,
        uuid: uuidv4(),
      });
      votes.push({
        optionID: 2,
        uuid: uuidv4(),
      });
      votes.push({
        optionID: 3,
        uuid: uuidv4(),
      });
      votes.push({
        optionID: 2,
        uuid: uuidv4(),
      });
      //Hash the votes
      for (var i = 0; i < 4; i++) {
        const stringToHash =
          "pollID:1;optionID:" + votes[i].optionID + ";uuid:" + votes[i].uuid;
        const hash = keccak256(stringToHash).toString("hex");
        const hashWithPrefix = "0x" + hash;
        votes[i].voteToEncrypt = stringToHash;
        votes[i].hash = hashWithPrefix;
      }
      //Encrypt the votes
      const publicKeyImported = fs.readFileSync(
        path.resolve(__dirname, "publicKey.pem")
      );
      for (var i = 0; i < 4; i++) {
        const encryptedVote = crypto.publicEncrypt(
          { key: publicKeyImported },
          Buffer.from(votes[i].voteToEncrypt)
        );
        votes[i].encryptedVote = encryptedVote;
      }
      //Cast the votes
      for (var i = 0; i < 4; i++) {
        //calculate the messageHash
        const messageHash = ethers.utils.solidityKeccak256(
          ["address", "uint", "bytes32", "address", "string"],
          [
            instance.address,
            1,
            votes[i].hash,
            voters[i],
            votes[i].encryptedVote.toString("base64"),
          ]
        );

        //create the vote signatures
        const messageHashBytes = ethers.utils.arrayify(messageHash);

        const signer = provider.getSigner(voters[i]);
        const signature = await signer.signMessage(messageHashBytes);

        await instance.vote(
          1,
          votes[i].hash,
          votes[i].encryptedVote.toString("base64"),
          voters[i],
          ethers.utils.joinSignature(signature)
        );
        votes[i].voter = voters[i];
      }
    });

    it("should test the countVote function by calling it with valid input || positive", async () => {
      await advanceBlockAtTime(tallyTime + 60 * 2);

      //decrypt the votes
      const privateKey = fs.readFileSync(
        path.resolve(__dirname, "privateKey.pem")
      );

      var voteOptionIDs = [];
      var uuids = [];

      const pollID = 1;

      for (var i = 0; i < 4; i++) {
        var optionID;
        var uuid;

        const decryptedVote = crypto.privateDecrypt(
          { key: privateKey },
          Buffer.from(votes[i].encryptedVote)
        );

        const vote = decryptedVote.toString();
        const sub_items = vote.split(";");

        // pollID = sub_items[0].split(":")
        // pollID = pollID[1]

        optionID = sub_items[1].split(":");
        optionID = optionID[1];

        uuid = sub_items[2].split(":");
        uuid = uuid[1];

        voteOptionIDs.push(optionID);
        uuids.push(uuid);
      }

      //count the votes
      await ta.passes(instance.countVotes(pollID, voteOptionIDs, uuids));

      //make sure the votes were counted correctly
      const optionOne = await instance.viewVoteOption(1, 1);
      const optionTwo = await instance.viewVoteOption(1, 2);
      const optionThree = await instance.viewVoteOption(1, 3);
      assert.equal(1, optionOne.votes);
      assert.equal(2, optionTwo.votes);
      assert.equal(1, optionThree.votes);
    });

    it("should test the countVote function by calling it with an invalid vote option || negative", async () => {
      await advanceBlockAtTime(tallyTime + 60 * 2);
      await ta.reverts(
        instance.countVotes(1, [1], [votes[2].uuid]),
        "Vote hash not valid"
      );
    });

    it("should test the countVote function by calling it before the tally time is reached || negative", async () => {
      await advanceBlockAtTime(endTime + 10);
      await ta.reverts(
        instance.countVotes(1, [3], [votes[2].uuid]),
        "Tally time not reached"
      );
    });
  });
});
