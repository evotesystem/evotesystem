const eVoteContract = artifacts.require("Evote");
const ta = require("truffle-assertions");
const { getAdmins, advanceBlockAtTime } = require("./helper");

contract("Admin", async (accounts) => {
  //constructor is tested in the test file of the Evote contract

  let instance;
  let admins = getAdmins(accounts);

  /// REGISTRARS ///
  describe("Registrars", () => {
    /// removeRegistrar ///
    describe("removeRegistrar function", () => {
      beforeEach(async () => {
        instance = await eVoteContract.new(
          admins.registrars,
          admins.chairpeople,
          admins.electoralBoard
        );
      });

      it("should test the removeRegistrar function by removing a registrar || positive", async () => {
        await instance.removeRegistrar(admins.registrars[0], 0, {
          from: admins.electoralBoard[0],
        });
        for (var i = 0; i < 3; i++) {
          await instance.removeRegistrar(admins.registrars[0], 1, {
            from: admins.electoralBoard[1 + i],
          });
        }
        assert.equal(false, await instance.isRegistrar(admins.registrars[0]));
        assert.equal(2, await instance.registrarCount());
      });

      it("should test the removeRegistrar function by removing an account that isn't a registrar || negative", async () => {
        await ta.reverts(
          instance.removeRegistrar(accounts[4], 0, {
            from: admins.electoralBoard[0],
          }),
          "Address is not a registrar"
        );
      });

      it("should test the removeRegistrar function by consecuitively removing two registrars || negative", async () => {
        await instance.removeRegistrar(admins.registrars[0], 0, {
          from: admins.electoralBoard[0],
        });
        for (var i = 0; i < 3; i++) {
          await instance.removeRegistrar(admins.registrars[0], 1, {
            from: admins.electoralBoard[1 + i],
          });
        }
        await ta.reverts(
          instance.removeRegistrar(admins.registrars[1], 0, {
            from: admins.electoralBoard[0],
          }),
          "The minimum number of registrars has already been reached"
        );
      });
    });

    /// addRegistrar ///
    describe("addRegistrar function", () => {
      beforeEach(async () => {
        instance = await eVoteContract.new(
          admins.registrars,
          admins.chairpeople,
          admins.electoralBoard
        );

        instance.removeRegistrar(admins.registrars[0], 0, {
          from: admins.electoralBoard[5],
        });

        for (var i = 0; i < 3; i++) {
          instance.removeRegistrar(admins.registrars[0], 1, {
            from: admins.electoralBoard[1 + i],
          });
        }
      });

      it("should test the addRegistrar function by adding a new account as a registrar || positive", async () => {
        await instance.addRegistrar(accounts[1], 0, {
          from: admins.electoralBoard[0],
        });

        for (var i = 0; i < 3; i++) {
          await instance.addRegistrar(accounts[1], 2, {
            from: admins.electoralBoard[1 + i],
          });
        }

        assert.isTrue(await instance.isRegistrar(accounts[1]));
        assert.equal(3, await instance.registrarCount());
      });

      it("should test the addRegistrar function by consecuitively adding two registrars || negative", async () => {
        await instance.addRegistrar(accounts[1], 0, {
          from: admins.electoralBoard[0],
        });

        for (var i = 0; i < 3; i++) {
          await instance.addRegistrar(accounts[1], 2, {
            from: admins.electoralBoard[1 + i],
          });
        }

        await ta.reverts(
          instance.addRegistrar(accounts[2], 0, {
            from: admins.electoralBoard[1],
          }),
          "The maximum number of registrars has already been reached"
        );
      });

      it("should test the addRegistrar function by adding a chairperson as a registrar || negative", async () => {
        await ta.reverts(
          instance.addRegistrar(admins.chairpeople[0], 0, {
            from: admins.electoralBoard[0],
          }),
          "User can't have two different admin roles at the same time"
        );
      });

      it("should test the addRegistrar function by adding an election board member as a registrar || negative", async () => {
        await ta.reverts(
          instance.addRegistrar(admins.electoralBoard[0], 0, {
            from: admins.electoralBoard[0],
          }),
          "User can't have two different admin roles at the same time"
        );
      });
    });
  });

  /// CHAIRPEOPLE ///
  describe("Chairpeople", () => {
    /// removeChairperson ///
    describe("removeChairperson function", () => {
      beforeEach(async () => {
        instance = await eVoteContract.new(
          admins.registrars,
          admins.chairpeople,
          admins.electoralBoard
        );
      });

      it("should test the removeChairperson function by removing a chairperson || positive", async () => {
        await instance.removeChairperson(admins.chairpeople[0], 0, {
          from: admins.electoralBoard[0],
        });
        for (var i = 0; i < 3; i++) {
          await instance.removeChairperson(admins.chairpeople[0], 1, {
            from: admins.electoralBoard[1 + i],
          });
        }
        assert.equal(
          false,
          await instance.isChairperson(admins.chairpeople[0])
        );
        assert.equal(2, await instance.chairpersonCount());
      });

      it("should test the removeChairperson function by removing an account that isn't a chairperson || negative", async () => {
        await ta.reverts(
          instance.removeChairperson(accounts[4], 0, {
            from: admins.electoralBoard[0],
          }),
          "Address is not a chairperson"
        );
      });

      it("should test the removeChairperson function by consecuitively removing two chairpeople || negative", async () => {
        await instance.removeChairperson(admins.chairpeople[0], 0, {
          from: admins.electoralBoard[0],
        });
        for (var i = 0; i < 3; i++) {
          await instance.removeChairperson(admins.chairpeople[0], 1, {
            from: admins.electoralBoard[1 + i],
          });
        }
        await ta.reverts(
          instance.removeChairperson(admins.chairpeople[1], 0, {
            from: admins.electoralBoard[0],
          }),
          "The minimum number of chairpeople has already been reached"
        );
      });
    });

    /// addChairperson ///
    describe("addChairperson function", () => {
      beforeEach(async () => {
        instance = await eVoteContract.new(
          admins.registrars,
          admins.chairpeople,
          admins.electoralBoard
        );

        instance.removeChairperson(admins.chairpeople[0], 0, {
          from: admins.electoralBoard[5],
        });

        for (var i = 0; i < 3; i++) {
          instance.removeChairperson(admins.chairpeople[0], 1, {
            from: admins.electoralBoard[1 + i],
          });
        }
      });

      it("should test the addChairperson function by adding a new account as a chairperson || positive", async () => {
        await instance.addChairperson(accounts[1], 0, {
          from: admins.electoralBoard[0],
        });

        for (var i = 0; i < 3; i++) {
          await instance.addChairperson(accounts[1], 2, {
            from: admins.electoralBoard[1 + i],
          });
        }

        assert.isTrue(await instance.isChairperson(accounts[1]));
        assert.equal(3, await instance.chairpersonCount());
      });

      it("should test the addChairperson function by consecuitively adding two chairpeople || negative", async () => {
        await instance.addChairperson(accounts[1], 0, {
          from: admins.electoralBoard[0],
        });

        for (var i = 0; i < 3; i++) {
          await instance.addChairperson(accounts[1], 2, {
            from: admins.electoralBoard[1 + i],
          });
        }

        await ta.reverts(
          instance.addChairperson(accounts[2], 0, {
            from: admins.electoralBoard[0],
          }),
          "The maximum number of chairpeople has already been reached"
        );
      });

      it("should test the addChairperson function by adding a registrar as a chairperson || negative", async () => {
        await ta.reverts(
          instance.addChairperson(admins.registrars[0], 0, {
            from: admins.electoralBoard[0],
          }),
          "User can't have two different admin roles at the same time"
        );
      });

      it("should test the addChairperson function by adding an election board member as a chairperson || negative", async () => {
        await ta.reverts(
          instance.addChairperson(admins.electoralBoard[0], 0, {
            from: admins.electoralBoard[0],
          }),
          "User can't have two different admin roles at the same time"
        );
      });
    });
  });

  /// ELECTION BOARD ///
  describe("Election Board", () => {
    /// removeElectionBoardMember ///
    describe("removeElectionBoardMember function", () => {
      beforeEach(async () => {
        instance = await eVoteContract.new(
          admins.registrars,
          admins.chairpeople,
          admins.electoralBoard
        );
      });

      it("should test the removeElectionBoardMember function by removing an election board member || positive", async () => {
        await instance.removeElectionBoardMember(admins.electoralBoard[0], 0, {
          from: admins.registrars[0],
        });
        for (var i = 0; i < 2; i++) {
          await instance.removeElectionBoardMember(
            admins.electoralBoard[0],
            1,
            { from: admins.chairpeople[1 + i] }
          );
        }
        await instance.removeElectionBoardMember(admins.electoralBoard[0], 1, {
          from: admins.chairpeople[0],
        });
        assert.equal(
          false,
          await instance.isElectionBoardMember(admins.electoralBoard[0])
        );
        assert.equal(5, await instance.electionBoardMemberCount());
      });

      it("should test the removeElectionBoardMember function by removing an account that isn't an election board member || negative", async () => {
        await ta.reverts(
          instance.removeElectionBoardMember(accounts[4], 0, {
            from: admins.registrars[0],
          }),
          "Address is not an election board member"
        );
      });

      it("should test the removeElectionBoardMember function by consecuitively removing two election board members || negative", async () => {
        await instance.removeElectionBoardMember(admins.electoralBoard[0], 0, {
          from: admins.registrars[0],
        });
        for (var i = 0; i < 2; i++) {
          await instance.removeElectionBoardMember(
            admins.electoralBoard[0],
            1,
            { from: admins.chairpeople[i + 1] }
          );
        }
        await instance.removeElectionBoardMember(admins.electoralBoard[0], 1, {
          from: admins.chairpeople[0],
        });
        await ta.reverts(
          instance.removeElectionBoardMember(admins.electoralBoard[1], 0, {
            from: admins.registrars[0],
          }),
          "The minimum number of election board members has already been reached"
        );
      });
    });

    /// addElectionBoardMember ///
    describe("addElectionBoardMember function", () => {
      beforeEach(async () => {
        instance = await eVoteContract.new(
          admins.registrars,
          admins.chairpeople,
          admins.electoralBoard
        );

        await instance.removeElectionBoardMember(admins.electoralBoard[0], 0, {
          from: admins.chairpeople[1],
        });

        for (var i = 0; i < 2; i++) {
          await instance.removeElectionBoardMember(
            admins.electoralBoard[0],
            1,
            { from: admins.registrars[1 + i] }
          );
        }
        await instance.removeElectionBoardMember(admins.electoralBoard[0], 1, {
          from: admins.chairpeople[0],
        });
      });

      it("should test the addElectionBoardMember function by adding a new account as an election board member || positive", async () => {
        await instance.addElectionBoardMember(accounts[1], 0, {
          from: admins.registrars[0],
        });

        for (var i = 0; i < 2; i++) {
          await instance.addElectionBoardMember(accounts[1], 2, {
            from: admins.registrars[1 + i],
          });
        }

        await instance.addElectionBoardMember(accounts[1], 2, {
          from: admins.chairpeople[0],
        });

        assert.isTrue(await instance.isElectionBoardMember(accounts[1]));
        assert.equal(6, await instance.electionBoardMemberCount());
      });

      it("should test the addElectionBoardMember function by consecuitively adding two election board members || negative", async () => {
        await instance.addElectionBoardMember(accounts[1], 0, {
          from: admins.registrars[0],
        });

        for (var i = 0; i < 2; i++) {
          await instance.addElectionBoardMember(accounts[1], 2, {
            from: admins.registrars[1 + i],
          });
        }

        await instance.addElectionBoardMember(accounts[1], 2, {
          from: admins.chairpeople[0],
        });

        await ta.reverts(
          instance.addElectionBoardMember(accounts[2], 0, {
            from: admins.registrars[2],
          }),
          "The maximum number of election board members has already been reached"
        );
      });

      it("should test the addElectionBoardMember function by adding a registrar as an election board member || negative", async () => {
        await ta.reverts(
          instance.addElectionBoardMember(admins.registrars[0], 0, {
            from: admins.chairpeople[0],
          }),
          "User can't have two different admin roles at the same time"
        );
      });

      it("should test the addElectionBoardMember function by adding a chairperson as an election board member || negative", async () => {
        await ta.reverts(
          instance.addElectionBoardMember(admins.chairpeople[0], 0, {
            from: admins.registrars[0],
          }),
          "User can't have two different admin roles at the same time"
        );
      });
    });
  });

  describe("Cooldown", () => {
    const blockchainStartTime = Math.round(new Date().getTime() / 1000);

    beforeEach(async () => {
      await advanceBlockAtTime(blockchainStartTime);

      instance = await eVoteContract.new(
        admins.registrars,
        admins.chairpeople,
        admins.electoralBoard
      );
    });

    it("should trigger the cooldown by opening two two-third requests immediately after another from the same account || positive", async () => {
      await instance.removeElectionBoardMember(admins.electoralBoard[0], 0, {
        from: admins.chairpeople[1],
      });
      await ta.reverts(
        instance.removeElectionBoardMember(admins.electoralBoard[2], 0, {
          from: admins.chairpeople[1],
        }),
        "Cooldown still in progress"
      );
    });

    it("should trigger the cooldown by opening two two-third requests 11 minutes after another from the same account || positive", async () => {
      await instance.removeElectionBoardMember(admins.electoralBoard[0], 0, {
        from: admins.chairpeople[1],
      });
      await advanceBlockAtTime(blockchainStartTime + 11 * 60);

      await ta.passes(
        instance.removeElectionBoardMember(admins.electoralBoard[2], 0, {
          from: admins.chairpeople[1],
        })
      );
    });
  });
});
