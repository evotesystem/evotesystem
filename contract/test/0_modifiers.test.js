const eVoteContract = artifacts.require("Evote");
const { ethers } = require("ethers");
const provider = new ethers.providers.JsonRpcProvider("http://localhost:7545");
const ta = require("truffle-assertions");

const {
  advanceBlockAtTime,
  getAdmins,
  generateSignatures,
  verificationHashes,
} = require("./helper");

contract("eVote & Admin: Modifiers", async (accounts) => {
  //get the admins
  const admins = getAdmins(accounts);

  //set the date of the blockchain to now
  const blockchainStartTime = Math.round(new Date().getTime() / 1000);
  await advanceBlockAtTime(blockchainStartTime);

  let instance;

  const startTime = Math.round(new Date().getTime() / 1000) + 10 * 60;
  const endTime = Math.round(new Date().getTime() / 1000) + 60 * 60;
  const tallyTime = Math.round(new Date().getTime() / 1000) + 90 * 60;

  const optionOne = "Yes";
  const optionTwo = "No";
  const optionThree = "I don't care";
  const pollName = "Should drugs be legalized?";
  const hash =
    "0x42bb176eb31c9c20c3889601d6533eb9e7de06a6040dd2f9ea73381c5b21e0ce";
  const publicKey =
    "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEArhnCBK2N2tspya7+17RSl0F6ghZySFTtRT1oIBln4CsZZo9NqW4D0hO4Me48fTK0LS+OLmUCNTzZklS3P2l4qK58yGD4qCP70LN7LjWSDR5fEPt5LHBq0ww6HOBtUkiDvzw8FEecgFYcf7m4HzvGTFsRE79oRMJlwT8wgCEmhL/TUHRE7qTKb+pk75IUirfF6PmFncrZDf9bjhX7/2JZMLM9dHp/aYOxQ15C+D2cx80ONvSkRY5AzN8lZNQSYihsPRi9YDBQWrjL+TH+tcclaHuM+wnxSWEum6te0yhpgKxEFzjcoli4gszOsgt0CRj1czItj+EU6IlSevp2M2Y13jev4Qw3qNtd/+OxnKG5WeEl+zHECteP2t0BWuEkyGJp//DAjikziLhDBzaKlryJkimF8VArFrfAJN21XJgN3uwRDyYGf1FUEPfFcUA2CKHNfV2dCAoa4W6zkTOE3G+VJ/MKaU2K0gdam9rbAyCM+KYdDBpdWRAGLIEqWa1eH+iElKYo1XdFx0s+i2M/S97iFTRSm+IcrXG4dE209kEJTXJ5Geb2rTQwjwHDlNjaGfSTvaTrvQCFt1toDwd0wCwjQnEYhLeL4+bLDdc3veKT35ea06cQSN4jt/wfpSEDAU60K7KVUAr8NN4TQIT5NTNQYM/2AbHts6TUv+PbGrTQwf0CAwEAAQ==";
  const signatures = await generateSignatures(
    verificationHashes,
    admins.registrars
  );

  /// ADMIN CONTRACT MODIFIERS ///

  describe("Admin Contract Modifiers", () => {
    beforeEach(async () => {
      await advanceBlockAtTime(blockchainStartTime);

      instance = await eVoteContract.new(
        admins.registrars,
        admins.chairpeople,
        admins.electoralBoard
      );

      await instance.createPoll(
        startTime,
        endTime,
        [optionOne, optionTwo, optionThree],
        pollName,
        hash,
        publicKey,
        tallyTime,
        { from: admins.chairpeople[0] }
      );
    });

    /// registrarOnly modifier ///

    it("should test the registrarOnly modifier by calling it from a non registrar account || negative", async () => {
      await ta.reverts(
        instance.addVerificationHashes(1, [], [], { from: accounts[3] }),
        "Sender is not a registrar"
      );
    });

    it("should test the registrarOnly modifier by calling it when there aren't sufficient registrars || negative", async () => {
      await instance.removeRegistrar(admins.registrars[2], 0, {
        from: admins.electoralBoard[0],
      });

      for (i = 0; i < 3; i++) {
        await instance.removeRegistrar(admins.registrars[2], 1, {
          from: admins.electoralBoard[1 + i],
        });
      }

      await ta.reverts(
        instance.addVerificationHashes(1, verificationHashes, signatures, {
          from: admins.registrars[0],
        }),
        "Registrars need to be complete to call functions"
      );
    });

    it("should test the registrarOnly modifier by calling it from a registrar account || positive", async () => {
      await ta.passes(
        instance.addVerificationHashes(1, verificationHashes, signatures, {
          from: admins.registrars[0],
        })
      );
    });

    /// chairpersonOnly modifier ///

    it("should test the chairpersonOnly modifier by calling it from a non chairperson account || negative", async () => {
      await ta.reverts(
        instance.createPoll(100, 1000, [], "", "", "", 1001, {
          from: accounts[4],
        }),
        "Sender is not a chairperson"
      );
    });

    it("should test the chairpersonOnly modifier by calling it when there aren't sufficient chairpeople || negative", async () => {
      await instance.removeChairperson(admins.chairpeople[0], 0, {
        from: admins.electoralBoard[0],
      });

      for (i = 0; i < 3; i++) {
        await instance.removeChairperson(admins.chairpeople[0], 1, {
          from: admins.electoralBoard[1 + i],
        });
      }

      await ta.reverts(
        instance.createPoll(100, 1000, [], "", "", "", 1001, {
          from: admins.chairpeople[1],
        }),
        "Chairpeople need to be complete to call functions"
      );
    });

    it("should test the chairpersonOnly modifier by calling it from a chairperson account || positive", async () => {
      await ta.reverts(
        instance.createPoll(100, 1000, [], "", "", "", 1001, {
          from: admins.chairpeople[1],
        }),
        "Too few vote options submitted"
      );
    });

    /// electionBoardOnly modifier ///

    it("should test the electionBoardOnly modifier by calling it from a non election board account || negative", async () => {
      await ta.reverts(
        instance.removeRegistrar(admins.registrars[0], 0, {
          from: accounts[6],
        }),
        "Sender is not an election board member"
      );
    });

    it("should test the electionBoardOnly modifier by calling it when there aren't sufficient election board members || negative", async () => {
      await instance.removeElectionBoardMember(admins.electoralBoard[0], 0, {
        from: admins.registrars[0],
      });

      for (i = 0; i < 2; i++) {
        await instance.removeElectionBoardMember(admins.electoralBoard[0], 1, {
          from: accounts[11 + i],
        });
      }

      await instance.removeElectionBoardMember(admins.electoralBoard[0], 1, {
        from: admins.chairpeople[0],
      });

      await ta.reverts(
        instance.removeRegistrar(admins.registrars[0], 0, {
          from: admins.electoralBoard[3],
        }),
        "Election board needs to be complete to call functions"
      );
    });

    it("should test the electionBoardOnly modifier by calling it from an election board account || positive", async () => {
      await ta.passes(
        instance.removeRegistrar(admins.registrars[0], 0, {
          from: admins.electoralBoard[0],
        })
      );
    });

    /// registrarAndChairpersonOnly modifier ///

    it("should test the registrarAndChairpersonOnly modifier by calling it from a non chairperson/registrar account || negative", async () => {
      await ta.reverts(
        instance.removeElectionBoardMember(admins.electoralBoard[0], 0, {
          from: accounts[4],
        }),
        "Sender neither is a registrar nor a chairperson"
      );
    });

    it("should test the registrarAndChairpersonOnly modifier by calling it when there aren't sufficient registrars || negative", async () => {
      await instance.removeRegistrar(admins.registrars[0], 0, {
        from: admins.electoralBoard[0],
      });

      for (i = 0; i < 3; i++) {
        await instance.removeRegistrar(admins.registrars[0], 1, {
          from: admins.electoralBoard[1 + i],
        });
      }

      await ta.reverts(
        instance.removeElectionBoardMember(admins.electoralBoard[0], 0, {
          from: accounts[11],
        }),
        "Registrars need to be complete to call functions"
      );
    });

    it("should test the registrarAndChairpersonOnly modifier by calling it when there aren't sufficient chairpeople || negative", async () => {
      await instance.removeChairperson(admins.chairpeople[0], 0, {
        from: admins.electoralBoard[0],
      });

      for (i = 0; i < 3; i++) {
        await instance.removeChairperson(admins.chairpeople[0], 1, {
          from: admins.electoralBoard[1 + i],
        });
      }

      await ta.reverts(
        instance.removeElectionBoardMember(admins.electoralBoard[0], 0, {
          from: admins.chairpeople[1],
        }),
        "Chairpeople need to be complete to call functions"
      );
    });

    it("should test the registrarAndChairpersonOnly modifier by calling it from a chairperson account || positive", async () => {
      await ta.passes(
        instance.removeElectionBoardMember(admins.electoralBoard[0], 0, {
          from: admins.chairpeople[0],
        })
      );
    });

    it("should test the registrarAndChairpersonOnly modifier by calling it from a registrar account || positive", async () => {
      await ta.passes(
        instance.removeElectionBoardMember(admins.electoralBoard[0], 0, {
          from: admins.registrars[0],
        })
      );
    });
  });

  /// EVOTE CONTRACT MODIFIERS ///

  describe("eVote Contract Modifiers", () => {
    beforeEach(async () => {
      await advanceBlockAtTime(blockchainStartTime);

      instance = await eVoteContract.new(
        admins.registrars,
        admins.chairpeople,
        admins.electoralBoard
      );

      await instance.createPoll(
        startTime,
        endTime,
        [optionOne, optionTwo, optionThree],
        pollName,
        hash,
        publicKey,
        tallyTime,
        { from: admins.chairpeople[0] }
      );
    });

    /// pollExists modifier ///

    it("should test the pollExists modifier by calling it with a poll id that doesn't exist || negative", async () => {
      await ta.reverts(
        instance.getVoteHashStatus(
          6,
          "0x6a0d259bd4fb907339fd7c65a133083c1e9554f2ca6325b806612c8df6d7df22"
        ),
        "Poll doesn't exist"
      );
    });

    it("should test the pollExists modifier by calling it with a poll id that exists || positive", async () => {
      await ta.reverts(
        instance.getVoteHashStatus(
          1,
          "0x6a0d259bd4fb907339fd7c65a133083c1e9554f2ca6325b806612c8df6d7df22"
        ),
        "Poll isn't approved"
      );
    });

    /// verificationHahsesAdded modifier ///

    it("should test the verificationHashesAdded modifier by calling it before the hashes are added || negative", async () => {
      await ta.reverts(
        instance.addVoters([], 1, { from: admins.chairpeople[0] }),
        "Verification hashes not added"
      );
    });

    it("should test the verificationHashesAdded modifier by calling it after the hashes were added || positive", async () => {
      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      await ta.passes(
        instance.addVoters([], 1, { from: admins.chairpeople[0] })
      );
    });

    /// pollApproved modifier ///

    it("should test the pollApproved modifier by calling it on a poll that isn't approved || negative", async () => {
      await advanceBlockAtTime(startTime + 10);

      //create a false vote signature
      const messageHashBytes = ethers.utils.arrayify(
        "0x6a0d259bd4fb907339fd7c65a133083c1e9554f2ca6325b806612c8df6d7df22"
      );

      const signer = provider.getSigner(accounts[1]);
      const signature = await signer.signMessage(messageHashBytes);

      await ta.reverts(
        instance.vote(
          1,
          "0x6a0d259bd4fb907339fd7c65a133083c1e9554f2ca6325b806612c8df6d7df22",
          "encryptedVote",
          accounts[1],
          signature
        ),
        "Poll isn't approved"
      );
    });

    it("should test the pollApproved modifier by calling it on a poll that is approved || positive", async () => {
      //1: Add Verification Hashes
      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      //2: Add and confirm voters
      await instance.addVoters([accounts[1], accounts[2], accounts[3]], 1, {
        from: admins.chairpeople[0],
      });
      await instance.confirmVoters(1, 0, { from: admins.chairpeople[0] });
      await instance.confirmVoters(1, 1, { from: admins.chairpeople[1] });

      //3: Approve poll
      await instance.confirmPoll(1, 0, { from: admins.electoralBoard[0] });
      for (i = 0; i < 3; i++) {
        await instance.confirmPoll(1, 2, {
          from: admins.electoralBoard[1 + i],
        });
      }

      await advanceBlockAtTime(startTime + 10);

      //4: Create a false vote signature
      const messageHashBytes = ethers.utils.arrayify(
        "0x6a0d259bd4fb907389fd7c65a133083c1e9554f2ca6325b806612c8df6d7df22"
      );

      const signer = provider.getSigner(accounts[4]);
      const signature = await signer.signMessage(messageHashBytes);

      //4: Check modifier
      await ta.reverts(
        instance.vote(
          1,
          "0x6a0d259bd4fb907339fd7c65a133083c1e9554f2ca6325b806612c8df6d7df22",
          "encryptedVote",
          accounts[4],
          signature
        ),
        "Voter address in transaction doesn't match recovered voter"
      );
    });

    /// votersNotConfirmed modifier ///

    it("should test the votersNotConfirmed modifier by calling it on a poll where the voters haven't been confirmed yet || positive", async () => {
      //1: Add Verification Hashes
      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      //2: Add voters
      await instance.addVoters([accounts[1], accounts[2], accounts[3]], 1, {
        from: admins.chairpeople[0],
      });

      //3: Test modifier
      await ta.passes(
        instance.confirmVoters(1, 0, { from: admins.chairpeople[0] })
      );
    });

    it("should test the votersNotConfirmed modifier by calling it on a poll where the voters have already been confirmed || negative", async () => {
      //1: Add Verification Hashes
      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      //2: Add and confirm voters
      await instance.addVoters([accounts[1], accounts[2], accounts[3]], 1, {
        from: admins.chairpeople[0],
      });
      await instance.confirmVoters(1, 0, { from: admins.chairpeople[0] });
      await instance.confirmVoters(1, 1, { from: admins.chairpeople[1] });

      //4: Test modifier
      await ta.reverts(
        instance.confirmVoters(1, 0, { from: admins.chairpeople[0] }),
        "Voters have already been confirmed"
      );
    });

    /// votersConfirmed ///

    it("should test the votersConfirmed modifier by calling it on a poll where the voters haven't been confirmed yet || negative", async () => {
      //1: Add Verification Hashes
      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      //2: Add voters
      await instance.addVoters([accounts[1], accounts[2], accounts[3]], 1, {
        from: admins.chairpeople[0],
      });

      //3: Test modifier
      await ta.reverts(
        instance.confirmPoll(1, 0, { from: admins.electoralBoard[0] }),
        "Voters haven't been confirmed"
      );
    });

    it("should test the votersConfirmed modifier by calling it on a poll where the voters have been confirmed || positive", async () => {
      //1: Add Verification Hashes
      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      //2: Add and confirm voters
      await instance.addVoters([accounts[1], accounts[2], accounts[3]], 1, {
        from: admins.chairpeople[0],
      });
      await instance.confirmVoters(1, 0, { from: admins.chairpeople[0] });
      await instance.confirmVoters(1, 1, { from: admins.chairpeople[1] });

      //4: Test modifier
      await ta.passes(
        instance.confirmPoll(1, 0, { from: admins.electoralBoard[0] })
      );
    });

    /// pollNotApproved modifier ///

    it("should test the pollNotApproved modifier by calling it on a poll that has already been approved || negative", async () => {
      //1: Add Verification Hashes
      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      //2: Add and confirm voters
      await instance.addVoters([accounts[1], accounts[2], accounts[3]], 1, {
        from: admins.chairpeople[0],
      });
      await instance.confirmVoters(1, 0, { from: admins.chairpeople[0] });
      await instance.confirmVoters(1, 1, { from: admins.chairpeople[1] });

      //3: Confirm poll
      await instance.confirmPoll(1, 0, { from: admins.electoralBoard[0] });
      for (i = 0; i < 3; i++) {
        await instance.confirmPoll(1, 2, {
          from: admins.electoralBoard[1 + i],
        });
      }

      //4: Test the modifier
      await ta.reverts(
        instance.confirmPoll(1, 3, { from: admins.electoralBoard[0] }),
        "Poll is approved"
      );
    });

    it("should test the pollNotApproved modifier by calling it on a poll that hasn't been approved yet || positive", async () => {
      //1: Add Verification Hashes
      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      //2: Add and confirm voters
      await instance.addVoters([accounts[1], accounts[2], accounts[3]], 1, {
        from: admins.chairpeople[0],
      });
      await instance.confirmVoters(1, 0, { from: admins.chairpeople[0] });
      await instance.confirmVoters(1, 1, { from: admins.chairpeople[1] });

      //3: Test the modifier
      await ta.passes(
        instance.confirmPoll(1, 0, { from: admins.electoralBoard[0] })
      );
    });

    /// pollNotClosed modifier ///

    it("should test the pollNotClosed modifier by calling it on a poll that has not been closed || positive", async () => {
      //1: Add Verification Hashes
      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      //2: Add and confirm voters
      await instance.addVoters([accounts[1], accounts[2], accounts[3]], 1, {
        from: admins.chairpeople[0],
      });
      await instance.confirmVoters(1, 0, { from: admins.chairpeople[0] });
      await instance.confirmVoters(1, 1, { from: admins.chairpeople[1] });

      //3: Approve poll
      await instance.confirmPoll(1, 0, { from: admins.electoralBoard[0] });
      for (i = 0; i < 3; i++) {
        await instance.confirmPoll(1, 2, {
          from: admins.electoralBoard[1 + i],
        });
      }

      //4: Create a false vote signature
      const messageHashBytes = ethers.utils.arrayify(
        "0x6a0d259bd4fb907339fd7c65a133083c1e9554f2ca6325b806612c8df6d7df22"
      );

      const signer = provider.getSigner(accounts[4]);
      const signature = await signer.signMessage(messageHashBytes);

      await advanceBlockAtTime(startTime + 10);

      await ta.reverts(
        instance.vote(
          1,
          "0x6a0d259bd4fb907339fd7c65a133083c1e9554f2ca6325b806612c8df6d7df22",
          "encryptedVote",
          accounts[4],
          signature
        ),
        "Voter address in transaction doesn't match recovered voter"
      );
    });

    it("should test the pollNotClosed modifier by calling it on a poll that has been closed || negative", async () => {
      //1: Add Verification Hashes
      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      //2: Add and confirm voters
      await instance.addVoters([accounts[1], accounts[2], accounts[3]], 1, {
        from: admins.chairpeople[0],
      });
      await instance.confirmVoters(1, 0, { from: admins.chairpeople[0] });
      await instance.confirmVoters(1, 1, { from: admins.chairpeople[1] });

      //3: Approve poll
      await instance.confirmPoll(1, 0, { from: admins.electoralBoard[0] });
      for (i = 0; i < 3; i++) {
        await instance.confirmPoll(1, 2, {
          from: admins.electoralBoard[1 + i],
        });
      }

      //4: Create a false vote signature
      const messageHashBytes = ethers.utils.arrayify(
        "0x6a0d259bd4fb907339fd7c65a133083c1e9554f2ca6325b806612c8df6d7df22"
      );

      const signer = provider.getSigner(accounts[4]);
      const signature = await signer.signMessage(messageHashBytes);

      await advanceBlockAtTime(endTime + 10);

      await ta.reverts(
        instance.vote(
          1,
          "0x6a0d259bd4fb907339fd7c65a133083c1e9554f2ca6325b806612c8df6d7df22",
          "encryptedVote",
          accounts[4],
          signature,
          { from: accounts[1] }
        ),
        "Poll already closed"
      );
    });

    /// pollClosed modifier ///

    it("should test the pollClosed modifier by calling it on a poll that has been closed || positive", async () => {
      //1: Add Verification Hashes
      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      //2: Add and confirm voters
      await instance.addVoters([accounts[1], accounts[2], accounts[3]], 1, {
        from: admins.chairpeople[0],
      });
      await instance.confirmVoters(1, 0, { from: admins.chairpeople[0] });
      await instance.confirmVoters(1, 1, { from: admins.chairpeople[1] });

      //3: Approve poll
      await instance.confirmPoll(1, 0, { from: admins.electoralBoard[0] });
      for (i = 0; i < 3; i++) {
        await instance.confirmPoll(1, 2, {
          from: admins.electoralBoard[1 + i],
        });
      }

      await advanceBlockAtTime(endTime + 10);

      await ta.reverts(
        instance.countVotes(1, [1], ["placeholder"]),
        "Tally time not reached"
      );
    });

    it("should test the pollClosed modifier by calling it on a poll that hasn't been closed || negative", async () => {
      //1: Add Verification Hashes
      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      //2: Add and confirm voters
      await instance.addVoters([accounts[1], accounts[2], accounts[3]], 1, {
        from: admins.chairpeople[0],
      });
      await instance.confirmVoters(1, 0, { from: admins.chairpeople[0] });
      await instance.confirmVoters(1, 1, { from: admins.chairpeople[1] });

      //3: Approve poll
      await instance.confirmPoll(1, 0, { from: admins.electoralBoard[0] });
      for (i = 0; i < 3; i++) {
        await instance.confirmPoll(1, 2, {
          from: admins.electoralBoard[1 + i],
        });
      }

      await advanceBlockAtTime(startTime + 10);

      await ta.reverts(
        instance.countVotes(1, [1], ["placeholder"]),
        "Poll isn't closed yet"
      );
    });

    /// pollNotBeingConfirmed modifier ///

    it("should test the pollNotBeingConfirmed modifier by calling it on a poll whose voters aren't being confirmed yet || positive", async () => {
      //1: Add Verification Hashes
      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      //2: Test the modifier
      await ta.passes(
        instance.addVoters([accounts[1], accounts[2], accounts[3]], 1, {
          from: admins.chairpeople[0],
        })
      );
    });

    it("should test the pollNotBeingConfirmed modifier by calling it on a poll whose voters are being confirmed || negative", async () => {
      //1: Add Verification Hashes
      await instance.addVerificationHashes(1, verificationHashes, signatures, {
        from: admins.registrars[0],
      });

      await instance.addVoters([accounts[1], accounts[2], accounts[3]], 1, {
        from: admins.chairpeople[0],
      });

      await instance.confirmVoters(1, 0, { from: admins.chairpeople[0] });

      await ta.reverts(
        instance.removeVoters([accounts[1]], 1, {
          from: admins.chairpeople[0],
        }),
        "Poll can't be modified, because it is currently being confirmed"
      );
    });
  });
});
