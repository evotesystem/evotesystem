// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

import "./2_TwoThirdMajority.sol";

contract Admin is TwoThirdMajority {

    event AdminAdded(address indexed _admin, uint indexed _level);
    event AdminRemoved(address indexed _admin, uint indexed _level);

    //Stores the registrars
    mapping(address => bool) public isRegistrar;
    uint8 public registrarCount;

    //Stores the chairpeople
    mapping(address => bool) public isChairperson;
    uint8 public chairpersonCount;

    //Stores the election board members
    mapping(address => bool) public isElectionBoardMember;
    uint8 public electionBoardMemberCount;

    constructor(address[] memory _registrars, address[] memory _chairpeople, address[] memory _electionBoardMembers) {

        require(_registrars.length == 3, "Too many or too few registrars specified");
        require(_chairpeople.length == 3, "Too many or too few chairpeople specified");
        require(_electionBoardMembers.length == 6, "Too many or too few election board members specified");

        for(uint i=0; i<3; i++) {
            require(isRegistrar[_registrars[i]] == false, "Address listed twice as parameter for registrar in constructor");
            isRegistrar[_registrars[i]] = true;
            emit AdminAdded(_registrars[i], 0);
        }

        for(uint i=0; i<3; i++) {
            require(isRegistrar[_chairpeople[i]] == false, "Address already registered as registrar, can't also be chairperson");
            require(isChairperson[_chairpeople[i]] == false, "Address listed twice as parameter for chairperson in constructor");
            isChairperson[_chairpeople[i]] = true;
            emit AdminAdded(_chairpeople[i], 1);
        }

        for(uint i=0; i<6; i++) {
            require(isRegistrar[_electionBoardMembers[i]] == false, "Address already registered as registrar, can't also be election board member");
            require(isChairperson[_electionBoardMembers[i]] == false, "Address already registered as chairperson, can't also be election board member");
            require(isElectionBoardMember[_electionBoardMembers[i]] == false, "Address listed twice as parameter for election board member in constructor");
            isElectionBoardMember[_electionBoardMembers[i]] = true;
            emit AdminAdded(_electionBoardMembers[i], 2);
        }

        registrarCount = 3;
        chairpersonCount = 3;
        electionBoardMemberCount = 6;

    }

    //Restricts access of functions to registrars only
    modifier registrarOnly() {
        require(isRegistrar[msg.sender], "Sender is not a registrar");
        require(registrarCount == 3, "Registrars need to be complete to call functions");
        _;
    }

    //Restricts access of functions to chairpeople only
    modifier chairpersonOnly() {
        require(isChairperson[msg.sender], "Sender is not a chairperson");
        require(chairpersonCount == 3, "Chairpeople need to be complete to call functions");
        _;
    }

    //Restricts access of functions to election board members only
    modifier electionBoardOnly() {
        require(isElectionBoardMember[msg.sender], "Sender is not an election board member");
        require(electionBoardMemberCount == 6, "Election board needs to be complete to call functions");
        _;
    }

    //Restricts access of functions to registrars and chairpeople
    modifier registrarAndChairpersonOnly() {
        require(registrarCount == 3, "Registrars need to be complete to call functions");
        require(chairpersonCount == 3, "Chairpeople need to be complete to call functions");
        if (isRegistrar[msg.sender]) {
            _;
        } else {
            require(isChairperson[msg.sender], "Sender neither is a registrar nor a chairperson");
            _;
        }
    }

    function addRegistrar(address _registrar, uint _requestID) public electionBoardOnly {

        require(isRegistrar[_registrar] == false, "Address already is a registrar");
        require(isChairperson[_registrar] == false, "User can't have two different admin roles at the same time");
        require(isElectionBoardMember[_registrar] == false, "User can't have two different admin roles at the same time");
        require(registrarCount < 3, "The maximum number of registrars has already been reached");

        if (_requestID == 0) {
            //if 0 is entered as requestID, a new request is created
            _createTwoThirdRequest(RequestType.addRegistrar, _registrar, 0);
        } else {
            //else the application tries to vote on the request
            bool requestFinished = _voteOnRequest(_requestID, RequestType.addRegistrar, _registrar, 0);

            //if the request finishes successfully (two-third majority was reached), the admin gets added to the registrars
            if (requestFinished) {
                isRegistrar[idToTwoThirdRequest[_requestID].adminAddress] = true;
                registrarCount++;
                emit AdminAdded(idToTwoThirdRequest[_requestID].adminAddress, 0);
            } 

        }

    }

    function removeRegistrar(address _registrar, uint _requestID) public electionBoardOnly {

        require(isRegistrar[_registrar] == true, "Address is not a registrar");
        require(registrarCount > 2, "The minimum number of registrars has already been reached");

        if (_requestID == 0) {
            //if 0 is entered as requestID, a new request is created
            _createTwoThirdRequest(RequestType.removeRegistrar, _registrar, 0);
        } else {

            //vote on the request
            bool requestFinished = _voteOnRequest(_requestID, RequestType.removeRegistrar, _registrar, 0);

            //if the request finishes successfully (two-third majority was reached), the admin gets removed from the registrars
            if (requestFinished) {
                isRegistrar[idToTwoThirdRequest[_requestID].adminAddress] = false;
                registrarCount--;
                emit AdminRemoved(idToTwoThirdRequest[_requestID].adminAddress, 0);
            } 

        }

    }

    function addChairperson(address _chairperson, uint _requestID) public electionBoardOnly {

        require(isChairperson[_chairperson] == false, "User already is a chairperson");
        require(isRegistrar[_chairperson] == false, "User can't have two different admin roles at the same time");
        require(isElectionBoardMember[_chairperson] == false, "User can't have two different admin roles at the same time");
        require(chairpersonCount < 3, "The maximum number of chairpeople has already been reached");

        if (_requestID == 0) {
            //if 0 is entered as requestID, a new request is created
            _createTwoThirdRequest(RequestType.addChairperson, _chairperson, 0);
        } else {

            //vote on the request
            bool requestFinished = _voteOnRequest(_requestID, RequestType.addChairperson, _chairperson, 0);

            //if the request finishes successfully (two-third majority was reached), the admin gets added to the chairpeople
            if (requestFinished) {
                isChairperson[idToTwoThirdRequest[_requestID].adminAddress] = true;
                chairpersonCount++;
                emit AdminAdded(idToTwoThirdRequest[_requestID].adminAddress, 1);
            } 

        }

    }

    function removeChairperson(address _chairperson, uint _requestID) public electionBoardOnly {
        require(isChairperson[_chairperson] == true, "Address is not a chairperson");
        require(chairpersonCount > 2, "The minimum number of chairpeople has already been reached");

        if (_requestID == 0) {
            //if 0 is entered as requestID, a new request is created
            _createTwoThirdRequest(RequestType.removeChairperson, _chairperson, 0);
        } else {

            //vote on the request
            bool requestFinished = _voteOnRequest(_requestID, RequestType.removeChairperson, _chairperson, 0);

            //if the request finishes successfully (two-third majority was reached), the admin gets removed from the chairpeople
            if (requestFinished) {
                isChairperson[idToTwoThirdRequest[_requestID].adminAddress] = false;
                chairpersonCount--;
                emit AdminRemoved(idToTwoThirdRequest[_requestID].adminAddress, 1);
            } 

        }

    }

    function addElectionBoardMember(address _member, uint _requestID) public registrarAndChairpersonOnly {
        require(isElectionBoardMember[_member] == false, "User already is an election board member");
        require(isChairperson[_member] == false, "User can't have two different admin roles at the same time");
        require(isRegistrar[_member] == false, "User can't have two different admin roles at the same time");
        require(electionBoardMemberCount < 6, "The maximum number of election board members has already been reached");

        if (_requestID == 0) {
            //if 0 is entered as requestID, a new request is created
            _createTwoThirdRequest(RequestType.addElectionBoardMember, _member, 0);
        } else {

            //vote on the request
            bool requestFinished = _voteOnRequest(_requestID, RequestType.addElectionBoardMember, _member, 0);

            //if the request finishes successfully (two-third majority was reached), the admin gets added to the election board
            if (requestFinished) {
                isElectionBoardMember[idToTwoThirdRequest[_requestID].adminAddress] = true;
                electionBoardMemberCount++;
                emit AdminAdded(idToTwoThirdRequest[_requestID].adminAddress, 2);
            } 

        }

    }
 
    function removeElectionBoardMember(address _member, uint _requestID) public registrarAndChairpersonOnly {
        require(isElectionBoardMember[_member] == true, "Address is not an election board member");
        require(electionBoardMemberCount > 5, "The minimum number of election board members has already been reached");

        if (_requestID == 0) {
            //if 0 is entered as requestID, a new request is created
            _createTwoThirdRequest(RequestType.removeElectionBoardMember, _member, 0);
        } else {

            //vote on the request
            bool requestFinished = _voteOnRequest(_requestID, RequestType.removeElectionBoardMember, _member, 0);

            //if the request finishes successfully (two-third majority was reached), the admin gets removed from the election board
            if (requestFinished) {
                isElectionBoardMember[idToTwoThirdRequest[_requestID].adminAddress] = false;
                electionBoardMemberCount--;
                emit AdminRemoved(idToTwoThirdRequest[_requestID].adminAddress, 2);
            } 

        }
    }

}