// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

contract TwoThirdMajority {

    event RequestCreated(address indexed _creator, uint indexed _requestID, RequestType _requestType);
    event AdminVoted(address indexed _admin, uint indexed _requestID);
    event RequestFinishedSuccessfully(uint _requestID, RequestType _requestType);

    enum RequestType { addRegistrar, removeRegistrar, addChairperson, removeChairperson, addElectionBoardMember, removeElectionBoardMember, confirmVoters, confirmPoll }

    struct TwoThirdRequest {
        uint id;
        uint createdAt;
        address adminAddress; //the address of the admin that is voted on
        uint pollID; //the id of the poll that is voted on
        RequestType requestType; //the type of the request
        uint voteCount; //how many votes the request has
        mapping(address => bool) hasVoted; //keeps track of who has voted on the request
    }

    mapping(uint => TwoThirdRequest) public idToTwoThirdRequest;
    uint public requestCount;

    //There is a cooldown on requests, so a rogue admin can't prevent other admins from removing them by always creating new requests and overwriting the request that should remove them
    mapping(address => uint) cooldown;

    function _createTwoThirdRequest(RequestType _requestType, address _adminAddress, uint _pollID) internal {

        require(block.timestamp > cooldown[msg.sender], "Cooldown still in progress");

        requestCount++;
        TwoThirdRequest storage newRequest = idToTwoThirdRequest[requestCount];
        newRequest.id = requestCount;
        newRequest.createdAt = block.timestamp;
        newRequest.adminAddress = _adminAddress;
        newRequest.pollID = _pollID;
        newRequest.requestType = _requestType;
        newRequest.voteCount = 1;
        newRequest.hasVoted[msg.sender] = true;

        cooldown[msg.sender] = block.timestamp + 10 minutes; //Only after ten minutes the admin can create a new request

        emit RequestCreated(msg.sender, newRequest.id, _requestType);
        emit AdminVoted(msg.sender, newRequest.id);
    }

    function _voteOnRequest(uint _requestID, RequestType _requestType, address _adminAddress, uint _pollID) internal returns (bool) {

        //Preventing admins from voting on old and non existent requests
        require(_requestID == requestCount, "Request is not valid"); //Admins can only vote on the newest request, preventing a bunch of problems => explaining this in the report

        //This prevents users from voting on a request they are not authorized to vote on
        require(idToTwoThirdRequest[_requestID].requestType == _requestType, "Request type in parameter doesn't match the actual request type");
        require(idToTwoThirdRequest[_requestID].hasVoted[msg.sender] == false, "Sender has already voted");

        //This makes sure that the specified pollID and adminAddress match the one in the request
        require(idToTwoThirdRequest[_requestID].pollID == _pollID, "PollID in the parameter doesn't match the one of the request");
        require(idToTwoThirdRequest[_requestID].adminAddress == _adminAddress, "Admin address in the parameter doesn't match the one of the request");

        idToTwoThirdRequest[_requestID].hasVoted[msg.sender] = true;
        idToTwoThirdRequest[_requestID].voteCount++;

        //Check if majority has been reached
        uint requiredMajority;

        if (_requestType == RequestType.confirmVoters) {
            requiredMajority = 2;
        } else {
            requiredMajority = 4;
        }

        emit AdminVoted(msg.sender, _requestID);
        
        if (idToTwoThirdRequest[_requestID].voteCount >= requiredMajority) {
            emit RequestFinishedSuccessfully(_requestID, _requestType);
            return true;
        } else {
            return false;
        }

    }

    function votedOnRequest(uint _requestID) public view returns (bool) {
        return(idToTwoThirdRequest[_requestID].hasVoted[msg.sender]);
    }

}