# eVote Smart Contract (Base Version)

## Prerequisites
### 1. NodeJs installed
### 2. Truffle installed (`npm install -g truffle`)
### 3. ganache-cli installed (`npm install -g ganache-cli`)

## Steps

### 1. Clone the project
### 2. Go into the project: `cd <folder name>`
### 3. Start ganache-cli: `npm run ganache`
### 4. Run the tests: `truffle test`
